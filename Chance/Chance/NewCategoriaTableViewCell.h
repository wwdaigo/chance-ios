//
//  NewCategoriaTableViewCell.h
//  Chance
//
//  Created by pedro henrique borges on 11/8/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCategoriaTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UILabel *uilDescricao;


@property(nonatomic, strong) NSDictionary *categoria;

-(void) reloadCell:(NSDictionary *) aCategoria;

+(NSString *)identifier;

@end

//
//  ListaCategoriaViewController.m
//  Chance
//
//  Created by pedro henrique borges on 11/8/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ListaCategoriaViewController.h"
#import "NewCategoriaTableViewCell.h"

@interface ListaCategoriaViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ListaCategoriaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [uitbCategorias reloadData];
    
    categoriasSelecionadas = [[NSMutableArray alloc]initWithCapacity:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewCategoriaTableViewCell *cell = (NewCategoriaTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[NewCategoriaTableViewCell identifier] forIndexPath:indexPath];
    
    
    
    [cell reloadCell:[self.categorias objectAtIndex:indexPath.row]];
    
    if([categoriasSelecionadas containsObject:[self.categorias objectAtIndex:indexPath.row]])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    
    
    return cell;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.categorias.count;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(self.type == 1000){
        [self.delegate didSelectCategoria:[self.categorias objectAtIndex:indexPath.row]];
        [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];
        return;
    }
    
    
    if(indexPath.row == 0)
    {
        
        categoriasSelecionadas = [NSMutableArray new];
        
        
        if(self.todosSelecionado){
            
            [categoriasSelecionadas removeObject:[self.categorias objectAtIndex:indexPath.row]];

            self.todosSelecionado = NO;
        }
        else {
          
            [categoriasSelecionadas addObject:[self.categorias objectAtIndex:indexPath.row]];
            self.todosSelecionado = YES;
        }

    }
    
    
    if(self.todosSelecionado){
        [uitbCategorias reloadData];
        return;
    }
    

    
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType ==UITableViewCellAccessoryCheckmark){
       
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        
            [categoriasSelecionadas removeObject:[self.categorias objectAtIndex:indexPath.row]];
        
    }
    else {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType =UITableViewCellAccessoryCheckmark;
            [categoriasSelecionadas addObject:[self.categorias objectAtIndex:indexPath.row]];
    }
    


}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 90.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

- (IBAction)onSelecionar:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];

    [self.delegate didSelectCategorias:categoriasSelecionadas];
}

@end

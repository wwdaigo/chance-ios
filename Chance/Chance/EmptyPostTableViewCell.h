//
//  EmptyPostTableViewCell.h
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EmptyPostDelegate <NSObject>

-(void)showFriendsFind;

@end

@interface EmptyPostTableViewCell : UITableViewCell

+(NSString *)identifier;

@property (nonatomic,weak) id<EmptyPostDelegate> delegate;

@end

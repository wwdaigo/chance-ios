//
//  AppDelegate.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filtro.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FBSession *session;

@property (nonatomic,strong) Filtro *filtro;

@end

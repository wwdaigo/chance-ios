//
//  FiltroViewController.m
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "FiltroViewController.h"
#import "ListaCategoriaViewController.h"
#import "AppDelegate.h"





@interface FiltroViewController ()<ListaCategoriaDelegate>

@end

@implementation FiltroViewController
{
    AppDelegate *app;
}

- (void)viewDidLoad {
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if(!app.filtro){
        
        app.filtro = [Filtro new];
    }
    
    
    app.filtro.categoriasSelecionadas = nil;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSegmentChanged:(id)sender {

    CGRect newFrame =  viewSegment.frame;
    if (uisgFiltro.selectedSegmentIndex == 0) {
        newFrame.size.height -= 45;
        uihsKm.alpha = 0;
        uilbKm.alpha = 0;
        
        app.filtro.tipoFiltro = 1;
        
    }
    else {
        newFrame.size.height += 45;
   
        uihsKm.alpha = 1;
        uilbKm.alpha = 1;
        app.filtro.tipoFiltro = 2;
        
    }
    
    [viewSegment setFrame:newFrame];
    
    [self.view layoutSubviews];

    
}

- (IBAction)onCancel:(id)sender {
    
    [self.delegate onCancel];
    
}

- (IBAction)onChangeValue:(id)sender {
    
    uilbKm.text = [NSString stringWithFormat:@"%i Km", [[NSNumber numberWithFloat:uihsKm.value] intValue]];
    
}

- (IBAction)onSelectCategory:(id)sender {
    
    ListaCategoriaViewController *listaCategoriaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ListaCategoriaVC"];
    
    listaCategoriaViewController.categorias = self.categoriasList;
    listaCategoriaViewController.delegate = self;
    
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:listaCategoriaViewController];
    
    
    
    formSheet.presentedFormSheetSize = CGSizeMake(300, self.view.frame.size.height - 100);
//    
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    
    //formSheet.shadowRadius = 2.0;
    
    //formSheet.shadowOpacity = 0.3;
    
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    
    formSheet.shouldCenterVertically = YES;
    
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    
    //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
    
    
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
    
    // formSheet.landscapeTopInset = 50;
    
    // formSheet.portraitTopInset = 100;
    
    
    
    formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
        
        
        
    };
    
    
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        
        
        
        
        
    };
    
    
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    
    
    //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
    
    
    
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
        
        
    }];
    
}


-(void)didSelectCategoria:(NSDictionary *)categoria{
    
    /*
     {"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}
     */
    
   
    app.filtro.idCategoria = [[categoria objectForKey:@"CategoriaId"] intValue];
    
    uilbCategoria.text = [categoria objectForKey:@"Nome"];
    
}

-(void)didSelectCategorias:(NSArray *)categorias{
    
    uilbCategoria.text = @"";
    if(categorias.count == 0)
    {
        uilbCategoria.text = @"Todas as categorias";
        return;
    }
    
    
    
    
    int index = 0;
    for (NSDictionary *dic in categorias) {
    
        uilbCategoria.text = [uilbCategoria.text stringByAppendingString:[dic objectForKey:@"Nome"]];
        
        if(index < categorias.count -1){
            
            uilbCategoria.text = [uilbCategoria.text stringByAppendingString:@", "];
        }
        
        index += 1;
        
    }
    
    app.filtro.categoriasSelecionadas = categorias;
    
}

- (IBAction)onFiltrar:(id)sender {
    
    app.filtro.distancia =  uihsKm.value;
    
    [self.delegate onFiltrar:app.filtro];
}

@end

//
//  NewCategoriaTableViewCell.m
//  Chance
//
//  Created by pedro henrique borges on 11/8/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "NewCategoriaTableViewCell.h"

@implementation NewCategoriaTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(NSString *)identifier{
    
    return @"Categoria Cell";
}


-(void) reloadCell:(NSDictionary *) aCategoria {
    
    self.categoria = aCategoria;
    
    if (![[self.categoria objectForKey:@"Nome"] isKindOfClass:[NSNull class]]) {
        
        [[self uilNome] setText:[self.categoria objectForKey:@"Nome"]];
    }
    
    if (![[self.categoria objectForKey:@"Descricao"] isKindOfClass:[NSNull class]]) {
        
        [[self uilDescricao] setText:[self.categoria objectForKey:@"Descricao"]];
    }
    
    if (([[self.categoria objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) &&
        (![[[self.categoria objectForKey:@"Foto"] objectForKey:@"UrlThumb"] isKindOfClass:[NSNull class]])) {
        
        ////[MRProgressOverlayView showOverlayAddedTo:self.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        [self.uiivFoto setImageWithURLString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        /*
         NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"UrlThumb"]]];
         
         AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
         operation.responseSerializer = [AFImageResponseSerializer serializer];
         
         [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         [self.uiivFoto setImage:(UIImage *)responseObject];
         [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"Error: %@", error);
         [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
         [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
         }];
         */
        //[[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        //[operation start];
    }
    else {
        
        [[self uiivFoto] setImage:[UIImage imageNamed:@"Icon-60"]];
    }
}



@end

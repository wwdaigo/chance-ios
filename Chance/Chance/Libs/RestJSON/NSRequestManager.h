//
//  RestJSON.h
//  Accor
//
//  Created by Luiz Carlos Sant'Ana Junior on 05/07/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking.h>

@interface NSRequestManager : NSObject {

}

@property (nonatomic, strong) AFHTTPSessionManager *manager;

+(NSRequestManager *) shared;

-(id) init;
-(void) cancelAllHTTPOperationsWithURL:(NSString *) urlString;
+(void) cancelAllOperations;

+(void) requestURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure;

+(void) requestURLComplex:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure;

+(void) requestURLComplex:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andButton:(UIButton *)button andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure;

+(void) requestURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andButton:(UIButton *)button andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure;
//+(void) postDataURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) pathData andSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success andFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
+(void) postDataURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) pathData andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure;
+(void) uploadTaskURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) urlData andFile:(void (^)(id<AFMultipartFormData> formData)) file andProgess:(void (^)(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend)) aProgress andSuccess:(void (^)(NSURLResponse *response, id responseObject, NSError *error)) success andFailure:(void (^)(NSError *error)) failure;

@end

//
//  RestJSON.m
//  Accor
//
//  Created by Luiz Carlos Sant'Ana Junior on 05/07/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "NSRequestManager.h"

@implementation NSRequestManager

static NSRequestManager *_shared = nil;

+(NSRequestManager *) shared {
    
	if(_shared == nil) {
        
		_shared = [[NSRequestManager alloc] init];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        //Server retornado tipo errado, para resolver segue abaixo solução alternativa
        //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
	}
	return _shared;
}

- (id) init {
    
	self = [super init];
	if (self != nil) {
		
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        [configuration setTimeoutIntervalForRequest:120];
        [configuration setHTTPMaximumConnectionsPerHost:6];
        NSLog(@"%i", configuration.allowsCellularAccess);
        NSLog(@"%i", configuration.HTTPMaximumConnectionsPerHost);
        NSLog(@"%@", configuration.HTTPAdditionalHeaders);
        NSLog(@"%f", configuration.timeoutIntervalForRequest);
        NSLog(@"%f", configuration.timeoutIntervalForResource);
        NSLog(@"%@", configuration.URLCache);
        
        self.manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        //Server retornado tipo errado, para resolver segue abaixo solução alternativa
        //self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        //NSLog(@"maxConcurrentOperationCount: %li", [self.manager operationQueue].maxConcurrentOperationCount);
	}
    
    return self;
}

-(void) cancelAllHTTPOperationsWithURL:(NSString *) urlString {
    
    //NSLog(@"cancelAllHTTPOperationsWithURL - operationCount: %i", [self.httpClient operationQueue].operationCount);
    
    [[self.manager operationQueue] cancelAllOperations];
    //[self.httpClient cancelAllHTTPOperationsWithMethod:@"POST" path:urlString];
}

+(void) cancelAllOperations {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [[manager operationQueue] cancelAllOperations];
}
/*
+(void) requestURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success andFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    
    //NSURL *url = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    [parameters setObject:[NSNumber numberWithInt:1] forKey:@"plataforma"];
    [parameters setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forKey:@"versao"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [parameters setObject:[defaults objectForKey:@"DeviceToken"] forKey:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [parameters setObject:[defaults objectForKey:@"Token"] forKey:@"Token"];
    }
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    NSLog(@"current locale: %@", locale);
    
    [parameters setObject:locale forKey:@"Idioma"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Server retornado tipo errado, para resolver segue abaixo solução alternativa
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
    }
    
    [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
    
    [manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:parameters:parameters success:success:success failure:failure:failure];
    
    //[manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:parameters success:success failure:failure];
    
    NSLog(@"requestURL: %@", urlString);
}
*/

/*
 +(void) postDataURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) urlData andSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success andFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
 
 //NSURL *url = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
 
 [parameters setObject:[NSNumber numberWithInt:1] forKey:@"plataforma"];
 [parameters setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forKey:@"versao"];
 
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 if ([defaults objectForKey:@"DeviceToken"]) {
 
 [parameters setObject:[defaults objectForKey:@"DeviceToken"] forKey:@"Identificador"];
 }
 
 if ([defaults objectForKey:@"Token"]) {
 
 [parameters setObject:[defaults objectForKey:@"Token"] forKey:@"Token"];
 }
 
 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 
 if ([defaults objectForKey:@"DeviceToken"]) {
 
 [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
 }
 
 if ([defaults objectForKey:@"Token"]) {
 
 [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
 }
 
 [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
 
 //NSURL *filePath = [NSURL fileURLWithPath:pathData];
 [manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
 [formData appendPartWithFileURL:urlData name:@"data" error:nil];
 } success:success failure:failure];
 
 NSLog(@"requestURL: %@", urlString);
 }
*/

+(NSDictionary *) setParametersInternalValues:(NSMutableDictionary *) parameters {
    
    [parameters setObject:[NSNumber numberWithInt:1] forKey:@"plataforma"];
    [parameters setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forKey:@"versao"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [parameters setObject:[defaults objectForKey:@"DeviceToken"] forKey:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [parameters setObject:[defaults objectForKey:@"DeviceToken"] forKey:@"DeviceToken"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [parameters setObject:[defaults objectForKey:@"Token"] forKey:@"Token"];
    }
    
    NSString *locale = [[NSLocale autoupdatingCurrentLocale] localeIdentifier];
    NSString *locale2 = [[NSLocale currentLocale] localeIdentifier];
    NSString *preferredLang = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"current locale: %@", locale);
    NSLog(@"current locale: %@", locale2);
    NSLog(@"current locale: %@", preferredLang);
    
    [parameters setObject:preferredLang forKey:@"Idioma"];
    
    [parameters setObject:[NSNumber numberWithInt:NUMERO_ITENS_POR_PAGINA] forKey:@"limit"];
    
    return parameters;
}

+(void) requestURLComplex:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure {
    
    [NSRequestManager requestURLComplex:urlString andParameters:parameters andButton:nil andSuccess:success andFailure:failure];
}

+(void) requestURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure {

    [NSRequestManager requestURL:urlString andParameters:parameters andButton:nil andSuccess:success andFailure:failure];
}

+(void) requestURLComplex:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andButton:(UIButton *)button andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure {
    
    if (button) {
        
        [button setEnabled:NO];
    }
    NSDictionary *internalParameters = [NSDictionary dictionaryWithDictionary:[NSRequestManager setParametersInternalValues:parameters]];
    
    //AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPSessionManager *manager =[[NSRequestManager shared] manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer =[AFJSONRequestSerializer serializer];
    //Server retornado tipo errado, para resolver segue abaixo solução alternativa
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"DeviceToken"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
    }
    
    [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
    
    NSLog(@"Parameters: %@", internalParameters);
    
    NSURLSessionDataTask *task = [manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:internalParameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Data: %@", responseObject);
        
        if (button) {
            
            [button setEnabled:YES];
        }
        success(task, responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        if (button) {
            
            [button setEnabled:YES];
        }
        failure(task, error);
    }];
    
    NSLog(@"Task: %@", [task description]);
    
    //[manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:parameters success:success failure:failure];
    
    NSLog(@"requestURL: %@", urlString);
}

+(void) requestURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andButton:(UIButton *)button andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure {
    
    if (button) {
        
        [button setEnabled:NO];
    }
    NSDictionary *internalParameters = [NSDictionary dictionaryWithDictionary:[NSRequestManager setParametersInternalValues:parameters]];
    
    //AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPSessionManager *manager =[[NSRequestManager shared] manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    //Server retornado tipo errado, para resolver segue abaixo solução alternativa
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"DeviceToken"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
    }
    
    [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
    
    NSLog(@"Parameters: %@", internalParameters);
    
    NSURLSessionDataTask *task = [manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:internalParameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Data: %@", responseObject);
        
        if (button) {
            
            [button setEnabled:YES];
        }
        success(task, responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        if (button) {
            
            [button setEnabled:YES];
        }
        failure(task, error);
    }];
    
    NSLog(@"Task: %@", [task description]);
    
    //[manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:parameters success:success failure:failure];
    
    NSLog(@"requestURL: %@", urlString);
}

+(void) postDataURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) urlData andSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject)) success andFailure:(void (^)(NSURLSessionDataTask *task, NSError *error)) failure {
    
    //NSURL *url = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSDictionary *internalParameters = [NSDictionary dictionaryWithDictionary:[NSRequestManager setParametersInternalValues:parameters]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
    }
    
    [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
    
    //NSURL *filePath = [NSURL fileURLWithPath:pathData];
    [manager POST:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:internalParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:urlData name:@"data" error:nil];
    } success:success failure:failure];
    
    NSLog(@"requestURL: %@", urlString);
}

+(void) uploadTaskURL:(NSString *) urlString andParameters:(NSMutableDictionary *) parameters andData:(NSURL *) urlData andFile:(void (^)(id<AFMultipartFormData> formData)) file andProgess:(void (^)(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend)) aProgress andSuccess:(void (^)(NSURLResponse *response, id responseObject, NSError *error)) success andFailure:(void (^)(NSError *error)) failure {
    
    NSDictionary *internalParameters = [NSDictionary dictionaryWithDictionary:[NSRequestManager setParametersInternalValues:parameters]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"DeviceToken"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"DeviceToken"] forHTTPHeaderField:@"Identificador"];
    }
    
    if ([defaults objectForKey:@"Token"]) {
        
        [manager.requestSerializer setValue:[defaults objectForKey:@"Token"] forHTTPHeaderField:@"Token"];
    }
    
    [manager.requestSerializer setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] forHTTPHeaderField:@"versao"];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[[URL_SERVER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAppendingString:urlString] parameters:internalParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileURL:urlData name:@"data" error:nil];
        //file(formData);
        //[formData appendPartWithFileURL:[NSURL fileURLWithPath:@"file://path/to/image.jpg"] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
        
    } error:nil];
    
    /*
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Upload Complete");
    }
    */

    [manager setTaskDidSendBodyDataBlock:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        NSLog(@"Progress.... %lld",100/totalBytesExpectedToSend*totalBytesSent);
        aProgress(session, task, bytesSent, totalBytesSent, totalBytesExpectedToSend);
    }];
    
    NSProgress *progress = nil;
    
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            
            NSLog(@"Error: %@", error);
            failure(error);
        }
        else {
            
            NSLog(@"%@ %@", response, responseObject);
            success(response, responseObject, error);
        }
    }];
    
    [uploadTask resume];
}

@end

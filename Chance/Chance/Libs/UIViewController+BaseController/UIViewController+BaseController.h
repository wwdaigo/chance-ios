//
//  UIViewController+BaseController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 21/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UIViewController_BaseController)

- (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay;

@end
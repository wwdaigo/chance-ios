//
//  UIViewController+BaseController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 21/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "UIViewController+BaseController.h"

@implementation UIViewController (UIViewController_BaseController)

- (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay {
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

@end
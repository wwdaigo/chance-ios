#import "NSNotificationView.h"

#define AL_IOS_7_OR_GREATER [UIDevice iOSVersion] >= 7.0

@interface UIDevice (NSNotificationSystemVersion)

+ (float)iOSVersion;

@end

@interface UIApplication (NSNotificationApplicationBarHeights)

+ (CGFloat)navigationBarHeight;
+ (CGFloat)statusBarHeight;

@end

@class NSNotificationView;
@protocol NSNotificationViewDelegate <NSObject>
@required
- (void)showAlertBanner:(NSNotificationView *)alertBanner hideAfter:(NSTimeInterval)delay;
- (void)hideAlertBanner:(NSNotificationView *)alertBanner forced:(BOOL)forced;
- (void)alertBannerWillShow:(NSNotificationView *)alertBanner inView:(UIView *)view;
- (void)alertBannerDidShow:(NSNotificationView *)alertBanner inView:(UIView *)view;
- (void)alertBannerWillHide:(NSNotificationView *)alertBanner inView:(UIView *)view;
- (void)alertBannerDidHide:(NSNotificationView *)alertBanner inView:(UIView *)view;
@end

@interface NSNotificationView ()

@property (nonatomic, weak) id <NSNotificationViewDelegate> delegate;
@property (nonatomic) BOOL isScheduledToHide;
@property (nonatomic, copy) void(^tappedBlock)(NSNotificationView *alertBanner);
@property (nonatomic) NSTimeInterval fadeInDuration;
@property (nonatomic) BOOL showShadow;
@property (nonatomic) BOOL shouldForceHide;

- (void)showAlertBanner;
- (void)hideAlertBanner;
- (void)pushAlertBanner:(CGFloat)distance forward:(BOOL)forward delay:(double)delay;
- (void)updateSizeAndSubviewsAnimated:(BOOL)animated;
- (void)updatePositionAfterRotationWithY:(CGFloat)yPos animated:(BOOL)animated;
- (id)nextAvailableViewController:(id)view;

@end
#import <UIKit/UIKit.h>

static CGFloat const kNSNotificationAlertViewHeight = 50.0f;
static CGFloat const kNSNotificationAlertViewSymbolViewSidelength = 44.0f;
static NSTimeInterval const kNSNotificationAlertViewDefaultShowDuration = 2.0;

typedef enum {
    NSNotificationAlertViewStyleSuccess,
    NSNotificationAlertViewStyleError,
    NSNotificationAlertViewStyleProgress,
} NSNotificationAlertViewStyle;

typedef void(^CSVoidBlock)();

@interface NSNotificationAlertView : UIView

#pragma mark + quick presentation

+ (void)showInViewController:(UIViewController*)viewController
             style:(NSNotificationAlertViewStyle)style
           message:(NSString*)message;

+ (void)showInViewController:(UIViewController*)viewController
         tintColor:(UIColor*)tintColor
             image:(UIImage*)image
           message:(NSString*)message
          duration:(NSTimeInterval)duration;

+ (void)showInViewController:(UIViewController*)viewController
                   tintColor:(UIColor*)tintColor
                        font:(UIFont*)font
               textAlignment:(NSTextAlignment)textAlignment
                       image:(UIImage*)image
                     message:(NSString*)message
                    duration:(NSTimeInterval)duration;

#pragma mark + creators

+ (NSNotificationAlertView*)notificationViewWithParentViewController:(UIViewController*)viewController
                                                      tintColor:(UIColor*)tintColor
                                                          image:(UIImage*)image
                                                        message:(NSString*)message;

+ (NSNotificationAlertView*)notificationViewWithParentViewController:(UIViewController*)viewController
                                                           tintColor:(UIColor*)tintColor
                                                             title:(NSString*)title;
#pragma mark - initialization

- (instancetype)initWithParentViewController:(UIViewController*)viewController;

#pragma mark - presentation

- (void)setVisible:(BOOL)showing animated:(BOOL)animated completion:(void (^)())completion;
- (void)dismissWithStyle:(NSNotificationAlertViewStyle)style message:(NSString*)message duration:(NSTimeInterval)duration animated:(BOOL)animated;
@property (readonly, nonatomic, getter = isShowing) BOOL visible;

#pragma mark - visible properties

/**
 The image property should be used for setting the image displayed in imageView
 Only the alpha value will be used and then be tinted to a 'legible' color
 */
@property (nonatomic, strong) UIImage* image;

@property (nonatomic, strong) UIColor* tintColor;

@property (nonatomic, getter = isShowingActivity) BOOL showingActivity;

@property (nonatomic, copy) CSVoidBlock tapHandler;

@property (nonatomic, strong) IBOutlet UIProgressView *uipvProgressBar;
@property (nonatomic, strong) IBOutlet UILabel *uilProgress;

@end

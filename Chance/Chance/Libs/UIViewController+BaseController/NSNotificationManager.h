#import <Foundation/Foundation.h>

@interface NSNotificationManager : NSObject

+ (NSNotificationManager *)sharedManager;

- (NSArray *)alertNotificationsInView:(UIView *)view;
- (void)hideAllAlertNotifications;
- (void)hideAlertNotificationsInView:(UIView *)view;
- (void)forceHideAllAlertNotificationsInView:(UIView *)view;

@end

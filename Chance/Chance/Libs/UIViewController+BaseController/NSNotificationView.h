//
//  NSNotificationView.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 21/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NSNotificationStyleSuccess = 0,
    NSNotificationStyleFailure,
    NSNotificationStyleNotify,
    NSNotificationStyleWarning,
    NSNotificationStyleProgress,
} NSNotificationStyle;

typedef enum {
    NSNotificationPositionTop = 0,
    NSNotificationPositionBottom,
    NSNotificationPositionUnderNavBar,
} NSNotificationPosition;

typedef enum {
    NSNotificationStateShowing = 0,
    NSNotificationStateHiding,
    NSNotificationStateMovingForward,
    NSNotificationStateMovingBackward,
    NSNotificationStateVisible,
    NSNotificationStateHidden
} NSNotificationState;

@interface NSNotificationView : UIView

@property (nonatomic, readonly) NSNotificationStyle style;
@property (nonatomic, readonly) NSNotificationPosition position;
@property (nonatomic, readonly) NSNotificationState state;

/**
 Length of time in seconds that a banner should show before auto-hiding.
 
 Default value is 3.5 seconds. A value == 0 will disable auto-hiding.
 */
@property (nonatomic) NSTimeInterval secondsToShow;

/**
 Tapping on a banner will immediately dismiss it.
 
 Default value is YES. If you supply a tappedHandler in one of the appropriate methods, this will be set to NO for that specific banner.
 */
@property (nonatomic) BOOL allowTapToDismiss;

/**
 The length of time it takes a banner to transition on-screen.
 
 Default value is 0.25 seconds.
 */
@property (nonatomic) NSTimeInterval showAnimationDuration;

/**
 The length of time it takes a banner to transition off-screen.
 
 Default value is 0.2 seconds.
 */
@property (nonatomic) NSTimeInterval hideAnimationDuration;

/**
 Banner opacity, between 0 and 1.
 
 Default value is 0.93f.
 */
@property (nonatomic) CGFloat bannerOpacity;

/**
 The default methods to customize and display a banner.
 */
+ (NSNotificationView *)alertBannerForView:(UIView *)view style:(NSNotificationStyle)style position:(NSNotificationPosition)position title:(NSString *)title;

+ (NSNotificationView *)alertBannerForView:(UIView *)view style:(NSNotificationStyle)style position:(NSNotificationPosition)position title:(NSString *)title subtitle:(NSString *)subtitle;

/**
 Optional method to handle a tap on a banner.
 
 By default, supplying a tap handler will disable allowTapToDismiss on this particular banner. If you want to reinstate this behavior alongside the tap handler, you can call `[[NSNotificationManager sharedManager] hideAlertBanner:alertBanner];` in tappedBlock().
 */
+ (NSNotificationView *)alertBannerForView:(UIView *)view style:(NSNotificationStyle)style position:(NSNotificationPosition)position title:(NSString *)title subtitle:(NSString *)subtitle tappedBlock:(void(^)(NSNotificationView *alertNotification))tappedBlock;

/**
 Show the alert banner
 */
- (void)show;

/**
 Immediately hide this alert banner, forgoing the secondsToShow value.
 */
- (void)hide;

/**
 Returns an array of all banners within a certain view.
 */
+ (NSArray *)alertBannersInView:(UIView *)view;

/**
 Immediately hides all alert banners in all views, forgoing their secondsToShow values.
 */
+ (void)hideAllAlertBanners;

/**
 Immediately hides all alert banners in a certain view, forgoing their secondsToShow values.
 */
+ (void)hideAlertBannersInView:(UIView *)view;

/**
 Immediately force hide all alert banners, forgoing their dismissal animations. Call this in viewWillDisappear: of your view controller if necessary.
 */
+ (void)forceHideAllAlertBannersInView:(UIView *)view;

@end
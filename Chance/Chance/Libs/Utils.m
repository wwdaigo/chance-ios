//
//  Utils.m
//  ConectCar
//
//  Created by Luiz Carlos Sant'Ana Junior on 03/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "Utils.h"

#import <objc/message.h>
#import <objc/runtime.h>

@implementation Utils

+(BOOL) IsValidEmail:(NSString *)checkString {
    
    BOOL stricterFilter = YES;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

@end

@implementation NSNumber (NSNumberUtils)

- (NSString *) toStringFormatCurrecy {
    
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyStyle setCurrencySymbol:@"R$ "];
    [currencyStyle setNegativeFormat:@"R$ -#,##0.00"];
    
    return [currencyStyle stringFromNumber:self];
}

- (NSString *) toStringFormatNumber {
    
    NSNumberFormatter *numberStyle = [[NSNumberFormatter alloc] init];
    
    return [numberStyle stringFromNumber:self];
}

@end

@implementation UIImageView (_UIImageViewUtils)

@dynamic af_imageRequestOperation;

@end

@implementation UIImageView (UIImageViewUtils)

- (NSString *)cachedPNGFilePathForURL:(NSString *)url {
    
    NSString *fileName = [url lastPathComponent];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *pathExtension = [fileName pathExtension];
    NSRange extensionRange = [fileName rangeOfString:pathExtension];
    NSString *cachedFileName = [fileName stringByReplacingCharactersInRange:extensionRange withString:@"png"];
    NSString *cachedFilePath = [cachePath stringByAppendingPathComponent:cachedFileName];
    
    return cachedFilePath;
}

-(BOOL)isCached:(NSString *)url {
    
    NSString *fileName = [self cachedPNGFilePathForURL:url];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileName]) {
    
        return YES;
    }
    
    return NO;
}

-(void)setImageWithURLString:(NSString *) urlString imageCache:(UIImage *) imageCache {
    
    NSString *cachedFilePath = [self cachedPNGFilePathForURL:urlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error = nil;
        
        self.image = imageCache;
        [UIImagePNGRepresentation(imageCache) writeToFile:cachedFilePath options:NSDataWritingAtomic error:&error];
        
        if (error) {
            
            NSLog(@"error: %@", error.localizedDescription);
        }
    });
}

-(void)setImageWithURLString:(NSString *) urlString placeholderImage:(UIImage *) placeholderImage {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *filePath = [self cachedPNGFilePathForURL:urlString];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            
            UIImage *image = [UIImage imageWithContentsOfFile:filePath];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                self.image = image;
            });
        }
        else {
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                self.image = placeholderImage;
            });
            
            NSString *filePath = [self cachedPNGFilePathForURL:[NSURL URLWithString:urlString]];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [self setImageWithURLRequest:urlRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                
                self.image = image;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    NSError *error = nil;
                    [UIImagePNGRepresentation(image) writeToFile:filePath options:NSDataWritingAtomic error:&error];

                    if (error) {
                        
                        NSLog(@"error: %@", error.localizedDescription);
                    }
                });
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
                self.image = placeholderImage;
            }];
        }
    });
}

- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
         downloadProgressBlock:(void (^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))downloadProgressBlock

{
    
    
    [self cancelImageRequestOperation];
    
    // get AFNetworking UIImageView cache
    NSCache * cache =  (NSCache *)objc_msgSend([self class], @selector(sharedImageCache));
    // try to get the image from cache
    //UIImage * cachedImage = objc_msgSend(cache, @selector(cachedImageForRequest:), urlRequest);
    
    UIImage * cachedImage =  (UIImage *)objc_msgSend(cache, @selector(cachedImageForRequest:), urlRequest);
    //UIImage * cachedImage = ((NSCache(*)(id, SEL, id))objc_msgSend)(cache, @selector(cachedImageForRequest:), urlRequest);
    
    //[[self sharedImageCache]
    
    if (cachedImage) {
        
        self.af_imageRequestOperation = nil;
        if (success) {
            
            success(nil, nil, cachedImage);
        }
        else {
        
            self.image = cachedImage;
        }
    }
    else {
    
        if (placeholderImage) {
            
            self.image = placeholderImage;
        }

        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
		
#ifdef _AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES_
		requestOperation.allowsInvalidSSLCertificate = YES;
#endif
		requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([urlRequest isEqual:[self.af_imageRequestOperation request]]) {
                if (self.af_imageRequestOperation == operation) {
                    self.af_imageRequestOperation = nil;
                }
                
                if (success) {
                    success(operation.request, operation.response, responseObject);
                } else if (responseObject) {
                    self.image = responseObject;
                }
            }
            // cache the image recently fetched.
            //objc_msgSend(cache, @selector(cacheImage:forRequest:), responseObject, urlRequest);
            //(void(*)objc_msgSend)(cache, @selector(cachedImageForRequest:), urlRequest);
             ((void(*)(id, SEL, id))objc_msgSend)(cache,@selector(cachedImageForRequest:), urlRequest);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if ([urlRequest isEqual:[self.af_imageRequestOperation request]]) {
                if (self.af_imageRequestOperation == operation) {
                    self.af_imageRequestOperation = nil;
                }
                
                if (failure) {
                    failure(operation.request, operation.response, error);
                }
            }
        }];
        
        if (downloadProgressBlock){
            [requestOperation setDownloadProgressBlock:downloadProgressBlock];
        }
        self.af_imageRequestOperation = requestOperation;
        // get the NSOperation associated to UIImageViewClass
        NSOperationQueue * operationQueue =  (NSOperationQueue *)objc_msgSend([self class], @selector(af_sharedImageRequestOperationQueue));
        [operationQueue addOperation:self.af_imageRequestOperation];
    }
}

@end

#define kXLCircleProgressIndicatorDefaultStrokeProgressColor      [UIColor colorWithRed:5.0f/255.0f green:54.0f/255.0f blue:24.0f/255.0f alpha:1.0f]
#define kXLCircleProgressIndicatorDefaultStrokeRemainingColor     [[UIColor colorWithRed:5.0f/255.0f green:54.0f/255.0f blue:24.0f/255.0f alpha:1.0f] colorWithAlphaComponent:0.1f]
#define kXLCircleProgressIndicatorDefaultStrokeWidthRatio         0.10
/*
 #define kXLCircleProgressIndicatorDefaultStrokeProgressColor      [UIColor redColor]
 #define kXLCircleProgressIndicatorDefaultStrokeRemainingColor     [[UIColor redColor] colorWithAlphaComponent:0.1f]
 #define kXLCircleProgressIndicatorDefaultStrokeWidthRatio         0.10
 */
@interface XLCircleProgressIndicator ()
@end

@implementation XLCircleProgressIndicator

@synthesize strokeWidth          = _strokeWidth;
@synthesize strokeProgressColor  = _strokeProgressColor;
@synthesize strokeRemainingColor = _strokeRemainingColor;
@synthesize strokeWidthRatio     = _strokeWidthRatio;
@synthesize progressValue        = _progressValue;


-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDefaultValues];
    }
    return self;
}

-(void)setDefaultValues {
    
    self.backgroundColor    = [UIColor clearColor];
    _strokeProgressColor    = kXLCircleProgressIndicatorDefaultStrokeProgressColor;
    _strokeRemainingColor   = kXLCircleProgressIndicatorDefaultStrokeRemainingColor;
    _strokeWidthRatio       = kXLCircleProgressIndicatorDefaultStrokeWidthRatio;
}


#pragma mark - Properties

-(void)setProgressValue:(float)progressValue {
    
    if (progressValue < 0.0) progressValue = 0.0;
    if (progressValue > 1.0) progressValue = 1.0;
    _progressValue = progressValue;
    [self setNeedsDisplay];
}

-(void)setStrokeWidth:(CGFloat)strokeWidth {
    
    _strokeWidthRatio = -1.0;
    _strokeWidth = strokeWidth;
    [self setNeedsDisplay];
}

-(void)setStrokeWidthRatio:(CGFloat)strokeWidthRatio {
    
    _strokeWidth = -1.0;
    _strokeWidthRatio = strokeWidthRatio;
    [self setNeedsDisplay];
}

-(void)setStrokeProgressColor:(UIColor *)strokeProgressColor {
    
    _strokeProgressColor = strokeProgressColor;
    [self setNeedsDisplay];
}

-(void)setStrokeRemainingColor:(UIColor *)strokeRemainingColor {
    
    _strokeRemainingColor = strokeRemainingColor;
    [self setNeedsDisplay];
}


#pragma mark - draw progress indicator view

-(void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    float minSize =  MIN(rect.size.width, rect.size.height);
    float lineWidth = _strokeWidth;
    if(lineWidth == -1.0) lineWidth = minSize * _strokeWidthRatio;
    float radius = (minSize - lineWidth) / 2.0;
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, center.x, center.y);
    CGContextRotateCTM(context, -M_PI * 0.5);
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextBeginPath(context);
    CGContextAddArc(context, 0, 0, radius, 0, 2 * M_PI, 0);
    CGContextSetStrokeColorWithColor(context, _strokeRemainingColor.CGColor);
    CGContextStrokePath(context);
    CGContextBeginPath(context);
    CGContextAddArc(context, 0, 0, radius, 0, M_PI * (_progressValue * 2), 0);
    CGContextSetStrokeColorWithColor(context, _strokeProgressColor  .CGColor);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

@end

static char kXLImageProgressIndicatorKey;

@interface UIImageView (_XLProgressIndicator)

@property (readwrite, nonatomic, strong, setter = xl_setProgressIndicatorView:) XLCircleProgressIndicator *xl_progressIndicatorView;

@end

@implementation UIImageView (_XLProgressIndicator)

@dynamic xl_progressIndicatorView;

@end

@implementation UIImageView (XLProgressIndicator)

-(XLCircleProgressIndicator *)progressIndicatorView
{
    return [self xl_progressIndicatorView];
}


-(void)xl_setProgressIndicatorView:(XLCircleProgressIndicator *)xl_progressIndicatorView
{
    objc_setAssociatedObject(self, &kXLImageProgressIndicatorKey, xl_progressIndicatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(XLCircleProgressIndicator *)xl_progressIndicatorView
{
    XLCircleProgressIndicator * progressIndicator = (XLCircleProgressIndicator *)objc_getAssociatedObject(self, &kXLImageProgressIndicatorKey);
    if (progressIndicator) return progressIndicator;
    CGRect frame = CGRectMake(0, 0, MIN(100, self.bounds.size.width), MIN(100, self.bounds.size.height));
    progressIndicator = [[XLCircleProgressIndicator alloc] initWithFrame:frame];
    progressIndicator.center = self.center;
    progressIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self xl_setProgressIndicatorView:progressIndicator];
    return progressIndicator;
}


-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url
{
    [self setImageWithProgressIndicatorAndURL:url placeholderImage:nil];
}

-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url
                          placeholderImage:(UIImage *)placeholderImage
{
    [self setImageWithProgressIndicatorAndURL:url placeholderImage:placeholderImage imageDidAppearBlock:nil];
}

-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url
                          placeholderImage:(UIImage *)placeholderImage
                       imageDidAppearBlock:(void (^)(UIImageView *))imageDidAppearBlock
{
    [self setImage:nil];
    [self.xl_progressIndicatorView setProgressValue:0.0f];
    if (![self.xl_progressIndicatorView superview]){
        [self addSubview:self.xl_progressIndicatorView];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    __typeof__(self) __weak weakSelf = self;
    [self setImageWithURLRequest:request
                placeholderImage:placeholderImage
                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                             [weakSelf.xl_progressIndicatorView removeFromSuperview];
                             weakSelf.image = image;
                             if (imageDidAppearBlock){
                                 imageDidAppearBlock(weakSelf);
                             }
                         }
                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                             [weakSelf.xl_progressIndicatorView setProgressValue:0.0f];
                         }
           downloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
               float newValue = ((float)totalBytesRead / totalBytesExpectedToRead);
               [weakSelf.xl_progressIndicatorView setProgressValue:newValue];
           }];
}

@end

@implementation UITableView (UITableViewUtils)

-(void)deleteTableViewCell:(NSIndexPath *) indexPath andList:(NSMutableArray *) list{
    
    @try {
        
        [self beginUpdates];
        
        NSLog(@"deleteTableViewCell= %i", [list count]);
        [list removeObjectAtIndex:indexPath.row];
        NSLog(@"deleteTableViewCell= %i", [list count]);
        [self deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        NSLog(@"deleteTableViewCell= %i", [list count]);
        [self endUpdates];
    }
    @catch (NSException *exception) {
        
        //[self reloadData];
    }
    @finally {
    }
    
}

@end
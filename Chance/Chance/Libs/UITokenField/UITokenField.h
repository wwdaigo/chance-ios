#import <UIKit/UIKit.h>

@class UITokenField, UIToken;

//==========================================================
#pragma mark - Delegate Methods -
//==========================================================
@protocol UITokenFieldDelegate <UITextFieldDelegate>
@optional
- (BOOL)tokenField:(UITokenField *)tokenField willAddToken:(UIToken *)token;
- (void)tokenField:(UITokenField *)tokenField didAddToken:(UIToken *)token;
- (BOOL)tokenField:(UITokenField *)tokenField willRemoveToken:(UIToken *)token;
- (void)tokenField:(UITokenField *)tokenField didRemoveToken:(UIToken *)token;

- (BOOL)tokenField:(UITokenField *)field shouldUseCustomSearchForSearchString:(NSString *)searchString;
- (void)tokenField:(UITokenField *)field performCustomSearchForSearchString:(NSString *)searchString withCompletionHandler:(void (^)(NSArray *results))completionHandler;

- (void)tokenField:(UITokenField *)tokenField didFinishSearch:(NSArray *)matches;
- (NSString *)tokenField:(UITokenField *)tokenField displayStringForRepresentedObject:(id)object;
- (NSString *)tokenField:(UITokenField *)tokenField searchResultStringForRepresentedObject:(id)object;
- (NSString *)tokenField:(UITokenField *)tokenField searchResultSubtitleForRepresentedObject:(id)object;
- (UITableViewCell *)tokenField:(UITokenField *)tokenField resultsTableView:(UITableView *)tableView cellForRepresentedObject:(id)object;
- (CGFloat)tokenField:(UITokenField *)tokenField resultsTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface UITokenFieldInternalDelegate : NSObject <UITextFieldDelegate>
@end

//==========================================================
#pragma mark - UITokenFieldView -
//==========================================================
@interface UITokenFieldView : UIScrollView <UITableViewDelegate, UITableViewDataSource, UITokenFieldDelegate>
@property (nonatomic, assign) BOOL showAlreadyTokenized;
@property (nonatomic, assign) BOOL searchSubtitles;
@property (nonatomic, assign) BOOL forcePickSearchResult;
@property (nonatomic, assign) BOOL shouldSortResults;
@property (nonatomic, assign) BOOL shouldSearchInBackground;
@property (nonatomic, readonly) UITokenField * tokenField;
@property (nonatomic, readonly) UIView * separator;
@property (nonatomic, readonly) UITableView * resultsTable;
@property (nonatomic, readonly) UIView * contentView;
@property (nonatomic, copy) NSArray * sourceArray;
@property (weak, nonatomic, readonly) NSArray * tokenTitles;

- (void)updateContentSize;

@end

//==========================================================
#pragma mark - UITokenField -
//==========================================================
typedef enum {
    
	UITokenFieldControlEventFrameWillChange = 1 << 24,
	UITokenFieldControlEventFrameDidChange = 1 << 25,
} UITokenFieldControlEvents;

@interface UITokenField : UITextField
@property (nonatomic, weak) id <UITokenFieldDelegate> delegate;
@property (weak, nonatomic, readonly) NSArray * tokens;
@property (weak, nonatomic, readonly) UIToken * selectedToken;
@property (weak, nonatomic, readonly) NSArray * tokenTitles;
@property (weak, nonatomic, readonly) NSArray * tokenObjects;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) BOOL resultsModeEnabled;
@property (nonatomic, assign) BOOL removesTokensOnEndEditing;
@property (nonatomic, readonly) int numberOfLines;
@property (nonatomic) int tokenLimit;
@property (nonatomic, strong) NSCharacterSet * tokenizingCharacters;

- (void)addToken:(UIToken *)title;
- (UIToken *)addTokenWithTitle:(NSString *)title;
- (UIToken *)addTokenWithTitle:(NSString *)title representedObject:(id)object;
- (void)addTokensWithTitleList:(NSString *)titleList;
- (void)addTokensWithTitleArray:(NSArray *)titleArray;
- (void)removeToken:(UIToken *)token;
- (void)removeAllTokens;

- (void)selectToken:(UIToken *)token;
- (void)deselectSelectedToken;

- (void)tokenizeText;

- (void)layoutTokensAnimated:(BOOL)animated;
- (void)setResultsModeEnabled:(BOOL)enabled animated:(BOOL)animated;

// Pass nil to hide label
- (void)setPromptText:(NSString *)aText;

@end

//==========================================================
#pragma mark - UIToken -
//==========================================================
typedef enum {
	UITokenAccessoryTypeNone = 0, // Default
	UITokenAccessoryTypeDisclosureIndicator = 1,
} UITokenAccessoryType;

@interface UIToken : UIControl
@property (nonatomic, copy) NSString * title;
@property (nonatomic, strong) id representedObject;
@property (nonatomic, strong) UIFont * font;
@property (nonatomic, strong) UIColor * textColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor * highlightedTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor * tintColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) UITokenAccessoryType accessoryType;
@property (nonatomic, assign) CGFloat maxWidth;

- (instancetype)initWithTitle:(NSString *)aTitle;
- (instancetype)initWithTitle:(NSString *)aTitle representedObject:(id)object;
- (instancetype)initWithTitle:(NSString *)aTitle representedObject:(id)object font:(UIFont *)aFont;

+ (UIColor *)blueTintColor;
+ (UIColor *)redTintColor;
+ (UIColor *)greenTintColor;

@end

//
//  Utils.h
//  ConectCar
//
//  Created by Luiz Carlos Sant'Ana Junior on 03/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking/AFNetworking.h>

@interface Utils : NSObject

+(BOOL) IsValidEmail:(NSString *)checkString;

@end

@interface NSNumber (NSNumberUtils)

- (NSString *) toStringFormatCurrecy;

@end

@interface UIImageView (_UIImageViewUtils)

@property (readwrite, nonatomic, strong, setter = af_setImageRequestOperation:) AFHTTPRequestOperation *af_imageRequestOperation;

@end

@interface UIImageView (UIImageViewUtils)

-(void)setImageWithURLString:(NSString *) urlString imageCache:(UIImage *) imageCache;
-(void)setImageWithURLString:(NSString *) urlString placeholderImage:(UIImage *) placeholderImage;
-(void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
         downloadProgressBlock:(void (^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))downloadProgressBlock;
@end

@interface XLCircleProgressIndicator : UIView

@property (nonatomic) UIColor * strokeProgressColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor * strokeRemainingColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat strokeWidthRatio UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat strokeWidth UI_APPEARANCE_SELECTOR;

@property (nonatomic) float progressValue;

@end

@interface UIImageView (XLProgressIndicator)

@property (nonatomic, readonly) XLCircleProgressIndicator * progressIndicatorView;

-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url;
-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url
                          placeholderImage:(UIImage *)placeholderImage;
-(void)setImageWithProgressIndicatorAndURL:(NSURL *)url
                          placeholderImage:(UIImage *)placeholderImage
                       imageDidAppearBlock:(void(^)(UIImageView * imageView))imageDidAppearBlock;
@end

@interface UITableView (UITableViewUtils)

-(void)deleteTableViewCell:(NSIndexPath *) indexPath andList:(NSMutableArray *) list;

@end
//
//  QuemContagiouViewController.h
//  Chance
//
//  Created by pedro henrique borges on 12/17/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuemContagiouDelegate <NSObject>

-(void)redirectToPerfil:(NSDictionary *)perfil;

@end

@interface QuemContagiouViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UITableView *tv_contagios;
    
}
@property (nonatomic,strong) NSNumber *idPost;

@property (nonatomic,strong) NSArray *pessoas;

@property(nonatomic,strong) UIViewController *parent;

@property(nonatomic,weak) id<QuemContagiouDelegate> delegate;

@end

//
//  NewPedidoAjudaViewController.h
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filtro.h"

@interface NewPedidoAjudaViewController : BaseViewController
{
    IBOutlet UILabel *uilbQtdSolicitacoes;
    
}

@property(nonatomic,strong) Filtro *filtro;

@property(nonatomic, strong) NSMutableArray *categoriasList;
@property(nonatomic, strong) NSMutableArray *ajudaOferecidaPedirAjudaList;
@property(nonatomic, strong) NSMutableArray *minhasOfertasPedirAjudaList;

@property(nonatomic, strong) NSMutableArray *ajudaOferecidaOferecerAjudaList;
@property(nonatomic, strong) NSMutableArray *minhasOfertasOferecidoAjudaList;


@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *localizacaoAtual;
@property(nonatomic, strong) NSString *cidadeAtual;

@property(nonatomic,strong) NSMutableArray *pedidosAjudaList;

@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end

//
//  QuemContagiouViewController.m
//  Chance
//
//  Created by pedro henrique borges on 12/17/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "QuemContagiouViewController.h"
#import "VerPerfilViewController.h"

@interface QuemContagiouViewController ()

@end

@implementation QuemContagiouViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self carregaQuemContagiou];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)carregaQuemContagiou{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [self startActivityIndicator];
    
    
            
    [parameters setObject:self.idPost forKey:@"PostId"];
    
    [NSRequestManager requestURL:@"Post/QuemContagiou" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
        
            self.pessoas = [NSMutableArray arrayWithArray:[result objectForKey:@"Contagiou"]];
            
            [tv_contagios reloadData];
            
            
        }
        
        [self stopActivityIndicator];
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self stopActivityIndicator];
        
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.pessoas.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contagiou Cell" forIndexPath:indexPath];
    
    UIImageView *image =   (UIImageView *)[cell viewWithTag:1000];
    UILabel *lbl = (UILabel *)[cell viewWithTag:1001];
    
    
    
    NSDictionary *autor = [[self.pessoas objectAtIndex:indexPath.row] objectForKey:@"Autor"];
    
    if ([[autor objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [image setImageWithURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[[self uiivFoto] setImageWithProgressIndicatorAndURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        //[[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [image setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[[self uiivFoto] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    [lbl setText:[autor objectForKey:@"Apelido"]];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 82.0f;
}

- (IBAction)onFechar:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
     NSDictionary *autor = [[self.pessoas objectAtIndex:indexPath.row] objectForKey:@"Autor"];

        if([[autor objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]]) {

            return;
        }
    
     [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
    
         [self.delegate redirectToPerfil:autor];
     
     }];

    
    
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}


@end

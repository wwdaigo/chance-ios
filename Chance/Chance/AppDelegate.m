//
//  AppDelegate.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "AFNetworkActivityIndicatorManager.h"
#import "AppDelegate.h"
#import "TestFairy.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[CrashlyticsKit]];

    	[TestFairy begin:@"95181e4323664237fd0c373476f134617a73dc89"];
    
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
  
    
    
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:5.0f/255.0f green:54.0f/255.0f blue:24.0f/255.0f alpha:1.0f]];
    
    [[UIRefreshControl appearance] setTintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
    //[[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    //[[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending) {
        
        //[[UINavigationBar appearanceWhenContainedIn:[UINavigationController class], nil] setBackgroundImage:[UIImage imageNamed:@"bg-topo.png"] forBarMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
        
        [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed:17.0f/255.0f green:28.0f/255.0f blue:23.0f/255.0f alpha:1.0f]];
        
        [[UISearchBar appearance] setBackgroundColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
    }
    else {
        
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
        
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
        
        [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:17.0f/255.0f green:28.0f/255.0f blue:23.0f/255.0f alpha:1.0f]];
        
        [[UISearchBar appearance] setBarTintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f]];
        
        //[[UINavigationBar appearance] setBackIndicatorImage:
        //[UIImage imageNamed:@"ico-voltar"]];
        //[[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:
        //[UIImage imageNamed:@"ico-voltar"]];
        
        //[[UIBarButtonItem appearance] setTitle:@" "];
        //[[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:61.0f/255.0f green:105.0f/255.0f blue:36.0f/255.0f alpha:1.0f]];
        
        //NSDictionary *textAttributes = @{:[UIColor colorWithRed:61.0f/255.0f green:105.0f/255.0f blue:36.0f/255.0f alpha:1.0f]};
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:61.0f/255.0f green:105.0f/255.0f blue:36.0f/255.0f alpha:1.0f] forKey:NSForegroundColorAttributeName];
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
        //[[UINavigationBar appearance] set
        
        //[[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
        
        //UIBarButtonItem *theBackButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
        
        //[[UIBarButtonItem appearance] SET:theBackButton];
        //[[UINavigationBar set ] setBackBarButtonItem:theBackButton];
        //[[UINavigationController appearance] setBackBarButtonItem:];
        
        //UIImage *backButtonImage = [[UIImage imageNamed:@"ico-voltar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        //[[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];
        
        //[[UINavigationBar appearanceWhenContainedIn:[UINavigationController class], nil] setBackgroundImage:[UIImage imageNamed:@"bg-topo"] forBarMetrics:UIBarMetricsDefault];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:NO];
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    
    return YES;
}

- (NSUInteger) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    if ([[self.window.rootViewController presentedViewController]
         isKindOfClass:[MPMoviePlayerViewController class]]) {
        
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else {
        
        if ([[self.window.rootViewController presentedViewController]
             isKindOfClass:[UINavigationController class]]) {
            
            UINavigationController *nc = (UINavigationController *)[self.window.rootViewController presentedViewController];
            
            if ([nc.topViewController isKindOfClass:[MPMoviePlayerViewController class]]) {
                
                return UIInterfaceOrientationMaskAllButUpsideDown;
            }
            else if ([[nc.topViewController presentedViewController]
                      isKindOfClass:[MPMoviePlayerViewController class]]) {
                
                return UIInterfaceOrientationMaskAllButUpsideDown;
            }
        }
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark - Push Notifications Delegate

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
	NSLog(@"My token is: %@", deviceToken);
    
    NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"My token is: %@", token);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:token forKey:@"DeviceToken"];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"NãoRegistrado" forKey:@"DeviceToken"];
    [defaults setObject:@"" forKey:@"DeviceToken"];
    
	NSLog(@"Failed to get token, error: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"Received Push: %@", userInfo);
    
    NSString *message = @"";
    id alert = [userInfo objectForKey:@"alert"];
    
    if (!alert) {
        
        message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    }
    else {
        if ([alert isKindOfClass:[NSString class]]) {
            
            message = alert;
        }
        else if ([alert isKindOfClass:[NSDictionary class]]) {
            
            message = [alert objectForKey:@"body"];
        }
    }
    
    if ( application.applicationState == UIApplicationStateActive ) {
        
        ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.window style:ALAlertBannerStyleNotify position:ALAlertBannerPositionUnderNavBar title:NSLocalizedString(@"Notificacao", nil) subtitle:message tappedBlock:^(ALAlertBanner *alertBanner) {
            
            NSLog(@"didReceiveRemoteNotification-tapped!");
            [alertBanner hide];
        }];
        banner.secondsToShow = 8;
        banner.showAnimationDuration = 0.7;
        banner.hideAnimationDuration = 0.7;
        [banner show];
    }
}

@end

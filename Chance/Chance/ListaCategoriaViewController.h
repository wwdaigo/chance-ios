//
//  ListaCategoriaViewController.h
//  Chance
//
//  Created by pedro henrique borges on 11/8/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ListaCategoriaDelegate <NSObject>

-(void)didSelectCategoria:(NSDictionary *)categoria;

-(void)didSelectCategorias:(NSArray *)categorias;



@end

@interface ListaCategoriaViewController : UIViewController{
    
    IBOutlet UITableView *uitbCategorias;
    NSMutableArray *categoriasSelecionadas;
    
}


@property (nonatomic,weak) id<ListaCategoriaDelegate> delegate;

@property (nonatomic,strong) NSMutableArray *categorias;

@property(nonatomic) int type;

@property (nonatomic) BOOL todosSelecionado;

@end

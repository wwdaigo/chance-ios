//
//  FiltroViewController.h
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filtro.h"

@protocol FiltroDelegate <NSObject>

-(void)onCancel;
-(void)onFiltrar:(Filtro *)filtro;


@end

@interface FiltroViewController : UIViewController{
    
    IBOutlet UISegmentedControl *uisgFiltro;
    IBOutlet UIView *viewSegment;
    IBOutlet UISlider *uihsKm;
    IBOutlet UILabel *uilbKm;
    IBOutlet UIButton *uibtCategoria;
    IBOutlet UILabel *uilbCategoria;
}

@property (nonatomic,strong) NSMutableArray *categoriasList;

@property(nonatomic,weak) id<FiltroDelegate> delegate;

@end

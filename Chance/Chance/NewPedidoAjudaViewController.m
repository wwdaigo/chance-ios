//
//  NewPedidoAjudaViewController.m
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "NewPedidoAjudaViewController.h"
#import "FiltroViewController.h"
#import "PedidoAjudaCell.h"
#import "PedidoAjudaViewController.h"
#import "PedirAjudaViewController.h"




NSString *const FiltroSegueId = @"Filtro Segue";

@interface NewPedidoAjudaViewController ()<FiltroDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation NewPedidoAjudaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initGPS];
    
    [self startActivityIndicator];
    [self carregaView];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

    [self startActivityIndicator];
    [self carregaView];

}


-(void)initGPS{
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManager.distanceFilter=kCLDistanceFilterNone;
    
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    
    [self.locationManager startUpdatingLocation];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    
    if ([segue.identifier isEqualToString:FiltroSegueId]) {
        
        UINavigationController *nacController = segue.destinationViewController;

        FiltroViewController *filtroVC = (FiltroViewController *)nacController.topViewController;
        
        filtroVC.delegate =self;
        filtroVC.categoriasList = self.categoriasList;
    }
    

}

-(void)onCancel{
    
    
    if (![self.presentedViewController isBeingDismissed])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
   
}

-(void)carregaView{
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [NSRequestManager requestURL:@"Ajuda/Resumo" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,"Mensagem":null,"Oferecidas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"MinhasOfertas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"Categorias":[{"CategoriaId":1,"Nome":"Emprego","Descricao":"Procure, ofereça , de uma chance"},{"CategoriaId":2,"Nome":"Educação","Descricao":"Livro, aula e conhecimento"},{"CategoriaId":3,"Nome":"Animais domésticos","Descricao":"Para tomar conta, dar comida, desapareceu?"},{"CategoriaId":4,"Nome":"Carona","Descricao":"compartilhe com vizinhos, ajude a melhorar o trânsito"},{"CategoriaId":5,"Nome":"Carro","Descricao":"Quebrou?, bateu?, conserto"},{"CategoriaId":6,"Nome":"Casa","Descricao":"Mudança, instalação, limpeza, manutenção"},{"CategoriaId":7,"Nome":"Saúde","Descricao":"dicas de saúde, ajuda de especialista, urgências"},{"CategoriaId":8,"Nome":"Tecnologia","Descricao":"recuperação de dados, instalação, produtos"},{"CategoriaId":9,"Nome":"Maternidade","Descricao":"Marinheira de primeira viagem?, querendo matar a vontade de ter um filho?"},{"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}]}
             */
            //@property(nonatomic, strong) NSMutableArray *CategoriasPedidoAjudaList;
            
            /*
             AceitasPorOferta =     (
             );
             CategoriasDeOferecimento =     (
             );
             CategoriasDePedido =     (
             );
             Mensagem = "<null>";
             MinhasOfertasPorOferecimento =     (
             );
             MinhasOfertasPorPedido =     (
             );
             OferecidasParaPedido =     (
             );
             */
            
            self.ajudaOferecidaOferecerAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"AceitasPorOferta"]];
            self.minhasOfertasOferecidoAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"MinhasOfertasPorOferecimento"]];
            
            self.ajudaOferecidaPedirAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"OferecidasParaPedido"]];
            self.minhasOfertasPedirAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"MinhasOfertasPorPedido"]];
            self.categoriasList = [NSMutableArray arrayWithArray:[result objectForKey:@"Categorias"]];
            
            
            int total = self.ajudaOferecidaOferecerAjudaList.count + self.ajudaOferecidaPedirAjudaList.count +
            self.minhasOfertasOferecidoAjudaList.count +
            self.minhasOfertasPedirAjudaList.count;
            
            uilbQtdSolicitacoes.text = [NSString stringWithFormat:@"%d",total];
            
            if(self.filtro)
                [self carregaPedidos:self.filtro];
            else
                [self carregaPedidos:nil];
            
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
    
}

-(void)carregaPedidos:(Filtro *)filtro{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    
    if(filtro){
        
        if(filtro.categoriasSelecionadas){
            
            NSMutableArray *categorias = [NSMutableArray arrayWithCapacity:0];
            
            for (NSDictionary *cat in filtro.categoriasSelecionadas) {
                
                NSDictionary *dicCat = @{@"CategoriaId":[cat objectForKey:@"CategoriaId"]};
                
                [categorias addObject:dicCat];
                
            }
            
            
            [parameters setObject:categorias forKey:@"Categorias"];
        }
        else{
            
            [parameters setObject:[NSNumber numberWithInt:0] forKey:@"CategoriaId"];

        }
        
        if (filtro.tipoFiltro == 2) {
            
            //[parameters setObject:[NSNumber numberWithInt:(self.uisDistancia.value)] forKey:@"Raio"];
            [parameters setObject:[NSString stringWithFormat:@"%i", [[NSNumber numberWithFloat:filtro.distancia] integerValue]] forKey:@"Raio"];
            
            if (self.localizacaoAtual) {
                
                [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:self.localizacaoAtual.coordinate.latitude], @"Latitude", [NSNumber numberWithFloat:self.localizacaoAtual.coordinate.longitude], @"Longitude", self.cidadeAtual, @"Descricao", nil]forKey:@"Localizacao"];
            }
        }
    }
    else{
        
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"CategoriaId"];
        
        
    }
    
    [NSRequestManager requestURLComplex:@"Ajuda/PedidosPorCategoria" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,"Mensagem":null,"Oferecidas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"MinhasOfertas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"Categorias":[{"CategoriaId":1,"Nome":"Emprego","Descricao":"Procure, ofereça , de uma chance"},{"CategoriaId":2,"Nome":"Educação","Descricao":"Livro, aula e conhecimento"},{"CategoriaId":3,"Nome":"Animais domésticos","Descricao":"Para tomar conta, dar comida, desapareceu?"},{"CategoriaId":4,"Nome":"Carona","Descricao":"compartilhe com vizinhos, ajude a melhorar o trânsito"},{"CategoriaId":5,"Nome":"Carro","Descricao":"Quebrou?, bateu?, conserto"},{"CategoriaId":6,"Nome":"Casa","Descricao":"Mudança, instalação, limpeza, manutenção"},{"CategoriaId":7,"Nome":"Saúde","Descricao":"dicas de saúde, ajuda de especialista, urgências"},{"CategoriaId":8,"Nome":"Tecnologia","Descricao":"recuperação de dados, instalação, produtos"},{"CategoriaId":9,"Nome":"Maternidade","Descricao":"Marinheira de primeira viagem?, querendo matar a vontade de ter um filho?"},{"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}]}
             */
            //@property(nonatomic, strong) NSMutableArray *CategoriasPedidoAjudaList;
            
            self.pedidosAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"Pedidos"]];
            
            [self.tableView reloadSectionIndexTitles];
            [self.tableView reloadData];
            
         
        }
        
        [self stopActivityIndicator];
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self stopActivityIndicator];
        
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
    
}


-(void)onFiltrar:(Filtro *)filtro{
    
    self.filtro = filtro;
    
    
    if (![self.presentedViewController isBeingDismissed])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self startActivityIndicator];
    [self carregaPedidos:filtro];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [self stopActivityIndicator];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"NaoFoiPossivelAcharLocalizacao", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    [self.locationManager stopUpdatingLocation];
    
    if (!self.localizacaoAtual) {
        
        NSLog(@"didUpdateToLocation: %@", newLocation);
        self.localizacaoAtual = newLocation;
        
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                
                self.cidadeAtual = [NSString stringWithFormat:@"%@, %@",[placemark locality], [placemark administrativeArea]];
                break;
            }
        }];
    }
}


#pragma tableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.pedidosAjudaList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    PedidoAjudaCell *cell = (PedidoAjudaCell *)[tableView dequeueReusableCellWithIdentifier:@"PedidoAjudaCell"];

    [cell setParent:self];
    [cell reloadCell:[self.pedidosAjudaList objectAtIndex:indexPath.row]];
    
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *texto = [[self.pedidosAjudaList objectAtIndex:indexPath.row] objectForKey:@"Texto"];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]};
    
    CGRect titleRect = [texto boundingRectWithSize:(CGSize){(tableView.frame.size.width - 20), 2000.0f} options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    
    CGSize size = titleRect.size;
    
    if (size.height <= 64) {
        
        return 210.0f;
    }
    
    return size.height + 156;
}

- (IBAction)onShowSolicitacoes:(id)sender {
    
    PedidoAjudaViewController *pedidoAjudaVC = (PedidoAjudaViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"PedidoAjuda"];
    
    
    [self.navigationController pushViewController:pedidoAjudaVC animated:YES];
    
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
    
    return [UIView new];
}
- (IBAction)onPedirAjuda:(id)sender {

    PedirAjudaViewController *pedirAjudaVC = (PedirAjudaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PedirAjuda"];

    pedirAjudaVC.categoriasList = self.categoriasList;
    
    [self.navigationController pushViewController:pedirAjudaVC animated:YES];
}

@end

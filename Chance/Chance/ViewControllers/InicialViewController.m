//
//  InicialViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/3/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "InicialViewController.h"

@interface InicialViewController ()

@end

@implementation InicialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([[[NSLocale autoupdatingCurrentLocale] objectForKey:NSLocaleLanguageCode] isEqualToString:@"en"]) {
        
        [self.uibLoginFacebook setImage:[UIImage imageNamed:@"btn-entrar-facebook-ingles"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibLoginFacebook setImage:[UIImage imageNamed:@"btn-entrar-facebook"] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Token"]) {
        
        TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:homeViewController animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Actions

-(IBAction)OnLoginFacebook:(id)sender {
    
    if ((FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) ||
        (FBSession.activeSession.state == FBSessionStateCreatedOpening) ||
        (FBSession.activeSession.state == FBSessionStateCreated)) {
        
        NSLog(@"Found a cached session");
        
        // If there's one, just open the session silently, without showing the user the login UI
        //[FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email", @"user_birthday"]
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email", @"user_birthday"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }
    else if ((FBSession.activeSession.state == FBSessionStateOpen) ||
             (FBSession.activeSession.state == FBSessionStateOpenTokenExtended)) {
        
        [self autenticarFacebook:FBSession.activeSession.accessTokenData.accessToken];
        return;
    }
    else if (FBSession.activeSession.state == FBSessionStateClosedLoginFailed) {
        
        [FBSession.activeSession closeAndClearTokenInformation];
        [[FBSessionTokenCachingStrategy defaultInstance] clearToken];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Erro ao logar no Facebook." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        return;
    }
    
}

#pragma mark Facebook Delegate

-(void) autenticarFacebook:(NSString *)accessTokenData {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:accessTokenData forKey:@"FacebookToken"];
    
    [NSRequestManager requestURL:@"Conta/Autenticar" andParameters:parameters andButton:self.uibLoginFacebook andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             Mensagem = "<null>";
             PerfilId = 26;
             Sucesso = 1;
             Token = fa4ba583b8354edfb64d84204867388d;
             */
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Token"] forKey:@"Token"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilId"];
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SelectedAccession"];
            
            TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
            [self.navigationController pushViewController:homeViewController animated:YES];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen) {
        
        NSLog(@"Session opened");
        
        [self autenticarFacebook:session.accessTokenData.accessToken];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed) {
       
        NSLog(@"Session closed");
    }
    
    if (error){
        
        NSLog(@"Error");
        
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES) {
            
            alertTitle = NSLocalizedString(@"Atencao", nil);
            alertText = [FBErrorUtility userMessageForError:error];
            //[self showMessage:alertText withTitle:alertTitle];
        }
        else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                
                alertTitle = NSLocalizedString(@"Atencao", nil);
                alertText = @"Your current session is no longer valid. Please log in again.";
                //[self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                //NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                if (!errorInformation) {
                    
                    alertTitle = NSLocalizedString(@"Atencao", nil);
                    alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                }
                else {
                    
                    alertTitle = NSLocalizedString(@"Atencao", nil);
                    alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                }
                
                // Show the user an error message
                alertTitle = NSLocalizedString(@"Atencao", nil);
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                //[self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        // Show the user the logged-out UI
        //[self userLoggedOut];
    }
    
    [FBSession.activeSession closeAndClearTokenInformation];
}

@end

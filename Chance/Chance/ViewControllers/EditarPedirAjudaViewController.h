//
//  EditarPedirAjudaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 4/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditarPedirAjudaViewController : BaseViewController<CLLocationManagerDelegate, UIAlertViewDelegate, UITextViewDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivCategoria;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UILabel *uilDados;
@property(nonatomic, strong) IBOutlet UIButton *uibAlterar;
@property(nonatomic, strong) IBOutlet UIButton *uibExcluir;
@property(nonatomic, strong) IBOutlet UITextView *uitxvDescricao;
@property(nonatomic, strong) IBOutlet UITextView *uitxvContato;

@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, strong) NSDictionary *itemAjuda;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *localizacaoAtual;
@property(nonatomic, strong) NSString *cidadeAtual;

@end

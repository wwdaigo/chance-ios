//
//  CriarContaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "CriarContaViewController.h"

@interface CriarContaViewController ()

@end

@implementation CriarContaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

-(IBAction)OnCadastrar:(id)sender {
    
    if([self.uitfNome.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaNome", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfSobrenome.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSobrenome", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfEmail.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaEmail", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if(![Utils IsValidEmail:self.uitfEmail.text]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"EmailNaoValido", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        [self.uitfEmail becomeFirstResponder];
        return;
    }
    
    if([self.uitfSenha.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSenha", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfConfirmarSenha.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaConfirmarSenha", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if(![self.uitfConfirmarSenha.text isEqualToString:self.uitfSenha.text]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoSenhaDiferenteSenhaConfirmarSenha", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    [self.view resignFirstResponder];
    [self.uitfNome resignFirstResponder];
    [self.uitfSobrenome resignFirstResponder];
    [self.uitfEmail resignFirstResponder];
    [self.uitfSenha resignFirstResponder];
    [self.uitfConfirmarSenha resignFirstResponder];
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:self.uitfNome.text forKey:@"Nome"];
    [parameters setObject:self.uitfSobrenome.text forKey:@"Sobrenome"];
    [parameters setObject:self.uitfEmail.text forKey:@"Email"];
    [parameters setObject:self.uitfSenha.text forKey:@"Senha"];
    //[parameters setObject:[self.imagemPerfil objectForKey:@"MidiaId"] forKey:@"MidiaId"];
    
    [NSRequestManager requestURL:@"Conta/Criar" andParameters:parameters andButton:sender andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             
             */
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Token"] forKey:@"Token"];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilId"];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"UsuarioCadastradoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            //[uialErro setTag:1];
            [uialErro show];
            
            /*
            [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"Dados"] objectForKey:@"Token"] forKey:@"Token"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Dados"] forKey:@"Client"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SelectedAccession"];
            */
            TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
            [self.navigationController pushViewController:homeViewController animated:YES];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnEsqueciSenha:(id)sender {
}

-(IBAction)OnUploadImagem:(id)sender {

    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO) &&
        ([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypePhotoLibrary] == NO) &&
        ([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)){
        
        return;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
    
    cameraUI.allowsEditing = YES;
    cameraUI.delegate = self;
    
    [self.navigationController presentViewController:cameraUI animated:YES completion:^{}];
}

-(IBAction)OnUsarDadosFacebook:(id)sender {
    
    FBLoginView *loginView = [[FBLoginView alloc] init];
    loginView.delegate = self;
}

#pragma mark Facebook Delegate

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        NSLog(@"Found a cached session");
        
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email", @"user_birthday"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }
    
    /*
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        
        // Show the user the logged-in UI
        //[self userLoggedIn];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
    */
}

/*
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
 
     // If the session was opened successfully
     if (!error && state == FBSessionStateOpen){
     NSLog(@"Session opened");
     // Show the user the logged-in UI
     [self userLoggedIn];
     return;
     }
     if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
     // If the session is closed
     NSLog(@"Session closed");
     // Show the user the logged-out UI
     [self userLoggedOut];
     }
     
     // Handle errors
     if (error){
     NSLog(@"Error");
     NSString *alertText;
     NSString *alertTitle;
     // If the error requires people using an app to make an action outside of the app in order to recover
     if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
     alertTitle = @"Something went wrong";
     alertText = [FBErrorUtility userMessageForError:error];
     [self showMessage:alertText withTitle:alertTitle];
     } else {
     
     // If the user cancelled login, do nothing
     if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
     NSLog(@"User cancelled login");
     
     // Handle session closures that happen outside of the app
     } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
     alertTitle = @"Session Error";
     alertText = @"Your current session is no longer valid. Please log in again.";
     [self showMessage:alertText withTitle:alertTitle];
     
     // For simplicity, here we just show a generic message for all other errors
     // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
     } else {
     //Get more error information from the error
     NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
     
     // Show the user an error message
     alertTitle = @"Something went wrong";
     alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
     [self showMessage:alertText withTitle:alertTitle];
     }
     }
     // Clear this token
     [FBSession.activeSession closeAndClearTokenInformation];
     // Show the user the logged-out UI
     [self userLoggedOut];
     }
}
*/

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    
    /*
    if (alertView.tag == 1) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    */
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    NSInteger tipoMedia = 0;
    NSString *filePath = @"";
    NSURL *dataURL;
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        tipoMedia = 1;
        UIImage *originalImage, *editedImage, *imageToSave;
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            
            imageToSave = editedImage;
        }
        else {
            
            imageToSave = originalImage;
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.png"];
        
        [UIImagePNGRepresentation(imageToSave) writeToFile:filePath atomically:YES];
        
        dataURL = [NSURL fileURLWithPath:filePath];
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    }
    
    if (dataURL) {
        
        ////[MRProgressOverlayView showOverlayAddedTo:picker.view.window animated:YES];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[NSNumber numberWithInteger:tipoMedia] forKey:@"Tipo"];
        //[parameters setObject:[self.solicitacao objectForKey:@"PostId"] forKey:@"PostId"];
        [NSRequestManager postDataURL:@"Midia/PostFile" andParameters:parameters andData:dataURL andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
            
            NSDictionary *result = (NSDictionary *)responseObject;
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                self.imagemPerfil = result;
                
                [picker dismissViewControllerAnimated:YES completion:nil];
                //self.midiaId = [result objectForKey:@"MidiaId"];
                //self.tipoMidia = [result objectForKey:@"Tipo"];
                //self.urlData = [result objectForKey:@"Url"];
                
                //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
                //[self.uitvFeed reloadData];
                
                //[picker dismissViewControllerAnimated:YES completion:nil];
            }
            
        } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];
    }
}

#pragma mark - TextField Delegate

-(void) hideKeyboard {
    
    [self.uitfEmail resignFirstResponder];
    [self.uitfNome resignFirstResponder];
    [self.uitfSenha resignFirstResponder];
    [self.uitfConfirmarSenha resignFirstResponder];
    [self.uitfSobrenome resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        
        return YES;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	if (heightFraction < 0.0) {
        
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
        
		heightFraction = 1.0;
	}
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
		self.animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	}
	else {
        
		self.animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    /*
     if (textField == self.uitfLogin) {
     
     [self.uitfPassword becomeFirstResponder];
     
     return;
     }
     */
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self hideKeyboard];
    
    return YES;
}

@end

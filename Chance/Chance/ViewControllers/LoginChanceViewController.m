//
//  LoginChanceViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "LoginChanceViewController.h"

@interface LoginChanceViewController ()

@end

@implementation LoginChanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    //[self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

-(IBAction)OnEsqueciSenha:(id)sender {
    
    UIAlertView *uialEsqueciSenha = [[UIAlertView alloc] initWithTitle:@"Recuperar Senha" message:@"Informe abaixo o e-mail usado para fazer o login e enviaremos um e-mail com os dados para a recuperação de senha" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Enviar", nil];
    [uialEsqueciSenha setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [uialEsqueciSenha setTag:1];
    
    UITextField *uitfEmail = [uialEsqueciSenha textFieldAtIndex:0];
    [uitfEmail setPlaceholder:@"E-mail"];
    [uitfEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    [uialEsqueciSenha show];
    
    return;
}

-(IBAction)OnLogin:(id)sender {
    
    if([self.uitfEmail.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Preencha o campo e-mail." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfPassword.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Preencha o campo senha." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    [self.view resignFirstResponder];
    [self.uitfEmail resignFirstResponder];
    [self.uitfPassword resignFirstResponder];
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:self.uitfEmail.text forKey:@"Email"];
    [parameters setObject:self.uitfPassword.text forKey:@"Senha"];
    
    [NSRequestManager requestURL:@"Conta/Autenticar" andParameters:parameters andButton:sender andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             Mensagem = "<null>";
             PerfilId = 26;
             Sucesso = 1;
             Token = fa4ba583b8354edfb64d84204867388d;
             */
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Token"] forKey:@"Token"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilId"];
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SelectedAccession"];
            
            TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
            [self.navigationController pushViewController:homeViewController animated:YES];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if (buttonIndex == 1) {
            
            //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            UITextField *uitfEmail = [alertView textFieldAtIndex:0];
            
            [parameters setObject:uitfEmail.text forKey:@"Email"];
            
            [NSRequestManager requestURL:@"Conta/RecuperarSenha" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    /*
                     Mensagem = "<null>";
                     PerfilId = 26;
                     Sucesso = 1;
                     Token = fa4ba583b8354edfb64d84204867388d;
                     */
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"DadosRecuperadosSenhaSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                     
                     [uialErro show];
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
        }
    }
}

#pragma mark - TextField Delegate

-(void) hideKeyboard {
    
    [self.uitfEmail resignFirstResponder];
    [self.uitfPassword resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        
        return YES;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	if (heightFraction < 0.0) {
        
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
        
		heightFraction = 1.0;
	}
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
		self.animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	}
	else {
        
		self.animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    /*
     if (textField == self.uitfLogin) {
     
     [self.uitfPassword becomeFirstResponder];
     
     return;
     }
     */
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self hideKeyboard];
    
    return YES;
}

@end

//
//  CriarContaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TabBarViewController.h"

@interface CriarContaViewController : BaseViewController<FBLoginViewDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    
}

@property(nonatomic, strong) IBOutlet UITextField *uitfNome;
@property(nonatomic, strong) IBOutlet UITextField *uitfSobrenome;
@property(nonatomic, strong) IBOutlet UITextField *uitfEmail;
@property(nonatomic, strong) IBOutlet UITextField *uitfSenha;
@property(nonatomic, strong) IBOutlet UITextField *uitfConfirmarSenha;

@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, strong) NSDictionary *imagemPerfil;

@end

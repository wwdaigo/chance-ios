//
//  PostDetalheViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MidiasPostDetalheCell.h"
#import "TextoPostDetalheCell.h"

@interface PostDetalheViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UIButton *uibComentar;
@property(nonatomic, strong) IBOutlet UIButton *uibContagiar;
@property(nonatomic, strong) IBOutlet UIButton *uibDenunciar;
@property(nonatomic, strong) IBOutlet UIButton *uibLerMais;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas1;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas2;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas3;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas4;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilComentarios;
@property(nonatomic, strong) IBOutlet UILabel *uilContagios;
@property(nonatomic, strong) IBOutlet UILabel *uilDataCriacao;
@property(nonatomic, strong) IBOutlet UILabel *uilLerMais;
@property(nonatomic, strong) IBOutlet UILabel *uilLocal;
@property(nonatomic, strong) IBOutlet UILabel *uilTexto;
@property(nonatomic, strong) IBOutlet UITextView *uitxvTexto;
@property(nonatomic, strong) IBOutlet UIView *uivCabecalho;
@property(nonatomic, strong) IBOutlet UIView *uivTexto;
@property(nonatomic, strong) IBOutlet UIView *uivMidias;
@property(nonatomic, strong) IBOutlet UIView *uivRodape;

@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) NSDictionary *post;

@property (strong) ReloadBlock reloadBlock;

-(void) reloadDataBlock:(ReloadBlock) reloadBlock;

-(void) reloadCell:(NSDictionary *) aPost;
-(void) reloadData:(NSDictionary *) aPost;

@end

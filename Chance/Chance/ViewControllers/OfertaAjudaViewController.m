//
//  OfertaAjudaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "OfertaAjudaViewController.h"

@interface OfertaAjudaViewController ()

@end

@implementation OfertaAjudaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([self tipo]) {
        
        [self.navigationItem setTitle:@"Pedir Ajuda"];
    }
    else {
        
        [self.navigationItem setTitle:@"Ofertar Ajuda"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.oferta objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    
    [parameters setObject:[NSNumber numberWithBool:self.tipo] forKey:@"TipoAjuda"];
    
    [NSRequestManager requestURL:@"Ajuda/Ver" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            [self.tableView reloadData];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}
//OfertaPedidoAjudaCell

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *texto = [self.oferta objectForKey:@"Texto"];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]};
    
    CGRect titleRect = [texto boundingRectWithSize:(CGSize){(tableView.frame.size.width - 20), 2000.0f} options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    
    CGSize size = titleRect.size;
    
    if (size.height <= 64) {
        
        return 200.0f;
    }
    
    return size.height + 156;
}
*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OfertaPedidoAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfertaAjudaCell"];
    
    [cell reloadCell:self.oferta];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  InicialViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/3/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"

#import "TabBarViewController.h"

@interface InicialViewController : BaseViewController {
    
}

@property(nonatomic, strong) IBOutlet UIButton *uibLoginFacebook;

@end

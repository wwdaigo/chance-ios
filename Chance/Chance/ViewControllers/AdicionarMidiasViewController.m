//
//  AdicionarMidiasViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "AdicionarMidiasViewController.h"

@interface AdicionarMidiasViewController ()

@end

@implementation AdicionarMidiasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

-(IBAction)OnAdicionarMidias:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
    }];
}

-(IBAction)OnFechar:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
    }];
}

#pragma mark - Internal Methods

-(void) reloadImagens {
    
    [self.uicvMidias reloadData];
    
    if ([self.midiasList count] > 1) {
        
        [self.uicvMidias scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self.midiasList count]-2 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    }
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
        
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.formSheetController presentViewController:cameraUI animated:YES completion:^{
                
                [self.uicvMidias reloadData];
            }];
        }
        else if (buttonIndex == 2) {
            
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.formSheetController presentViewController:cameraUI animated:YES completion:^{
                
            }];
        }
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.midiasList count] + 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self.midiasList count]) {
        
        if ([self.midiasList count] == 0) {
            
            return CGSizeMake(320,190);
        }
        return CGSizeMake(55,190);
    }
    return CGSizeMake(280, 190);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self.midiasList count]) {
        
        static NSString *cellIdentifier = @"AdicionarMidiaViewCell";
        
        AdicionarMidiaViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if ([self.midiasList count] == 0) {
            
            [cell.uiivAdicionarMidia setFrame:CGRectMake(132, 68, 55, 55)];
        }
        else {
            
            [cell.uiivAdicionarMidia setFrame:CGRectMake(0, 68, 55, 55)];
        }
        //[cell.uilAdicionarMidia setText:@"Clique para aqui adicionar uma nova mídia."];
        
        return cell;
    }
    else {
        
        static NSString *cellIdentifier = @"MidiaViewCell";
        
        MidiaViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        [cell setPermiteFullscreen:NO];
        
        if (![indexPath row]) {
            
            [cell carregandoMidia:self.midiasList indexPath:[NSIndexPath indexPathForRow:0 inSection:0] parent:self.parent collection:collectionView];
        }
        else {
            
            [cell carregandoMidia:self.midiasList indexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] parent:self.parent collection:collectionView];
        }
        
        return cell;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self.midiasList count]) {
        
        if (([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeCamera] == NO) &&
            ([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypePhotoLibrary] == NO) &&
            ([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)){
            
            return;
        }
        
        UIAlertView *uialFoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Selecione", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Camera", nil), NSLocalizedString(@"Album", nil), nil];
        [uialFoto setTag:1];
        [uialFoto show];
        
        //[self.parent.navigationController presentViewController:cameraUI animated:YES completion:^{}];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    NSInteger tipoMedia = 0;
    NSString *filePath = @"";
    NSString *fileName = @"";
    NSURL *dataURL;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        tipoMedia = 1;
        UIImage *originalImage, *editedImage, *imageToSave;
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            
            imageToSave = editedImage;
        }
        else {
            
            imageToSave = originalImage;
        }
        
        fileName = @"Image.png";
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        
        [UIImagePNGRepresentation(imageToSave) writeToFile:filePath atomically:YES];
        
        dataURL = [NSURL fileURLWithPath:filePath];
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    }
    else if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0)
             == kCFCompareEqualTo) {
        
        tipoMedia = 2;
        dataURL = [info valueForKey:UIImagePickerControllerMediaURL];
        fileName = @"Video.mov";
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    }
    
    if (dataURL) {
        
        NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:dataURL.path error:nil];
        NSNumber * size = [properties objectForKey: NSFileSize];
        
        ////[MRProgressOverlayView showOverlayAddedTo:picker.view.window animated:YES];
        MRProgressOverlayView *progress = [MRProgressOverlayView showOverlayAddedTo:picker.view animated:YES];
        [progress setMode:MRProgressOverlayViewModeDeterminateHorizontalBar];
        [progress setTitleLabelText:NSLocalizedString(@"UploadMidia", nil)];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[NSNumber numberWithInteger:tipoMedia] forKey:@"Tipo"];
        
        //
        
        [NSRequestManager uploadTaskURL:@"Midia/PostFile" andParameters:parameters andData:dataURL andFile:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath] name:@"file" fileName:fileName mimeType:mediaType error:nil];
            
        } andProgess:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
            
            //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
            //int aPercentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
            
            //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) size.floatValue) * 100.0);
            
            [self performBlock:^{
                
                float percentDownloaded = ((float) totalBytesSent / (float) size.floatValue);
                NSLog(@"Upload %.2f", percentDownloaded);
                
                if(percentDownloaded < 1)
                    [progress setProgress:percentDownloaded];
                
            } afterDelay:0.3];
            
            /*
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
            });
            */
            
        } andSuccess:^(NSURLResponse *response, id responseObject, NSError *error) {
            NSDictionary *result = (NSDictionary *)responseObject;
            
            [progress dismiss:YES];
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                [self.midiasList addObject:result];
                
                [picker dismissViewControllerAnimated:YES completion:^{
                    
                    [self reloadImagens];
                }];
            }
        } andFailure:^(NSError *error) {
            
            [progress dismiss:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];
        
        /*
        [NSRequestManager postDataURL:@"Midia/PostFile" andParameters:parameters andData:dataURL andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
            
            NSDictionary *result = (NSDictionary *)responseObject;
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                [self.midiasList addObject:result];
                
                [picker dismissViewControllerAnimated:YES completion:nil];
            }
            
        } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];
        */
    }
}

@end

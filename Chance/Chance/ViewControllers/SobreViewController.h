//
//  SobreViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"

@interface SobreViewController : BaseViewController<UIWebViewDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UIWebView *uiwbSobre;

@end

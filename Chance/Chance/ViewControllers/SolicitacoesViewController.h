//
//  SolicitacoesViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 1/30/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SolicitacaoCell.h"
#import "VerPerfilViewController.h"

@interface SolicitacoesViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) NSMutableArray *solicitacoesList;

@end

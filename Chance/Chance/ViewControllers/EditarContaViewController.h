//
//  EditarContaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 19/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AlterarSenhaViewController.h"

@interface EditarContaViewController : BaseViewController<UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UIButton *uibFoto;
@property(nonatomic, strong) IBOutlet UIButton *uibSalvarAlteracoes;
@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UIDatePicker *uidpDataNascimento;
@property(nonatomic, strong) IBOutlet UITextField *uitfNome;
@property(nonatomic, strong) IBOutlet UITextField *uitfSobrenome;
@property(nonatomic, strong) IBOutlet UITextField *uitfDataNascimento;
@property(nonatomic, strong) IBOutlet UITextField *uitfNomeEmpresa;
@property(nonatomic, strong) IBOutlet UITextField *uitfEmail;
@property(nonatomic, strong) IBOutlet UISegmentedControl *uisgcSexo;
@property(nonatomic, strong) IBOutlet UISwitch *uiswEmpresa;
@property(nonatomic, strong) IBOutlet UIView *uivData;
//@property(nonatomic, strong) IBOutlet UITableView *uitvEditarDados;

//@property(nonatomic, strong) RETableViewManager *uiretvEditarDados;
@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, strong) NSDictionary *imagemPerfilData;
@property(nonatomic, strong) NSMutableDictionary *imagemPerfilParameters;
@property(nonatomic, strong) NSDate *dataNascimento;

@end

//
//  LoginChanceViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/01/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TabBarViewController.h"

@interface LoginChanceViewController : BaseViewController {
    
}

@property(nonatomic, strong) IBOutlet UITextField *uitfEmail;
@property(nonatomic, strong) IBOutlet UITextField *uitfPassword;

@property(nonatomic, assign) CGFloat animatedDistance;

@end

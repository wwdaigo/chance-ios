//
//  BaseTableViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 22/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NSNotificationAlertView.h"
#import "UIViewController+BaseController.h"

@interface BaseTableViewController : BaseViewController {
    
}
@property(nonatomic, strong) IBOutlet UITableView *tableView;
@property(nonatomic, strong) UIRefreshControl *refreshControl;
//@property(nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@property(nonatomic) NSMutableArray *visibleCells;

@property(nonatomic) NSInteger currentPage;
@property(nonatomic) BOOL pageLoad;
@property(nonatomic) NSInteger offset;


-(BOOL) isPaging:(NSInteger)items;
-(void) loadTableView;
-(UITableViewCell *)loadingCell;
-(void) refreshData:(NSInteger) sender;
-(void) feedOlgPages;

@end

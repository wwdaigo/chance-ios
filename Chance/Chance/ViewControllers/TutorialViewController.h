//
//  TutorialViewController.h
//  TicketUP
//
//  Created by Luiz Carlos Sant'Ana Junior on 04/09/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : BaseViewController {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivTutorial;
@property(nonatomic, strong) IBOutlet UIButton *uibTutorial;
@property(nonatomic, strong) IBOutlet UIScrollView *uiscTutorial;
@property(nonatomic, strong) IBOutlet UIPageControl *uipcTutorial;

@property(nonatomic, assign) NSInteger paginaTutorial;
@property(nonatomic, strong) NSMutableArray *listaTutorial;

-(IBAction)OnProsseguir:(id)sender;

@end

//
//  BaseTableViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 22/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];

    [self.refreshControl addTarget:self action:@selector(OnRefreshData:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    //self.tableView.refreshControl = refreshControl;
    
    /*
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:self.activityIndicatorView];
    self.activityIndicatorView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.activityIndicatorView startAnimating];
    */
    self.currentPage = 0;
    self.pageLoad = YES;
    
    [self.tableView setBackgroundColor:[UIColor colorWithRed:188.0f/255.0f green:194.0f/255.0f blue:175.0f/255.0f alpha:1.0f]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    //[self.navigationItem setTitle:<#(NSString *)#>]
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(void) OnRefreshData:(id) page {
    
    self.currentPage = 0;
    [self refreshData:0];
}

#pragma mark - Internal actions

-(BOOL) isPaging:(NSInteger)items {
    
    if ((items > 0) &&
        (self.pageLoad) &&
        ((items % NUMERO_ITENS_POR_PAGINA) == 0)) {
        
        return YES;
    }
    
    return NO;
}

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator setTintColor:[UIColor colorWithRed:188.0f/255.0f green:194.0f/255.0f blue:175.0f/255.0f alpha:1.0f]];
    [activityIndicator startAnimating];
    
    cell.tag = TAG_LOADING_CELL;
    
    return cell;
}

-(void) refreshData:(NSInteger) page {
    
    [self.refreshControl endRefreshing];
    
    self.offset = page;
}

-(void) feedOlgPages {

    NSInteger offset = 0;
    
    if (self.currentPage == 0) {
        
        offset = 1;
    }
    else {
        
        offset = (self.currentPage + 1);
    }
    if ((offset * NUMERO_ITENS_POR_PAGINA) > (self.offset)) {
        
        [self refreshData:(offset * NUMERO_ITENS_POR_PAGINA)];
    }
    
    //self.currentPage++;
}

#pragma mark - Table view data source

-(void) loadTableView {
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (cell.tag == TAG_LOADING_CELL) {
        
        [self refreshData:(self.currentPage * NUMERO_ITENS_POR_PAGINA)];
        self.currentPage++;
    }
}
*/
/*
-(CGFloat)tableView:(UITableView *)aTableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [aTableView tableView:aTableView heightForRowAtIndexPath:indexPath];
}
*/
/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
        if (indexPath.row==0){//if it is the first row
            self.loadmoreTableViewCell=(MyCustomTableViewCell *) cell; // get the reference to the tableview cell
            [self.loadmoreTableViewCell.loadingActivityIndicator startAnimating];//start animating the activity indicator view
            [self loadMoreStuffFromInternet]; //Load More Stuff from Internet
            return;
        }
    }
*/
/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((cell.tag == TAG_LOADING_CELL) &&
        (indexPath.row > (NUMERO_ITENS_POR_PAGINA - 1))) {
    
        NSLog(@"willDisplayCell Row: %i", indexPath.row);
        [self refreshData:(self.currentPage * NUMERO_ITENS_POR_PAGINA)];
        
        self.currentPage++;
    }
}
*/
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

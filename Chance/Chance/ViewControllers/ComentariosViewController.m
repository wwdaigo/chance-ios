//
//  ComentariosViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 16/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ComentariosViewController.h"

@interface ComentariosViewController ()

@end

@implementation ComentariosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [self startCommenting];
    [self disableCommenting];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self loadComentarios];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    if (self.reloadBlock) {
        
        self.reloadBlock();
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)OnPostar:(id)sender {
    
    if (([self.uitxvPost.text isEqualToString:NSLocalizedString(@"EscrevaDesejaComentar", nil)]) ||
        ([self.uitxvPost.text isEqualToString:@""])) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"EscrevaComentarioPostar", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.post objectForKey:@"PostId"] forKey:@"PostId"];
    [parameters setObject:self.uitxvPost.text forKey:@"Comentario"];
    
    
    if(self.uitxvPost.isFirstResponder){
        
        //[self.uitxvPost resignFirstResponder];
        [self.uitxvPost setText:@""];
        [self disableCommenting];
    }
    
    [NSRequestManager requestURL:@"Post/Comentar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Comentário enviado com sucesso." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            */
            //[self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
            //}];
            
            [self loadComentarios];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(void) loadComentarios {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.post objectForKey:@"PostId"] forKey:@"PostId"];
    
    [NSRequestManager requestURL:@"Post/Comentarios" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            self.comentariosList = [NSMutableArray arrayWithArray:[result objectForKey:@"Comentarios"]];
            if ([[result objectForKey:@"Comentarios"] count] > 0) {
                
                [self reloadComments];
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - Internal Actions

- (void)enableCommenting {
    
    //[self.uitxvPost setHidden:YES];
    [self setPostButtonHidden:NO];
    [self.uilCommentTextViewPlaceholder setText:@""];
}

- (void)disableCommenting {
    
    //[self.uitxvPost setHidden:NO];
    [self setPostButtonHidden:YES];
    [self.uilCommentTextViewPlaceholder setFrame:self.uitxvPost.frame];
    [self.uilCommentTextViewPlaceholder setFrame:CGRectOffset(self.uilCommentTextViewPlaceholder.frame, 5, -3)];
    [self.uilCommentTextViewPlaceholder setText:NSLocalizedString(@"EscrevaDesejaComentar", nil)];
}

- (void)startCommenting {
    
    [self.uitxvPost becomeFirstResponder];
}

- (void)cancelCommenting {
    
    [self.uitxvPost resignFirstResponder];
}

- (void)reloadComments {
    
    [self.uitvComentarios reloadData];
    
    [self recalculateContentInset];
    [self recalculateScrollIndicator];
}

- (void)recalculateContentInset {
    
    CGFloat contentInsetHeight = MAX(self.uitvComentarios.frame.size.height - self.uitvComentarios.contentSize.height, 0);
    CGFloat duration = 0.0;
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [[self uitvComentarios] setContentInset:UIEdgeInsetsMake(contentInsetHeight, 0, 0, 0)];
                     }completion:nil];
}

- (void)recalculateScrollIndicator {
    
    if(self.uitvComentarios.contentSize.height >= self.uitvComentarios.frame.size.height){
        
        [[self uitvComentarios] setShowsVerticalScrollIndicator:YES];
    }
    else {
        
        [[self uitvComentarios] setShowsVerticalScrollIndicator:NO];
    }
}

- (void)setPostButtonHidden:(BOOL)hidden {
    
    const CGFloat duration = 0.20;
    CGFloat alpha = hidden ? 0 : 1.0;
    CGFloat delay = hidden ? 0 : 0.05;
    
    [UIView animateWithDuration:duration
                          delay:delay
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.uibPost setAlpha:alpha];
                     }
                     completion:nil];
}

-(void) reloadDataBlock:(ReloadBlock) reloadBlock {
    
    self.reloadBlock = reloadBlock;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.comentariosList count];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *text = [[self.comentariosList objectAtIndex:indexPath.row] objectForKey:@"Texto"];
    
    if ([text isKindOfClass:[NSNull class]]) {
        
        text = @"";
    }
    //CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    CGRect rect = CGRectZero;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:14]};
    
    if ((![self.comentariosList objectAtIndex:indexPath.row]) ||
        ([[[[self.comentariosList objectAtIndex:indexPath.row] objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        rect = [text boundingRectWithSize:CGSizeMake(170, MAXFLOAT) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    }
    else {
     
        rect = [text boundingRectWithSize:CGSizeMake(210, MAXFLOAT) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    }
    
    //CGFloat heightLabel = MIN(rect.size.height + (10.0f * 2) + 40.0f, 41.0f);
    CGFloat heightLabel = rect.size.height + (10.0f * 2) + 40.0f;
    //CGFloat heightLabel = rect.size.height;
    CGFloat heightCell = 100;
    if (heightLabel > 100.0f) {
        
        heightCell = (heightLabel) + 10;
    }
    
    return heightCell;
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
     if ((![self.comentariosList objectAtIndex:indexPath.row]) ||
         ([[[[self.comentariosList objectAtIndex:indexPath.row] objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
         
         ComentarioCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ComentarioDeleteCell" forIndexPath:indexPath];
         
         [cell setComentariosList:self.comentariosList];
         [cell setIndexPath:indexPath];
         [cell setTableView:tableView];
         [cell reloadCell:[self.comentariosList objectAtIndex:indexPath.row]];
         
         return cell;
     }
     else {
         
         ComentarioCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ComentarioCell" forIndexPath:indexPath];
         
         [cell reloadCell:[self.comentariosList objectAtIndex:indexPath.row]];
         
         return cell;
     }
     
     return nil;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - TextView Delegate

-(void) hideKeyboard {
    
    [self.uitxvPost resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}
/*
-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:NSLocalizedString(@"EscrevaDesejaComentar", nil)]) {
        
        [textView setText:@""];
    }
}
*/
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if (([[textView text] length] > 1) ||
        (![text isEqualToString:@""])) {
        
        [self enableCommenting];
    }
    else {
        
        [self disableCommenting];
    }
    
    if ([text isEqualToString:@""]) {
        
        return YES;
    }
    
    if ([textView.text length] >= TAMANHO_MAXIMO_COMENTARIO) {
        
        return NO;
    }
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
 
    [self setPostButtonHidden:YES];
    
    /*
    if ([textView.text isEqualToString:@""]) {
        
        [textView setText:NSLocalizedString(@"EscrevaDesejaComentar", nil)];
    }
    */
}

- (void)keyboardDidChangeFrame:(NSNotification *)notification {
    
    /*
    NSDictionary *userInfo = notification.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect beginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    */
    
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    NSValue* value = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration;
    [value getValue:&duration];
    
    NSLog(@"Keyboard frame with conversion is %f,%f,%f,%f", keyboardFrame.origin.x, keyboardFrame.origin.y, keyboardFrame.size.width, keyboardFrame.size.height);
    
    NSLog(@"uivPost frame with conversion is %f,%f,%f,%f", self.uivPost.frame.origin.x, self.uivPost.frame.origin.y, self.uivPost.frame.size.width, self.uivPost.frame.size.height);
    
    NSLog(@"UIView frame with conversion is %f,%f,%f,%f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    
    //3.5
    //Keyboard frame with conversion is 0.000000,200.000000,320.000000,216.000000
    //uivPost frame with conversion is 0.000000,381.000000,320.000000,35.000000
    //UIView frame with conversion is 0.000000,64.000000,320.000000,416.000000
    //uitvComentarios frame with conversion is 0.000000,0.000000,320.000000,251.000000
    
    //4.0
    //Keyboard frame with conversion is 0.000000,288.000000,320.000000,216.000000
    //uivPost frame with conversion is 0.000000,381.000000,320.000000,35.000000
    //UIView frame with conversion is 0.000000,64.000000,320.000000,504.000000
    //uitvComentarios frame with conversion is 0.000000,0.000000,320.000000,251.000000
    
    CGPoint newCenter = CGPointMake(self.uivPost.frame.size.width*0.5,
                                    keyboardFrame.origin.y - (self.uivPost.frame.size.height*0.5));
    
    [UIView beginAnimations:@"" context:nil];
    
    [UIView setAnimationDuration:duration];
    [self.uivPost setCenter:newCenter];
    
    //[self.uitvComentarios setFrame:CGRectMake(0, 0, 320, (self.view.frame.size.height - self.uivPost.frame.origin.y))];
    [self.uitvComentarios setFrame:CGRectMake(0, 0, 320, (self.view.frame.size.height - self.uivPost.frame.size.height - keyboardFrame.size.height))];

    [UIView commitAnimations];
    
    NSLog(@"uitvComentarios frame with conversion is %f,%f,%f,%f", self.uitvComentarios.frame.origin.x, self.uitvComentarios.frame.origin.y, self.uitvComentarios.frame.size.width, self.uitvComentarios.frame.size.height);
}

@end

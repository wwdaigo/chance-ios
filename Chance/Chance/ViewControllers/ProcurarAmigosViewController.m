//
//  ProcurarAmigosViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 11/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ProcurarAmigosViewController.h"

@interface ProcurarAmigosViewController ()

@end

@implementation ProcurarAmigosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //[super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.amigosList = [NSMutableArray arrayWithCapacity:0];
    self.procurarAmigosList = [NSMutableArray arrayWithCapacity:0];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([self.amigosList count] == 0) {
        
        [self refreshData:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions
/*
-(IBAction)OnConvidarAmigosFacebook:(id)sender {
    
    if (!FBSession.activeSession) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"É necessário logar com o Fecebook." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }

    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:FBSession.activeSession.accessTokenData.accessToken forKey:@"FacebookToken"];
    
    //Listar - > Listar amigos
    [NSRequestManager requestURL:@"Conta/ImportarAmigos" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            ConvidarAmigosFacebookViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ConvidarAmigosFacebook"];
            
            vc.amigosFacebookList = [NSMutableArray arrayWithArray:[result objectForKey:@"ListaAmigos"]];
            MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
            
            formSheet.presentedFormSheetSize = CGSizeMake(300, 300);
            formSheet.shouldDismissOnBackgroundViewTap = NO;
            formSheet.shouldCenterVertically = YES;
            formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
            
            //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
            
            formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {

            };
            
            formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
            
            [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
                
            }];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}
*/
#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    [self refreshData:page searchBar:self.uisbProcurar];
}

-(void) refreshData:(NSInteger) page searchBar:(UISearchBar *)searchBar {
    
    [NSRequestManager cancelAllOperations];
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //Listar - > Listar amigos
    
    if (![searchBar.text isEqualToString:@""]) {
        
        [parameters setObject:searchBar.text forKey:@"Q"];
        [NSRequestManager requestURL:@"Relacionamento/Procurar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            
            [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
            
            NSDictionary *result = (NSDictionary *)responseObject;
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                self.procurarAmigosList = [NSMutableArray arrayWithArray:[result objectForKey:@"Pessoas"]];
                [self.tableView reloadData];
                //self.solicitacoesList
                /*
                 Dados =     {
                 Token = c14b62f19fba46399dbd0675d079bc2c;
                 };
                 Mensagem = "<null>";
                 Sucesso = 1;
                 */
            }
            
        } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
            
            [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];

    }
    
    [parameters removeAllObjects];
    [NSRequestManager requestURL:@"Relacionamento/Listar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            self.amigosList = [NSMutableArray arrayWithArray:[result objectForKey:@"Pessoas"]];
            [self.tableView reloadData];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UISearchBar Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    [self refreshData:1 searchBar:searchBar];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    searchBar.text = @"";
    
    [self.procurarAmigosList removeAllObjects];
    [self.tableView reloadData];
    
    [searchBar resignFirstResponder];
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([self.uisbProcurar.text isEqualToString:@""]) {
        
        return 2;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return [self.procurarAmigosList count];
    }
    else  if (section == 1) {
        
        return [self.amigosList count];
    }
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProcurarAmigoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProcurarAmigoCell"];
    cell.parent = self;
    
    if (indexPath.section == 0) {
        
        [cell reloadCell:[self.procurarAmigosList objectAtIndex:indexPath.row] andTableView:tableView andIndexPath:indexPath];
    }
    else if (indexPath.section == 1) {
        
        [cell reloadCell:[self.amigosList objectAtIndex:indexPath.row] andTableView:tableView andIndexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VerPerfilViewController *verPerfilViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    
    if (indexPath.section == 0) {
    
        verPerfilViewController.perfil = [self.procurarAmigosList objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1) {
        
        verPerfilViewController.perfil = [self.amigosList objectAtIndex:indexPath.row];
    }
    
    [self.navigationController pushViewController:verPerfilViewController animated:YES];
}

@end

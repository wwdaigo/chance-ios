//
//  FeedGlobalViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 10/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"

@interface FeedGlobalViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *uiaiLoading;

@property(nonatomic, strong) NSMutableArray *feedList;

@end
//
//  VerPerfilViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 1/30/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "VerPerfilViewController.h"

@interface VerPerfilViewController ()

@end

@implementation VerPerfilViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.uiaiLoading.alpha = 0;
   

    self.postList = [NSMutableArray arrayWithCapacity:0];
    self.pedidosAjudaList = [NSMutableArray arrayWithCapacity:0];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self startActivityIndicator];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self carregarPerfil];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Actions

-(IBAction)OnAdicionar:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([self.perfil objectForKey:@"PerfilId"]) {
        
        [parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    }
    else {
        
        [parameters setObject:[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    }
    [NSRequestManager requestURL:@"Relacionamento/SolicitarAmizade" andParameters:parameters  andButton:sender andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            //[(BaseTableViewController *)self.parent refreshData:1];
            //[self.uibAdicionar setHidden:YES];
            [self visibleAdicionar:YES andSeguir:YES];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnEditarDados:(id)sender {
    
    EditarContaViewController *editarContaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditarDados"];
    [self.navigationController pushViewController:editarContaViewController animated:YES];
}

-(IBAction)OnSeguir:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //[parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    if ([self.perfil objectForKey:@"PerfilId"]) {
        
        [parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    }
    else {
        
        [parameters setObject:[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    }
    [NSRequestManager requestURL:@"Relacionamento/SeguirPerfil" andParameters:parameters andButton:sender andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self.uibSeguir setHidden:YES];
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - Internal Actions

-(void)carregarPerfil {
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [self.uibAdicionar setHidden:YES];
    [self.uibEditarDados setHidden:YES];
    [self.uibSeguir setHidden:YES];
    
    if ((!self.perfil) ||
        ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        //[self.uibAdicionar setHidden:YES];
        //[self.uibEditarDados setHidden:YES];
        //[self.uibSeguir setHidden:YES];
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"] forKey:@"Id"];
    }
    else {
        
        //[self.uibAdicionar setHidden:NO];
        //[self.uibEditarDados setHidden:YES];
        //[self.uibSeguir setHidden:NO];
        
        if ([self.perfil objectForKey:@"PerfilId"]) {
            
            [parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"Id"];
        }
        else {
            
            [parameters setObject:[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] forKey:@"Id"];
        }
    }
    
    [NSRequestManager requestURL:@"Conta/VerPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            
            [self stopActivityIndicator];
        }
        else {
            
            [self.uiivNivel setImage:[UIImage imageNamed:[NSString stringWithFormat:@"img-nivel-0%i", [[result objectForKey:@"Nivel"] intValue]]]];
            
            if ((!self.perfil) ||
                ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
                
                self.pedidosAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"ListaSolicitacoes"]];
            }
            else {
                
                self.pedidosAjudaList = [NSMutableArray arrayWithCapacity:1];
            }
            
            if ([[result objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                
                [self.uiivFoto setImageWithURLString:[[result objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
                
                /*
                 //[MRProgressOverlayView showOverlayAddedTo:self.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
                 
                 NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[result objectForKey:@"Foto"] objectForKey:@"UrlThumb"]]];
                 
                 AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
                 operation.responseSerializer = [AFImageResponseSerializer serializer];
                 
                 [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 [self.uiivFoto setImage:(UIImage *)responseObject];
                 [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 NSLog(@"Error: %@", error);
                 [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
                 [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
                 }];
                 
                 [operation start];
                 */
                //[self.uiivFoto setImageWithURL:[NSURL URLWithString:[[[self.post objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
            else {
                
                [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
            
            [self.uilApelido setText:[result objectForKey:@"Apelido"]];
            
            if ((![[[result objectForKey:@"Idade"] stringValue] isEqualToString:@""]) &&
                (![[result objectForKey:@"Idade"] isKindOfClass:[NSNull class]])) {
                
                if (([result objectForKey:@"Sexo"] == nil) ||
                    ([[result objectForKey:@"Sexo"] isKindOfClass:[NSNull class]])) {
                    
                    if ([[result objectForKey:@"Idade"] intValue] == 0) {
                        
                        [self.uilIdade setText:@""];
                    }
                    else {
                        
                        [self.uilIdade setText:[NSString stringWithFormat:@"%@ %@", [[result objectForKey:@"Idade"] stringValue], NSLocalizedString(@"Anos", nil)]];
                    }
                }
                else {
                    
                    NSString *sexo;
                    
                    if ([[result objectForKey:@"Sexo"] isEqualToString:@"M"]) {
                        
                        sexo = NSLocalizedString(@"Masculino", nil);
                    }
                    else {
                        
                        sexo = NSLocalizedString(@"Feminino", nil);
                    }
                    
                    [self.uilIdade setText:[NSString stringWithFormat:@"%@ %@, %@", [[result objectForKey:@"Idade"] stringValue], NSLocalizedString(@"Anos", nil), sexo]];
                }
            }
            else {
                
                [self.uilIdade setText:@""];
            }
            
            [self.uilNivel setText:[result objectForKey:@"Nivel"]];
            [self.uilPontuacao setText:[[result objectForKey:@"Pontuacao"] stringValue]];
            
            [self visibleAdicionar:[[result objectForKey:@"Amizade"] boolValue] andSeguir:[[result objectForKey:@"Seguindo"] boolValue]];
            
            /*
            if ([[result objectForKey:@"Seguindo"] boolValue]) {
                
                [self.uibSeguir setHidden:YES];
            }
            else {
                
                [self.uibSeguir setHidden:NO];
            }
            
            if ([[result objectForKey:@"Amizade"] boolValue]) {
                
                [self.uibAdicionar setHidden:YES];
                [self visibleAdicionar:YES andSeguir:YES];
            }
            else {
                
                [self.uibAdicionar setHidden:NO];
                [self visibleAdicionar:YES andSeguir:YES];
            }
            */
            if ((!self.perfil) ||
                ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
                
                ///[self.uibSeguir setHidden:YES];
                //[self.uibAdicionar setHidden:YES];
                [self visibleAdicionar:YES andSeguir:YES];
            }
            /*
             Apelido = "Bruno Costa";
             DataNascimento = "<null>";
             Email = "bruno.costa@iterative.com.br";
             Idade = 0;
             Mensagem = "<null>";
             Nivel = Maufeitor;
             Nome = Bruno;
             NomeEmpresa = "<null>";
             PerfilEmpresa = 0;
             PerfilId = 26;
             Pontuacao = 154;
             Sexo = "<null>";
             Sobrenome = Costa;
             Sucesso = 1;
             URLFoto = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1/c4.4.55.55/p64x64/75515_3948873490099_1044453224_s.jpg";
             */
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilIdOpened"];
            
            [self carregarPosts];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        [self stopActivityIndicator];
    }];
}

-(void)carregarPosts {
    
    if (self.currentPage == 0) {
        
        [self.postList removeAllObjects];
    }
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ((!self.perfil) ||
        ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        //[self.uibAdicionar setHidden:YES];
        //[self.uibEditarDados setHidden:YES];
        //[self.uibSeguir setHidden:YES];
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    }
    else {
        
        //[self.uibAdicionar setHidden:NO];
        //[self.uibEditarDados setHidden:YES];
        //[self.uibSeguir setHidden:NO];
        
        if ([self.perfil objectForKey:@"PerfilId"]) {
            
            [parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"PerfilId"];
        }
        else {
            
            [parameters setObject:[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] forKey:@"PerfilId"];
        }
    }
    
    [parameters setObject:[NSNumber numberWithInteger:self.offset] forKey:@"offset"];
    
    [NSRequestManager requestURL:@"Feed/DePerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,
             "Mensagem":null,
             "Posts":[
             {"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat, augue eu pellentesque suscipit, arcu purus dignissim urna, quis auctor tortor erat nec magna. Pellentesque convallis est non tempor suscipit. Praesent et aliquam justo. In magna justo, sodales et sapien in, commodo aliquet sapien. In a lacus et eros porttitor posuere eu sit amet metus. Vivamus id augue nec libero lacinia euismod. Suspendisse potenti. Cras dictum rutrum pharetra. Mauris ligula nunc, dignissim vitae ante et, lobortis vulputate lectus. Vivamus nec neque sit amet dolor vulputate pulvinar. In vel dictum sem, quis fermentum odio. Ut consectetur quam vitae luctus porttitor. Curabitur quis ante ante. Sed ligula libero, dictum nec malesuada ut, sodales sed risus. Donec adipiscing, tellus id lacinia ornare, quam velit dapibus urna, ut sagittis metus nulla sed augue. Vestibulum luctus, neque et gravida pellentesque, tortor lacus imperdiet nisl, sed commodo tellus nunc at dolor. Integer bibendum porta nisl, luctus laoreet velit posuere non. Aenean gravida placerat urna, quis tempus turpis ullamcorper et. Nulla magna arcu, tincidunt eget nisi in, varius sollicitudin elit. Integer commodo scelerisque odio eget lacinia. Donec a molestie libero, et semper elit.",
             "DataCriacao":"03/02/2014 11:38",
             "Contagios":14,
             "Comentarios":7,
             "Autor":
             {"PerfilId":1,"Apelido":"Autor da Silva",
             "Foto":
             {"Tipo":1,"Url":"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t1/222875_1535228920254_8195527_n.jpg"}},
             "Localizacao":
             {"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"},
             "Pessoas":
             [{"PerfilId":2,"Apelido":"Pessoa dos Santos","Foto":{"Tipo":1,"Url":"https://scontent-a-mia.xx.fbcdn.net/hphotos-prn2/t1/1533838_10200356296869617_805886947_n.jpg"}}],"Midias":[{"Tipo":1,"Url":"https://scontent-b-mia.xx.fbcdn.net/hphotos-prn1/t31/133418_1351987699338_3348746_o.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-ash2/t1/154808_1351977259077_3058407_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-prn1/t1/155008_1351969058872_3574943_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-ash2/t1/430923_2334975593421_1912162679_n.jpg"}
             ]}
             */
            
            if ([self.postList count] == 0) {
                
                self.postList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            }
            else {
                
                [self.postList addObjectsFromArray:[result objectForKey:@"Posts"]];
            }
            
            [self loadTableView];
            
            [self stopActivityIndicator];
            
            /*
             if ([[result objectForKey:@"Posts"] count] > 0) {
             
             [self.postList addObjectsFromArray:[result objectForKey:@"Posts"]];
             [self.tableView reloadData];
             }
             */
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(void) loadTableView {
    
    [self.tableView reloadData];
}

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    [self carregarPosts];
}

-(void) reloadPerfil {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [self.uibAdicionar setHidden:YES];
    [self.uibEditarDados setHidden:YES];
    [self.uibSeguir setHidden:YES];
    
    if ((!self.perfil) ||
        ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"] forKey:@"Id"];
    }
    else {
        
        if ([self.perfil objectForKey:@"PerfilId"]) {
            
            [parameters setObject:[self.perfil objectForKey:@"PerfilId"] forKey:@"Id"];
        }
        else {
            
            [parameters setObject:[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] forKey:@"Id"];
        }
    }
    
    [NSRequestManager requestURL:@"Conta/VerPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self.uiivNivel setImage:[UIImage imageNamed:[NSString stringWithFormat:@"img-nivel-0%i", [[result objectForKey:@"Nivel"] intValue]]]];
            
            if ((!self.perfil) ||
                ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
                
                self.pedidosAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"ListaSolicitacoes"]];
            }
            else {
                
                self.pedidosAjudaList = [NSMutableArray arrayWithCapacity:1];
            }
            
            if ([[result objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                
                [self.uiivFoto setImageWithURLString:[[result objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
            else {
                
                [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
            
            [self.uilApelido setText:[result objectForKey:@"Apelido"]];
            
            if ((![[[result objectForKey:@"Idade"] stringValue] isEqualToString:@""]) &&
                (![[result objectForKey:@"Idade"] isKindOfClass:[NSNull class]])) {
                
                if (([result objectForKey:@"Sexo"] == nil) ||
                    ([[result objectForKey:@"Sexo"] isKindOfClass:[NSNull class]])) {
                    
                    if ([[result objectForKey:@"Idade"] intValue] == 0) {
                        
                        [self.uilIdade setText:@""];
                    }
                    else {
                        
                        [self.uilIdade setText:[NSString stringWithFormat:@"%@ %@", [[result objectForKey:@"Idade"] stringValue], NSLocalizedString(@"Anos", nil)]];
                    }
                }
                else {
                    
                    NSString *sexo;
                    
                    if ([[result objectForKey:@"Sexo"] isEqualToString:@"M"]) {
                        
                        sexo = NSLocalizedString(@"Masculino", nil);
                    }
                    else {
                        
                        sexo = NSLocalizedString(@"Feminino", nil);
                    }
                    
                    [self.uilIdade setText:[NSString stringWithFormat:@"%@ %@, %@", [[result objectForKey:@"Idade"] stringValue], NSLocalizedString(@"Anos", nil), sexo]];
                }
            }
            else {
                
                [self.uilIdade setText:@""];
            }
            
            [self.uilNivel setText:[result objectForKey:@"Nivel"]];
            [self.uilPontuacao setText:[[result objectForKey:@"Pontuacao"] stringValue]];
            
            [self visibleAdicionar:[[result objectForKey:@"Amizade"] boolValue] andSeguir:[[result objectForKey:@"Seguindo"] boolValue]];
            
            if ((!self.perfil) ||
                ([[[self.perfil objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
                
                [self visibleAdicionar:YES andSeguir:YES];
            }

            //[[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilIdOpened"];
            //[self carregarPosts];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(void) visibleAdicionar:(BOOL) statusAdicionar andSeguir:(BOOL) statusSeguir {
    
    if (!statusSeguir) {
        
        [self.uibSeguir setHidden:NO];
    }
    else {
        
        [self.uibSeguir setHidden:YES];
    }
    
    if (statusAdicionar) {
        
        [self.uibAdicionar setHidden:YES];
        [self.uibSeguir setHidden:YES];
        
        [self.uilNivel setHidden:NO];
        [self.uilNivelTitle setHidden:NO];
    }
    else {
        
        [self.uibAdicionar setHidden:NO];
        
        [self.uilNivel setHidden:YES];
        [self.uilNivelTitle setHidden:YES];
    }
}

#pragma mark - Table view Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	// Return the number of sections.
    if ([self.pedidosAjudaList count] == 0) {
        
        return 1;
    }
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if ([self.pedidosAjudaList count] == 0) {
        
        return @"Posts";
    }
    
    if (section == 0) {
        
        return @"Meus Pedidos de Ajuda";
    }
    else if (section == 1) {
        
        return @"Posts";
    }
    
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 30)];
    
    //labelHeader.font = [UIFont fontWithName:@"" size:17.0f];
    labelHeader.textColor = [UIColor whiteColor];
    
    [headerView addSubview:labelHeader];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:184.0f/255.0f green:190.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    
    if ([self.pedidosAjudaList count] == 0) {
        
        labelHeader.text = NSLocalizedString(@"Posts", nil);
        return headerView;
    }
    
    if (section == 0) {
        
        labelHeader.text = NSLocalizedString(@"MeusPedidosOfertas", nil);
    }
    else if (section == 1) {
        
        labelHeader.text = NSLocalizedString(@"Posts", nil);
    }
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.pedidosAjudaList count] == 0) {
        
        //return [self.postList count];
        
        if ([self isPaging:[self.postList count]]) {
            
            return [self.postList count] + 1;
        }
        return [self.postList count];
    }
    
    if (section == 0) {
        
        return [self.pedidosAjudaList count];
    }
    else if (section == 1) {
        
        //return [self.postList count];
        
        if ([self isPaging:[self.postList count]]) {
            
            return [self.postList count] + 1;
        }
        
        return [self.postList count];
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.section == 1) ||
        ([self.pedidosAjudaList count] == 0)) {
        
        if (cell.tag == TAG_LOADING_CELL) {

            if ([self isPaging:indexPath.row]) {
                
                [self refreshData:(self.currentPage * NUMERO_ITENS_POR_PAGINA)];
                
                self.currentPage++;
            }
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.pedidosAjudaList count] == 0) {
        
        if ([self.postList count] == indexPath.row) {
            
            return TAMANHO_LOADING_CELL;
        }
        
        
            NSDictionary *post = [self.postList objectAtIndex:indexPath.row];
            if ([[post objectForKey:@"Midias"] count] == 0) {
                
                return (405.0f - 190.0f);
            }
            
            return 405.0f;
        
    }
    
    if (indexPath.section == 0) {
        
        return 320.0f;
    }
    else if (indexPath.section == 1) {
        
        if ([self.postList count] == indexPath.row) {
            
            return TAMANHO_LOADING_CELL;
        }
        
        

            NSDictionary *post = [self.postList objectAtIndex:indexPath.row];
            if ([[post objectForKey:@"Midias"] count] == 0) {
                
                return (405.0f - 190.0f);
            }
        
        return 405.0f;
    }
    
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.section == 0) &&
        ([self.pedidosAjudaList count] > 0)) {
        
        static NSString *cellIdentifier = @"OfertaAjudaCell";
        
        OfertaPedidoAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        [cell reloadCell:[self.pedidosAjudaList objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if ((indexPath.section == 1) ||
             ([self.pedidosAjudaList count] == 0)) {
        
        if (indexPath.row == [self.postList count]) {
            
            return [self loadingCell];
        }
        
        static NSString *cellIdentifier = @"PostTableViewCell";
        
        PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if ([self.postList count] == 0) {
            
            return cell;
        }
        [cell setParent:self];
        [cell setPostList:self.postList];
        [cell reloadCell:[self.postList objectAtIndex:indexPath.row] andTableView:tableView andIndexPath:indexPath andAllowDeleted:YES andReloadDataBlock:^{
            
            [self reloadPerfil];
        }];
        cell.tag = 1;

        return cell;
    }
    
    return nil;
}

#pragma mark - Table view Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ((indexPath.section == 0) &&
        ([self.pedidosAjudaList count] > 0)) {
        
        EditarPedirAjudaViewController *editarPedirAjudaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditarPedirAjuda"];
        editarPedirAjudaViewController.itemAjuda = [self.pedidosAjudaList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:editarPedirAjudaViewController animated:YES];
    }
    
    /*
    else if ((indexPath.section == 1) ||
             ([self.pedidosAjudaList count] == 0)) {
        
        VerPerfilViewController *verPerfilViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
        [self.navigationController pushViewController:verPerfilViewController animated:YES];
    }
    */
}

@end

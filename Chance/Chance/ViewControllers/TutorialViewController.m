//
//  TutorialViewController.m
//  TicketUP
//
//  Created by Luiz Carlos Sant'Ana Junior on 04/09/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
    /*
	// Do any additional setup after loading the view.
    //listaTutorial
    NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor], nil];
    
    for (int i = 0; i < colors.count; i++) {
        CGRect frame;
        frame.origin.x = self.uiscTutorial.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.uiscTutorial.frame.size;
        
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        subview.backgroundColor = [colors objectAtIndex:i];
        [self.uiscTutorial addSubview:subview];
    }
    
    self.uiscTutorial.contentSize = CGSizeMake(self.uiscTutorial.frame.size.width * colors.count, self.uiscTutorial.frame.size.height);
    */
    self.paginaTutorial = 1;
    //self.uiivTutorial
    
    if ([[[NSLocale autoupdatingCurrentLocale] objectForKey:NSLocaleLanguageCode] isEqualToString:@"en"]) {
    
        [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles1"]];
    }
    else {
        
        [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial1"]];
    }
    
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setTitle:@"Tutorial"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"tutorial"];
    
    //[UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)OnProsseguir:(id)sender {
    
    [UIView beginAnimations:@"changePage" context:nil];
    
    if (self.paginaTutorial == 1) {
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles2"]];
        }
        else {
            
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial2"]];
        }
    }
    else if (self.paginaTutorial == 2) {
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles3"]];
        }
        else {
            
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial3"]];
        }
    }
    else if (self.paginaTutorial == 3) {
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles4"]];
        }
        else {
            
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial4"]];
        }
    }
    else if (self.paginaTutorial == 4) {
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles5"]];
        }
        else {
            
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial5"]];
        }
    }
    else if (self.paginaTutorial == 5) {
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial_ingles6"]];
        }
        else {
            
            [self.uiivTutorial setImage:[UIImage imageNamed:@"tutorial6"]];
        }
    }
    else if (self.paginaTutorial > 5) {
        
        [UIView commitAnimations];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [UIView commitAnimations];
    self.paginaTutorial ++;
}

#pragma mark - Internal Methods

- (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.uiscTutorial.frame.size.width;
    int page = floor((self.uiscTutorial.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.uipcTutorial.currentPage = page;
}

/*
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}
*/
#pragma mark -

@end

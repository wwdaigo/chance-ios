//
//  TabBarViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 27/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EditarContaViewController.h"
#import "FeedAmigosViewController.h"
#import "ProcurarAmigosViewController.h"
#import "SobreViewController.h"
#import "SolicitacoesViewController.h"
#import "TutorialViewController.h"

@interface TabBarViewController : UITabBarController<FBLoginViewDelegate, UIActionSheetDelegate, UITabBarControllerDelegate, UITabBarDelegate> {
    
}

@end

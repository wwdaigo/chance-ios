//
//  EditarPedirAjudaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 4/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "EditarPedirAjudaViewController.h"

@interface EditarPedirAjudaViewController ()

@end

@implementation EditarPedirAjudaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.locationManager = [[CLLocationManager alloc] init];
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        [self.locationManager startUpdatingLocation];
    }
    
    if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
        
        [self.navigationItem setTitle:NSLocalizedString(@"OferecerAjuda", nil)];
        [self.uilDados setText:NSLocalizedString(@"DadosContatoOferta", nil)];
        
        [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
        [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
    }
    else {
        
        [self.navigationItem setTitle:NSLocalizedString(@"PedirAjuda", nil)];
        [self.uilDados setText:NSLocalizedString(@"DadosContatoAjuda", nil)];
        
        [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
        [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
    }
    
    [self.uibAlterar setTitle:NSLocalizedString(@"Salvar", nil) forState:UIControlStateNormal];
    [self.uibExcluir setTitle:NSLocalizedString(@"Excluir", nil) forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.uilNome setText:[[self.itemAjuda objectForKey:@"Categoria"] objectForKey:@"Nome"]];
    
    if (([[[self.itemAjuda objectForKey:@"Categoria"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) &&
        (![[[[self.itemAjuda objectForKey:@"Categoria"] objectForKey:@"Foto"] objectForKey:@"Url"] isKindOfClass:[NSNull class]])) {
        
        [self.uiivCategoria setImageWithURLString:[[[self.itemAjuda objectForKey:@"Categoria"] objectForKey:@"Foto"] objectForKey:@"Url"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
    }
    else {
        
        [[self uiivCategoria] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
    }
    
    if (![[self.itemAjuda objectForKey:@"Texto"] isKindOfClass:[NSNull class]]) {
        
        [self.uitxvDescricao setText:[self.itemAjuda objectForKey:@"Texto"]];
    }
    else {
        
        if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
        
            [self.uitxvDescricao setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
        }
        else {
            
            [self.uitxvDescricao setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
        }
    }
    
    if (![[self.itemAjuda objectForKey:@"TextoDeContato"] isKindOfClass:[NSNull class]]) {
    
    
        [self.uitxvContato setText:[self.itemAjuda objectForKey:@"TextoDeContato"]];
    }
    else {
        
        if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
            
            [self.uitxvContato setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
        }
        else {
            
            [self.uitxvContato setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)OnAlterar:(id)sender {
    
    if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
        
        if (([self.uitxvDescricao.text isEqualToString:@"Escreva aqui qual a oferta de ajuda."]) ||
            ([self.uitxvDescricao.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoOfertaAjudaObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
        
        if (([self.uitxvContato.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarOferta", nil)]) ||
            ([self.uitxvContato.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoContatoObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
    }
    else {
        
        if (([self.uitxvDescricao.text isEqualToString:NSLocalizedString(@"EscrevaPedidoAjuda", nil)]) ||
            ([self.uitxvDescricao.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoPedidoAjudaObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
        
        if (([self.uitxvContato.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)]) ||
            ([self.uitxvContato.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoContatoObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
    }
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //categoria - CategoriaId
    [parameters setObject:[[self.itemAjuda objectForKey:@"Categoria"] objectForKey:@"CategoriaId"] forKey:@"CategoriaId"];
    
    if (self.localizacaoAtual) {
        
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:self.localizacaoAtual.coordinate.latitude], @"Latitude", [NSNumber numberWithFloat:self.localizacaoAtual.coordinate.longitude], @"Longitude", self.cidadeAtual, @"Descricao", nil]forKey:@"Localizacao"];
    }
    
    [parameters setObject:self.uitxvDescricao.text forKey:@"Texto"];
    [parameters setObject:self.uitxvContato.text forKey:@"TextoDeContato"];
    
    [parameters setObject:[NSNumber numberWithBool:[[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]] forKey:@"TipoAjuda"];
    /*
     Tipo - 0 : Pedir e 1: Oferecer
     */
    
    [parameters setObject:[self.itemAjuda objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    
    [NSRequestManager requestURL:@"Ajuda/Editar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             */
            
            if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
                
                [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
                [self.uitxvDescricao becomeFirstResponder];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"OfertaAlteradoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
                
                [uialErro show];
            }
            else {
                
                [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
                [self.uitxvDescricao becomeFirstResponder];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PedidoAlteradoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
                
                [uialErro show];
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnExcluir:(id)sender {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //PedidoId
    [parameters setObject:[self.itemAjuda objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    
    [NSRequestManager requestURL:@"Ajuda/Excluir" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {

            if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {

                [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
                [self.uitxvDescricao becomeFirstResponder];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"OfertaExcluidaSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
                
                [uialErro show];
            }
            else {
                
                [self.uitxvDescricao setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
                [self.uitxvDescricao becomeFirstResponder];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PedidoExcluidoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
                
                [uialErro show];
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"NaoFoiPossivelAcharLocalizacao", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
    if (!self.localizacaoAtual) {
        
        NSLog(@"didUpdateToLocation: %@", newLocation);
        self.localizacaoAtual = newLocation;
        
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                
                self.cidadeAtual = [NSString stringWithFormat:@"%@, %@",[placemark locality], [placemark administrativeArea]];
                break;
            }
        }];
    }
}

#pragma mark - Alert Delegate


#pragma mark - TextField Delegate

-(void) hideKeyboard {
    
    [self.view resignFirstResponder];
    [self.uitxvContato resignFirstResponder];
    [self.uitxvDescricao resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarOferta", nil)]) {
                
                [textView setText:@""];
            }
        }
        else if ([textView isEqual:self.uitxvDescricao]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EscrevaOfertaAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
    }
    else {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
        else if ([textView isEqual:self.uitxvDescricao]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EscrevaPedidoAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
    }
    
    CGRect textFieldRect = [self.view.window convertRect:textView.bounds fromView:textView];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	if (heightFraction < 0.0) {
        
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
        
		heightFraction = 1.0;
	}
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
		self.animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	}
	else {
        
		self.animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([[self.itemAjuda objectForKey:@"TipoAjuda"] boolValue]) {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
            }
        }
        else if ([textView isEqual:self.uitxvDescricao]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
            }
        }
    }
    else {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
            }
        }
        else if ([textView isEqual:self.uitxvDescricao]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
            }
        }
    }
    
    CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

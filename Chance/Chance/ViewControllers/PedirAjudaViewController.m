//
//  PedirAjudaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PedirAjudaViewController.h"
#import "ListaCategoriaViewController.h"

@interface PedirAjudaViewController ()<ListaCategoriaDelegate>

@end

@implementation PedirAjudaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)changeViewName
{
    if ([self tipo]) {
        
        [self.navigationItem setTitle:NSLocalizedString(@"OferecerAjuda", nil)];
        [self.uibPedirAjuda setTitle:NSLocalizedString(@"PublicarOfertaAjuda", nil) forState:UIControlStateNormal];
        [self.uilDados setText:NSLocalizedString(@"DadosContatoOferta", nil)];
        
        [self.uitxvPedidoAjuda setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
        [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
    }
    else {
        
        [self.navigationItem setTitle:NSLocalizedString(@"PedirAjuda", nil)];
        [self.uibPedirAjuda setTitle:NSLocalizedString(@"PublicarPedidoAjuda", nil) forState:UIControlStateNormal];
        [self.uilDados setText:NSLocalizedString(@"DadosContatoAjuda", nil)];
        
        [self.uitxvPedidoAjuda setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
        [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.locationManager = [[CLLocationManager alloc] init];
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        [self.locationManager startUpdatingLocation];
    }
    
    [self changeViewName];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.uilNome setText:[self.categoria objectForKey:@"Nome"]];
    
    if (([[self.categoria objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) &&
        (![[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"] isKindOfClass:[NSNull class]])) {
        
        [self.uiivCategoria setImageWithURLString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
        
        /*
        //[MRProgressOverlayView showOverlayAddedTo:self.uiivCategoria title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"]]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self.uiivCategoria setImage:(UIImage *)responseObject];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivCategoria animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [self.uiivCategoria setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivCategoria animated:YES];
        }];
        //[[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[self.oferta objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        [operation start];
        */
    }
    else {
        
        [[self uiivCategoria] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)OnPedirAjuda:(id)sender {
    
    
    if(!self.categoria){
     
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Selecione uma categoria!" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        return;
    }
    
    if ([self tipo]) {
        
        if (([self.uitxvPedidoAjuda.text isEqualToString:NSLocalizedString(@"EscrevaOfertaAjuda", nil)]) ||
            ([self.uitxvPedidoAjuda.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoOfertaAjudaObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
        
        if (([self.uitxvContato.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarOferta", nil)]) ||
            ([self.uitxvContato.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoContatoObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
    }
    else {
        
        if (([self.uitxvPedidoAjuda.text isEqualToString:NSLocalizedString(@"EscrevaPedidoAjuda", nil)]) ||
            ([self.uitxvPedidoAjuda.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoPedidoAjudaObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
        
        if (([self.uitxvContato.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)]) ||
            ([self.uitxvContato.text isEqualToString:@""])) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CampoContatoObrigatorio", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
    }
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //categoria - CategoriaId
    [parameters setObject:[self.categoria objectForKey:@"CategoriaId"] forKey:@"CategoriaId"];

    if (self.localizacaoAtual) {
        
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:self.localizacaoAtual.coordinate.latitude], @"Latitude", [NSNumber numberWithFloat:self.localizacaoAtual.coordinate.longitude], @"Longitude", self.cidadeAtual, @"Descricao", nil]forKey:@"Localizacao"];
    }
    
    [parameters setObject:self.uitxvPedidoAjuda.text forKey:@"Texto"];
    [parameters setObject:self.uitxvContato.text forKey:@"TextoDeContato"];
    
    
    if (self.tipo) {
    
        [parameters setObject:[NSNumber numberWithInt:1] forKey:@"TipoAjuda"];
    }
    else {
        
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"TipoAjuda"];
    }
    
    /*
     Tipo - 0 : Pedir e 1: Oferecer
     */
    
    [NSRequestManager requestURL:@"Ajuda/Pedir" andParameters:parameters andButton:self.uibPedirAjuda andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            if ([self tipo]) {
            
                [self.uitxvPedidoAjuda setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
                [self.uitxvPedidoAjuda becomeFirstResponder];
            
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"OfertaSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
            
                [uialErro show];
            }
            else {
                
                [self.uitxvPedidoAjuda setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
                [self.uitxvContato setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
                [self.uitxvPedidoAjuda becomeFirstResponder];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PedidoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:1];
                
                [uialErro show];
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"NaoFoiPossivelAcharLocalizacao", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
    if (!self.localizacaoAtual) {
        
        NSLog(@"didUpdateToLocation: %@", newLocation);
        self.localizacaoAtual = newLocation;
        
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                
                self.cidadeAtual = [NSString stringWithFormat:@"%@, %@",[placemark locality], [placemark administrativeArea]];
                break;
            }
        }];
    }
}

#pragma mark - TextField Delegate

-(void) hideKeyboard {
    
    [self.view resignFirstResponder];
    [self.uitxvContato resignFirstResponder];
    [self.uitxvPedidoAjuda resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([self tipo]) {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarOferta", nil)]) {
                
                [textView setText:@""];
            }
        }
        else if ([textView isEqual:self.uitxvPedidoAjuda]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EscrevaOfertaAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
    }
    else {
    
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
        else if ([textView isEqual:self.uitxvPedidoAjuda]) {
            
            if ([textView.text isEqualToString:NSLocalizedString(@"EscrevaPedidoAjuda", nil)]) {
                
                [textView setText:@""];
            }
        }
    }
    
    CGRect textFieldRect = [self.view.window convertRect:textView.bounds fromView:textView];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	if (heightFraction < 0.0) {
        
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
        
		heightFraction = 1.0;
	}
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
		self.animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	}
	else {
        
		self.animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([self tipo]) {
        
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EssesDadosAceitarOferta", nil)];
            }
        }
        else if ([textView isEqual:self.uitxvPedidoAjuda]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EscrevaOfertaAjuda", nil)];
            }
        }
    }
    else {
    
        if ([textView isEqual:self.uitxvContato]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EssesDadosAceitarAjuda", nil)];
            }
        }
        else if ([textView isEqual:self.uitxvPedidoAjuda]) {
            
            if ([textView.text isEqualToString:@""]) {
                
                [textView setText:NSLocalizedString(@"EscrevaPedidoAjuda", nil)];
            }
        }
    }
    
    CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}

/*
 -(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
 
 if ([text isEqualToString:@""]) {
 
 return YES;
 }
 
 return YES;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)onChangeViewNames:(id)sender {
    
    if(self.uiscTipoPedido.selectedSegmentIndex == 0)
    {
        self.tipo = NO;
    }
    else {
        
        self.tipo = YES;
    }
    
    [self changeViewName];
    
}

- (IBAction)onShowSelectCategoria:(id)sender {
    
    NSMutableArray *categorias =  [NSMutableArray arrayWithArray:self.categoriasList];
    
    ListaCategoriaViewController *listaCategoriaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ListaCategoriaVC"];
    
    [categorias removeObjectAtIndex:0];
    
    listaCategoriaViewController.categorias = categorias;
    listaCategoriaViewController.delegate = self;
    listaCategoriaViewController.type = 1000;
    
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:listaCategoriaViewController];
    
    
    
    formSheet.presentedFormSheetSize = CGSizeMake(300, self.view.frame.size.height - 100);
    //
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    
    //formSheet.shadowRadius = 2.0;
    
    //formSheet.shadowOpacity = 0.3;
    
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    
    formSheet.shouldCenterVertically = YES;
    
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    
    //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
    
    
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
    
    // formSheet.landscapeTopInset = 50;
    
    // formSheet.portraitTopInset = 100;
    
    
    
    formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
        
        
        
    };
    
    
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        
        
        
        
        
    };
    
    
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    
    
    //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
    
    
    
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
        
        
    }];
    
}


-(void)didSelectCategoria:(NSDictionary *)categoria{
    
    /*
     {"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}
     */
    
    
    self.categoria = categoria;
    
    self.uilbCategoria.text = [categoria objectForKey:@"Nome"];
    
}
@end

//
//  FeedGlobalViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 10/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "FeedGlobalViewController.h"

@interface FeedGlobalViewController ()

@end

@implementation FeedGlobalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.feedList = [NSMutableArray arrayWithCapacity:0];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([self.feedList count] == 0) {
        
        [self refreshData:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Internal actions

-(void) loadTableView {
    
    [self performBlock:^{
        
        [self.uiaiLoading setHidden:YES];
    } afterDelay:1];
    
    [self.tableView reloadData];
}

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    if (page == 0) {
        
        [self.feedList removeAllObjects];
    }
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //(self.currentPage * NUMERO_ITENS_POR_PAGINA)
    [parameters setObject:[NSNumber numberWithInt:self.offset] forKey:@"offset"];
    
    [NSRequestManager requestURL:@"Feed/Global" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,
             "Mensagem":null,
             "Posts":[
             {"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat, augue eu pellentesque suscipit, arcu purus dignissim urna, quis auctor tortor erat nec magna. Pellentesque convallis est non tempor suscipit. Praesent et aliquam justo. In magna justo, sodales et sapien in, commodo aliquet sapien. In a lacus et eros porttitor posuere eu sit amet metus. Vivamus id augue nec libero lacinia euismod. Suspendisse potenti. Cras dictum rutrum pharetra. Mauris ligula nunc, dignissim vitae ante et, lobortis vulputate lectus. Vivamus nec neque sit amet dolor vulputate pulvinar. In vel dictum sem, quis fermentum odio. Ut consectetur quam vitae luctus porttitor. Curabitur quis ante ante. Sed ligula libero, dictum nec malesuada ut, sodales sed risus. Donec adipiscing, tellus id lacinia ornare, quam velit dapibus urna, ut sagittis metus nulla sed augue. Vestibulum luctus, neque et gravida pellentesque, tortor lacus imperdiet nisl, sed commodo tellus nunc at dolor. Integer bibendum porta nisl, luctus laoreet velit posuere non. Aenean gravida placerat urna, quis tempus turpis ullamcorper et. Nulla magna arcu, tincidunt eget nisi in, varius sollicitudin elit. Integer commodo scelerisque odio eget lacinia. Donec a molestie libero, et semper elit.",
             "DataCriacao":"03/02/2014 11:38",
             "Contagios":14,
             "Comentarios":7,
             "Autor":
             {"PerfilId":1,"Apelido":"Autor da Silva",
             "Foto":
             {"Tipo":1,"Url":"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t1/222875_1535228920254_8195527_n.jpg"}},
             "Localizacao":
             {"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"},
             "Pessoas":
             [{"PerfilId":2,"Apelido":"Pessoa dos Santos","Foto":{"Tipo":1,"Url":"https://scontent-a-mia.xx.fbcdn.net/hphotos-prn2/t1/1533838_10200356296869617_805886947_n.jpg"}}],"Midias":[{"Tipo":1,"Url":"https://scontent-b-mia.xx.fbcdn.net/hphotos-prn1/t31/133418_1351987699338_3348746_o.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-ash2/t1/154808_1351977259077_3058407_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-prn1/t1/155008_1351969058872_3574943_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-ash2/t1/430923_2334975593421_1912162679_n.jpg"}
             ]}
             */
            
            if ([self.feedList count] == 0) {
                
                self.pageLoad = YES;
                self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            }
            else {
                
                if ([[result objectForKey:@"Posts"] count] > 0) {
                    
                    [self.feedList addObjectsFromArray:[result objectForKey:@"Posts"]];
                    self.currentPage++;
                }
                else {
                    
                    self.pageLoad = NO;
                }
            }
            [self performSelector:@selector(loadTableView) withObject:nil afterDelay:1];
            
            /*
            if ([[result objectForKey:@"Posts"] count] > 0) {
                
                [self.feedList addObjectsFromArray:[result objectForKey:@"Posts"]];
                [self.tableView reloadData];
            }
            */
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - Table view Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	// Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self isPaging:[self.feedList count]]) {
        
        return [self.feedList count] + 1;
    }
    
    return [self.feedList count];
    
    /*
     if ([self.feedList count] == 0) {
     
     return 0;
     }
     else if ([self.feedList count] < NUMERO_ITENS_POR_PAGINA) {
     
     return [self.feedList count];
     }
     
     // Return the number of rows in the section.
     return ([self.feedList count] + 1);
     */
}

/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (cell.tag == TAG_LOADING_CELL) {
        
        if ([self isPaging:indexPath.row]) {
            
            [self refreshData:(self.currentPage * NUMERO_ITENS_POR_PAGINA)];
            
            self.currentPage++;
        }
    }
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < self.feedList.count) {
        
        NSDictionary *post = [self.feedList objectAtIndex:indexPath.row];
        if ([[post objectForKey:@"Midias"] count] == 0) {
            
            return (405.0f - 190.0f);
        }
        
        return 405.0f;
    }
    
    return TAMANHO_LOADING_CELL;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < self.feedList.count) {
        
        static NSString *cellIdentifier = @"PostTableViewCell";
        
        PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if ([self.feedList count] == 0) {
            
            return cell;
        }
        
        [cell setParent:self];
        [cell setPostList:self.feedList];
        [cell reloadCell:[self.feedList objectAtIndex:indexPath.row] andTableView:tableView andIndexPath:indexPath];
        
        cell.tag = 1;
        
        return cell;
    }
    
    [self feedOlgPages];
    return [self loadingCell];
}

#pragma mark - Table view Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end

//
//  SolicitacoesViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 1/30/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "SolicitacoesViewController.h"

@interface SolicitacoesViewController ()

@end

@implementation SolicitacoesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.solicitacoesList = [NSMutableArray arrayWithCapacity:0];
    [self refreshData:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [NSRequestManager requestURL:@"Relacionamento/ListarSolicitacoes" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            self.solicitacoesList = [NSMutableArray arrayWithArray:[result objectForKey:@"Pessoas"]];
            [self.tableView reloadData];
            
            //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:[self.solicitacoesList count]];
            /*
             Dados =     {
             Token = c14b62f19fba46399dbd0675d079bc2c;
             };
             Mensagem = "<null>";
             Sucesso = 1;
             */
            /*
             [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"Dados"] objectForKey:@"Token"] forKey:@"Token"];
             
             [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Dados"] forKey:@"Client"];
             [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SelectedAccession"];
             
             TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
             [self.navigationController pushViewController:homeViewController animated:YES];
             */
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.solicitacoesList count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 30)];
    
    //labelHeader.font = [UIFont fontWithName:@"" size:17.0f];
    labelHeader.textColor = [UIColor whiteColor];
    
    [headerView addSubview:labelHeader];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:184.0f/255.0f green:190.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    labelHeader.text = NSLocalizedString(@"VejaSolicitacoesAmizade", nil);
    
    return headerView;
}
/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 76;
}
*/
/*
 
 -(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 
 return @"";
 }
 */

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SolicitacaoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SolicitacaoCell"];
    
    cell.parent = self;
    cell.solicitacoesList = self.solicitacoesList;
    
    [cell reloadCell:[self.solicitacoesList objectAtIndex:indexPath.row] andTableView:tableView andIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    NSDictionary *solicitacao = [self.solicitacoesList objectAtIndex:indexPath.row];
    
    if ((!solicitacao) ||
        ([[solicitacao objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    VerPerfilViewController *verPerfilViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    verPerfilViewController.perfil = [self.solicitacoesList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:verPerfilViewController animated:YES];
    */
}

@end

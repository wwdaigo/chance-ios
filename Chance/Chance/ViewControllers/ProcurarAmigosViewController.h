//
//  ProcurarAmigosViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 11/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ConvidarAmigosFacebookViewController.h"
#import "ProcurarAmigoCell.h"
#import "VerPerfilViewController.h"

@interface ProcurarAmigosViewController : BaseTableViewController<UISearchBarDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UISearchBar *uisbProcurar;

@property(nonatomic, strong) NSMutableArray *procurarAmigosList;
@property(nonatomic, strong) NSMutableArray *amigosList;
@property(nonatomic, strong) NSMutableArray *amigosFacebookList;

@end

//
//  ImageFullscreenViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 19/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFullscreenViewController : BaseViewController<UIScrollViewDelegate> {
   
}

@property(nonatomic, strong) IBOutlet UIScrollView *uisvImageFullscreen;

@property(nonatomic, strong) UIButton *uibDone;
@property(nonatomic, strong) UIImageView *uiivFoto;
@property(nonatomic, strong) NSString *imageURL;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic) BOOL isDoneAnimating;

-(void)centerScrollViewContents;
-(void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer;
-(void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer;

-(void)reloadData:(NSString *) aImageURL;

@end
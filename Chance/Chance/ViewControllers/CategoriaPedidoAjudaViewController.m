//
//  CategoriaPedidoAjudaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "CategoriaPedidoAjudaViewController.h"

@interface CategoriaPedidoAjudaViewController ()

@end

@implementation CategoriaPedidoAjudaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.pedidosAjudaList = [NSMutableArray arrayWithCapacity:0];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManager.distanceFilter=kCLDistanceFilterNone;

    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startMonitoringSignificantLocationChanges];    }
    
    [self.locationManager startUpdatingLocation];
    
    [self.uilNome setText:[self.categoria objectForKey:@"Nome"]];
    
    if (([[self.categoria objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) &&
        (![[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"] isKindOfClass:[NSNull class]])) {
        
        [self.uiivCategoria setImageWithURLString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
        
        /*
        //[MRProgressOverlayView showOverlayAddedTo:self.uiivCategoria title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self.categoria objectForKey:@"Foto"] objectForKey:@"Url"]]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self.uiivCategoria setImage:(UIImage *)responseObject];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivCategoria animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [self.uiivCategoria setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivCategoria animated:YES];
        }];
        //[[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[self.oferta objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        [operation start];
        */
    }
    else {
        
        [[self uiivCategoria] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    [self refreshData:1];
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        
        [self.uiscTipoFiltro setEnabled:YES];
    }
    else {
        
        [self.uiscTipoFiltro setEnabled:NO];
    }
    
    /*
    if ([self tipo]) {
        
        [self.uibPedirOferecerAjuda setTitle:NSLocalizedString(@"OferecerAjuda", nil) forState:UIControlStateNormal];
    }
    else {
        
        [self.uibPedirOferecerAjuda setTitle:NSLocalizedString(@"PedirAjuda", nil) forState:UIControlStateNormal];
    }
    */
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [self.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

#pragma mark - Actions

-(IBAction)OnOferecerAjuda:(id)sender {
    
    PedirAjudaViewController *pedirAjudaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PedirAjuda"];
    pedirAjudaViewController.categoria = [NSDictionary dictionaryWithDictionary:self.categoria];
    pedirAjudaViewController.tipo = YES;
    [self.navigationController pushViewController:pedirAjudaViewController animated:YES];
}

-(IBAction)OnPedirAjuda:(id)sender {
    
    PedirAjudaViewController *pedirAjudaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PedirAjuda"];
    pedirAjudaViewController.categoria = [NSDictionary dictionaryWithDictionary:self.categoria];
    pedirAjudaViewController.tipo = NO;
    [self.navigationController pushViewController:pedirAjudaViewController animated:YES];
}

-(IBAction)OnTrocarFiltro:(id)sender {
    
    [self refreshData:1];
    
    if (self.uiscTipoFiltro.selectedSegmentIndex == 0) {
    
        //130
        [self.uivRaio setHidden:YES];
        [self.uivCabecalho setFrame:CGRectMake(0, 0, 320, 130)];
    }
    else if (self.uiscTipoFiltro.selectedSegmentIndex == 1) {
    
        //170
        [self.uivRaio setHidden:NO];
        [self.uivCabecalho setFrame:CGRectMake(0, 0, 320, 170)];
    }
    
    [self.tableView layoutSubviews];
    [self.tableView reloadSectionIndexTitles];
    [self.tableView reloadData];
}

-(IBAction)OnChangeValue:(id)sender {
    
    self.uilDistancia.text = [NSString stringWithFormat:@"%i Km", [[NSNumber numberWithFloat:self.uisDistancia.value] intValue]];
}

-(IBAction)OnBuscar:(id)sender {

    [self refreshData:1];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"NaoFoiPossivelAcharLocalizacao", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
    if (!self.localizacaoAtual) {
        
        NSLog(@"didUpdateToLocation: %@", newLocation);
        self.localizacaoAtual = newLocation;
        
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                
                self.cidadeAtual = [NSString stringWithFormat:@"%@, %@",[placemark locality], [placemark administrativeArea]];
                break;
            }
        }];
    }
}

#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    //categoria - CategoriaId
    [parameters setObject:[self.categoria objectForKey:@"CategoriaId"] forKey:@"CategoriaId"];
    
    if (self.uiscTipoFiltro.selectedSegmentIndex == 1) {
        
        //[parameters setObject:[NSNumber numberWithInt:(self.uisDistancia.value)] forKey:@"Raio"];
        [parameters setObject:[NSString stringWithFormat:@"%i", [[NSNumber numberWithFloat:self.uisDistancia.value] integerValue]] forKey:@"Raio"];
        
        if (self.localizacaoAtual) {
            
            [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:self.localizacaoAtual.coordinate.latitude], @"Latitude", [NSNumber numberWithFloat:self.localizacaoAtual.coordinate.longitude], @"Longitude", self.cidadeAtual, @"Descricao", nil]forKey:@"Localizacao"];
        }
    }
    
    [NSRequestManager requestURL:@"Ajuda/PedidosPorCategoria" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,"Mensagem":null,"Oferecidas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"MinhasOfertas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"Categorias":[{"CategoriaId":1,"Nome":"Emprego","Descricao":"Procure, ofereça , de uma chance"},{"CategoriaId":2,"Nome":"Educação","Descricao":"Livro, aula e conhecimento"},{"CategoriaId":3,"Nome":"Animais domésticos","Descricao":"Para tomar conta, dar comida, desapareceu?"},{"CategoriaId":4,"Nome":"Carona","Descricao":"compartilhe com vizinhos, ajude a melhorar o trânsito"},{"CategoriaId":5,"Nome":"Carro","Descricao":"Quebrou?, bateu?, conserto"},{"CategoriaId":6,"Nome":"Casa","Descricao":"Mudança, instalação, limpeza, manutenção"},{"CategoriaId":7,"Nome":"Saúde","Descricao":"dicas de saúde, ajuda de especialista, urgências"},{"CategoriaId":8,"Nome":"Tecnologia","Descricao":"recuperação de dados, instalação, produtos"},{"CategoriaId":9,"Nome":"Maternidade","Descricao":"Marinheira de primeira viagem?, querendo matar a vontade de ter um filho?"},{"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}]}
             */
            //@property(nonatomic, strong) NSMutableArray *CategoriasPedidoAjudaList;
            
            self.pedidosAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"Pedidos"]];
            
            [self.tableView reloadSectionIndexTitles];
            [self.tableView reloadData];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.pedidosAjudaList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.uiscTipoFiltro.selectedSegmentIndex == 1) {
        
        return 170.0f;
    }
    
    return 130.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *texto = [[self.pedidosAjudaList objectAtIndex:indexPath.row] objectForKey:@"Texto"];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]};
    
    CGRect titleRect = [texto boundingRectWithSize:(CGSize){(tableView.frame.size.width - 20), 2000.0f} options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    
    CGSize size = titleRect.size;
    
    if (size.height <= 64) {
        
        return 210.0f;
    }
    
    return size.height + 156;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return self.uivCabecalho;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PedidoAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PedidoAjudaCell"];
    
    [cell setParent:self];
    [cell reloadCell:[self.pedidosAjudaList objectAtIndex:indexPath.row]];

    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

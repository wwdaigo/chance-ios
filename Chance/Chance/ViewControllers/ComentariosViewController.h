//
//  ComentariosViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 16/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ComentarioCell.h"

@interface ComentariosViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate> {
    
}

typedef void (^ReloadBlock)();

@property(nonatomic, strong) IBOutlet UITableView *uitvComentarios;
@property(nonatomic, strong) IBOutlet UITextView *uitxvPost;
@property(nonatomic, strong) IBOutlet UIButton *uibPost;
@property(nonatomic, strong) IBOutlet UIView *uivPost;
@property(nonatomic, strong) IBOutlet UILabel *uilCommentTextViewPlaceholder;

@property (strong) UIColor *commentCellHighlightColor;
@property (nonatomic, assign, getter = isInputPlaceholderEnabled) BOOL inputPlaceholderEnabled;

@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, strong) NSMutableArray *comentariosList;
@property(nonatomic, strong) NSString *postId;
@property(nonatomic, strong) NSDictionary *post;

@property (strong) ReloadBlock reloadBlock;

-(void) reloadDataBlock:(ReloadBlock) reloadBlock;

@end

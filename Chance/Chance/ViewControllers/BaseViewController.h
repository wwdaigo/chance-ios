//
//  BaseViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 27/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NSNotificationAlertView.h"
#import "UIViewController+BaseController.h"

@interface BaseViewController : UIViewController {
    
}
@property(nonatomic, strong) IBOutlet UIView *uivUpload;
@property(nonatomic, strong) IBOutlet UIProgressView *uipvProgressBar;
@property(nonatomic, strong) IBOutlet UILabel *uilProgress;

@property (nonatomic, getter = isVisible) BOOL visible;

-(void)showNotificationAlert;
-(void)dismissNotificationAlert;


- (void)startActivityIndicator;

- (void)stopActivityIndicator;

@end

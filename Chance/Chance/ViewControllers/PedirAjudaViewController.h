//
//  PedirAjudaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PedirAjudaViewController : BaseViewController<CLLocationManagerDelegate, UITextViewDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UILabel *uilbCategoria;

@property (strong, nonatomic) IBOutlet UISegmentedControl *uiscTipoPedido;
@property(nonatomic, strong) IBOutlet UIImageView *uiivCategoria;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UILabel *uilDados;
@property(nonatomic, strong) IBOutlet UIButton *uibPedirAjuda;
@property(nonatomic, strong) IBOutlet UITextView *uitxvPedidoAjuda;
@property(nonatomic, strong) IBOutlet UITextView *uitxvContato;

@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, strong) NSDictionary *categoria;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *localizacaoAtual;
@property(nonatomic, strong) NSString *cidadeAtual;

@property(nonatomic, assign) BOOL tipo;

@property (nonatomic,strong) NSMutableArray *categoriasList;

@end

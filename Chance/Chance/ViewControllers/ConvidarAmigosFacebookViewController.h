//
//  ConvidarAmigosFacebookViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 3/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"

#import "ConvidarAmigoFacebookCell.h"

@interface ConvidarAmigosFacebookViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) NSMutableArray *amigosFacebookList;

@end

//
//  TabBarViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 27/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setDelegate:self];
    
	// Do any additional setup after loading the view.
    UIBarButtonItem *configItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ico-config"] style:UIBarButtonItemStylePlain target:self action:@selector(OnConfigurar:)];
    
    /*UIBarButtonItem *procurarAmigoItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ico-procurar-amigo"] style:UIBarButtonItemStylePlain target:self action:@selector(OnProcurarAmigo:)];
    
    UIBarButtonItem *solicitacaoItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ico-solicitacao"] style:UIBarButtonItemStylePlain target:self action:@selector(OnSolicitacao:)];
     */
    
//    NSArray *actionButtonItems = @[configItem, procurarAmigoItem, solicitacaoItem];
     NSArray *actionButtonItems = @[configItem];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    self.navigationItem.leftBarButtonItem = nil;
    
    self.navigationItem.backBarButtonItem.title = @"";
    [self.navigationItem setHidesBackButton:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,100,40)];
    lblTitle.text = [self selectedViewController].navigationItem.title;
    lblTitle.backgroundColor = [UIColor clearColor];
    [lblTitle setTextAlignment:NSTextAlignmentLeft];
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle sizeToFit];
    
    //[self.navigationItem setTitleView:lblTitle];
    UIBarButtonItem *mCustomBarItem = [[UIBarButtonItem alloc] initWithCustomView:lblTitle];
    
    [self.navigationItem setLeftBarButtonItem:mCustomBarItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions

-(IBAction)OnConfigurar:(id)sender {
    /*
     "DoarParaInstituicao" = "Donate to Charity";
     "Solicitacoes" = "Friendship Requests";
     "ProcurarAmigos" = "Search Friends";*/
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Config", nil)  delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) destructiveButtonTitle:nil
        otherButtonTitles:NSLocalizedString(@"DoarParaInstituicao", nil),NSLocalizedString(@"Solicitacoes", nil),NSLocalizedString(@"ProcurarAmigos", nil),
                                  NSLocalizedString(@"Sobre", nil), NSLocalizedString(@"EditarCadastro", nil), NSLocalizedString(@"Tutorial", nil), NSLocalizedString(@"Sair", nil), nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
}

-(IBAction)OnProcurarAmigo:(id)sender {
    
    //ProcurarAmigosViewController.h
    ProcurarAmigosViewController *procurarAmigosViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProcurarAmigos"];
    
    //UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:procurarAmigosViewController];
    
    [self.navigationController pushViewController:procurarAmigosViewController animated:YES];
    //[self.navigationController presentViewController:navBar animated:YES completion:nil];
}

-(IBAction)OnSolicitacao:(id)sender {
    
    //SolicitacoesViewController.h
    SolicitacoesViewController *solicitacoesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Solicitacoes"];
    
    //UINavigationController *navBar = [[UINavigationController alloc] initWithRootViewController:solicitacoesViewController];
    
    [self.navigationController pushViewController:solicitacoesViewController animated:YES];
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            break;
        }
        case 1:
        {
            [self OnSolicitacao:actionSheet];
            break;
        }
        case 2:
        {
            [self OnProcurarAmigo:actionSheet];
            break;
        }
        case 3:
        {
            SobreViewController *sobreViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Sobre"];
            [self.navigationController pushViewController:sobreViewController animated:YES];
            break;
        }
     
        case 4:
        {
            EditarContaViewController *editarContaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditarDados"];
            [self.navigationController pushViewController:editarContaViewController animated:YES];
            break;
        }

        case 5:
        {
            TutorialViewController *tutorialContaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Tutorial"];
            [self.navigationController pushViewController:tutorialContaViewController animated:YES];
            break;
        }
            
        case 6:
        {
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"Token"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        }
    }
}

#pragma mark FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    /*
    // first get the buttons set for login mode
    self.buttonPostPhoto.enabled = YES;
    self.buttonPostStatus.enabled = YES;
    self.buttonPickFriends.enabled = YES;
    self.buttonPickPlace.enabled = YES;
    
    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
    [self.buttonPostStatus setTitle:@"Post Status Update (Logged On)" forState:self.buttonPostStatus.state];
    */
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    
    /*
    // here we use helper properties of FBGraphUser to dot-through to first_name and
    // id properties of the json response from the server; alternatively we could use
    // NSDictionary methods such as objectForKey to get values from the my json object
    self.labelFirstName.text = [NSString stringWithFormat:@"Hello %@!", user.first_name];
    // setting the profileID property of the FBProfilePictureView instance
    // causes the control to fetch and display the profile picture for the user
    self.profilePic.profileID = user.id;
    self.loggedInUser = user;
*/
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    
    /*
    // test to see if we can use the share dialog built into the Facebook application
    FBShareDialogParams *p = [[FBShareDialogParams alloc] init];
    p.link = [NSURL URLWithString:@"http://developers.facebook.com/ios"];
#ifdef DEBUG
    [FBSettings enableBetaFeatures:FBBetaFeaturesShareDialog];
#endif
    BOOL canShareFB = [FBDialogs canPresentShareDialogWithParams:p];
    BOOL canShareiOS6 = [FBDialogs canPresentOSIntegratedShareDialogWithSession:nil];
    
    self.buttonPostStatus.enabled = canShareFB || canShareiOS6;
    self.buttonPostPhoto.enabled = NO;
    self.buttonPickFriends.enabled = NO;
    self.buttonPickPlace.enabled = NO;
    
    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
    [self.buttonPostStatus setTitle:@"Post Status Update (Logged Off)" forState:self.buttonPostStatus.state];
    
    self.profilePic.profileID = nil;
    self.labelFirstName.text = nil;
    self.loggedInUser = nil;
    */
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
   
    // see https://developers.facebook.com/docs/reference/api/errors/ for general guidance on error handling for Facebook API
    // our policy here is to let the login view handle errors, but to log the results
    NSLog(@"FBLoginView encountered an error=%@", error);
}

#pragma mark UITabBarController Delegate

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    NSLog(@"selected index : %i",[tabBarController selectedIndex]);
    
    //UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    //lbNavTitle.textAlignment = NSTextAlignmentLeft;
    //lbNavTitle.text = self.navigationItem.title;
    //self.navigationItem.titleView = lbNavTitle;
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,100,40)];
    lblTitle.text = viewController.navigationItem.title;
    lblTitle.backgroundColor = [UIColor clearColor];
    [lblTitle setTextAlignment:NSTextAlignmentLeft];
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle sizeToFit];
    
    //[self.navigationItem setTitleView:lblTitle];
    UIBarButtonItem *mCustomBarItem = [[UIBarButtonItem alloc] initWithCustomView:lblTitle];
    
    [self.navigationItem setLeftBarButtonItem:mCustomBarItem];
    
    /*
    if (([tabBarController selectedIndex] == 0) ||
        ([tabBarController selectedIndex] == 4)) {
        
        [(BaseTableViewController *) viewController refreshData:0];
    }
    */
    //[self.navigationItem setTitle:viewController.navigationItem.title];
}

@end

//
//  PedidoAjudaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 16/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AjudaOfertadaCell.h"
#import "CategoriaAjudaCell.h"
#import "OfertarAjudaCell.h"

#import "CategoriaPedidoAjudaViewController.h"

@interface PedidoAjudaViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) NSMutableArray *categoriasList;
@property(nonatomic, strong) NSMutableArray *ajudaOferecidaPedirAjudaList;
@property(nonatomic, strong) NSMutableArray *minhasOfertasPedirAjudaList;

@property(nonatomic, strong) NSMutableArray *ajudaOferecidaOferecerAjudaList;
@property(nonatomic, strong) NSMutableArray *minhasOfertasOferecidoAjudaList;

@end

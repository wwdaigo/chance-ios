//
//  EditarContaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 19/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "EditarContaViewController.h"

@interface EditarContaViewController ()

@end

@implementation EditarContaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imagemPerfilParameters = nil;
    // Do any additional setup after loading the view.
    //self.uiretvEditarDados = [[RETableViewManager alloc] initWithTableView:self.uitvEditarDados];
    
    //RETableViewSection *sectionPessoais = [RETableViewSection sectionWithHeaderTitle:@"Dados Pessoais"];
    //[self.uiretvEditarDados addSection:sectionPessoais];
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"] forKey:@"Id"];
    
    [NSRequestManager requestURL:@"Conta/VerPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            if ([[result objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                
                [self.uiivFoto setImageWithURLString:[[result objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
                
                /*
                 //[MRProgressOverlayView showOverlayAddedTo:self.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
                 
                 NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[result objectForKey:@"Foto"] objectForKey:@"UrlThumb"]]];
                 
                 AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
                 operation.responseSerializer = [AFImageResponseSerializer serializer];
                 
                 [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 [self.uiivFoto setImage:(UIImage *)responseObject];
                 [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 NSLog(@"Error: %@", error);
                 [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
                 [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
                 }];
                 
                 [operation start];
                 */
            }
            else {
                
                [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
            
            [self.uitfNome setText:[result objectForKey:@"Nome"]];
            
            [self.uitfSobrenome setText:[result objectForKey:@"Sobrenome"]];
            
            if ([[result objectForKey:@"PerfilEmpresa"] boolValue]) {
                
                [self.uiswEmpresa setOn:YES];
                
                if (![[result objectForKey:@"NomeEmpresa"] isKindOfClass:[NSNull class]]) {
                    
                    [self.uitfNomeEmpresa setEnabled:YES];
                    [self.uitfNomeEmpresa setText:[result objectForKey:@"NomeEmpresa"]];
                }
                else {
                    
                    [self.uitfNomeEmpresa setEnabled:NO];
                }
            }
            else {
                
                [self.uiswEmpresa setOn:NO];
            }
            
            if (![[result objectForKey:@"Sexo"] isKindOfClass:[NSNull class]]) {
                
                if ([[result objectForKey:@"Sexo"] isEqualToString:@"M"]) {
                    
                    [self.uisgcSexo setSelectedSegmentIndex:0];
                }
                else {
                    
                    [self.uisgcSexo setSelectedSegmentIndex:1];
                }
            }
            
            if (![[result objectForKey:@"DataNascimento"] isKindOfClass:[NSNull class]]) {
                
                if ([result objectForKey:@"DataNascimento"]) {
                    
                    self.uitfDataNascimento.text = [result objectForKey:@"DataNascimento"];
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                    
                    self.dataNascimento = [dateFormatter dateFromString:[result objectForKey:@"DataNascimento"]];
                    
                    [self.uidpDataNascimento setDate:[dateFormatter dateFromString:[result objectForKey:@"DataNascimento"]]];
                }
            }
            
            self.uitfEmail.text = [result objectForKey:@"Email"];
            
            /*
             Apelido = "Bruno Costa";
             DataNascimento = "<null>";
             Email = "bruno.costa@iterative.com.br";
             Idade = 0;
             Mensagem = "<null>";
             Nivel = Maufeitor;
             Nome = Bruno;
             NomeEmpresa = "<null>";
             PerfilEmpresa = 0;
             PerfilId = 26;
             Pontuacao = 154;
             Sexo = "<null>";
             Sobrenome = Costa;
             Sucesso = 1;
             URLFoto = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/t1/c4.4.55.55/p64x64/75515_3948873490099_1044453224_s.jpg";
             */
            
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilIdOpened"];
            //[[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:result] forKey:@"Perfil"];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    //[self.uidpDataNascimento setMinimumDate:[NSDate dateWithTimeIntervalSince1970:(-(60*60*24*30*12*70))]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [self.uidpDataNascimento setMinimumDate:[dateFormatter dateFromString:@"1900-01-01"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Actions

-(IBAction)OnCancelarData:(id)sender {
    
    [self hideDate];
}

-(IBAction)OnSelecionarData:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:self.uidpDataNascimento.date forKey:@"dateSelected"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *textDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:self.uidpDataNascimento.date]];
    
    [self.uitfDataNascimento setText:textDate];
    self.dataNascimento = self.uidpDataNascimento.date;
    [self hideDate];
}


-(IBAction)OnHabilitarEmpresa:(id)sender {
    
    if (self.uiswEmpresa.on) {
        
        [self.uitfNomeEmpresa setEnabled:YES];
    }
    else {
        
        [self.uitfNomeEmpresa setEnabled:NO];
    }
}

-(IBAction)OnAlterarSenha:(id)sender {
    
    AlterarSenhaViewController *alterarSenhaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AlterarSenha"];
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:alterarSenhaViewController];
    
    formSheet.presentedFormSheetSize = CGSizeMake(300, 180);
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    //formSheet.shadowRadius = 2.0;
    //formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
    // formSheet.landscapeTopInset = 50;
    // formSheet.portraitTopInset = 100;
    
    formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
        
    };
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        
        
    };
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
    
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
}

-(IBAction)OnSalvarAlteracoes:(id)sender {
    
    [self.view resignFirstResponder];
    [self.uitfNome resignFirstResponder];
    [self.uitfSobrenome resignFirstResponder];
    [self.uitfNomeEmpresa resignFirstResponder];
    [self.uitfDataNascimento resignFirstResponder];
    
    if([self.uitfNome.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaNome", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfSobrenome.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSobrenome", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if([self.uitfEmail.text isEqualToString:@""]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaEmail", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if(![Utils IsValidEmail:self.uitfEmail.text]) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"EmailNaoValido", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        [self.uitfEmail becomeFirstResponder];
        return;
    }
    
    if (self.uiswEmpresa.on) {
        
        if([self.uitfNomeEmpresa.text isEqualToString:@""]) {
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaNomeEmpresa", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            return;
        }
    }
    
    [self.uibSalvarAlteracoes setEnabled:NO];
    
    if (self.imagemPerfilParameters) {
        
        [self uploadPhoto];
    }
    else {
        
        [self editarPerfil];
    }
}

-(IBAction)OnShowDate:(id)sender {
    
    [self showDate];
}

-(IBAction)OnUploadImagem:(id)sender {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO) &&
        ([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypePhotoLibrary] == NO) &&
        ([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)){
        
        return;
    }
    
    UIAlertView *uialFoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Selecione", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Camera", nil), NSLocalizedString(@"Album", nil), nil];
    [uialFoto setTag:1];
    [uialFoto show];
}

#pragma mark - Internal Methods

-(void) editarPerfil {
    
    /*
         [NSNotificationAlertView notificationViewWithParentViewController:<#(UIViewController *)#> tintColor:<#(UIColor *)#> title:<#(NSString *)#>];
     */
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"] forKey:@"Id"];
    [parameters setObject:self.uitfNome.text forKey:@"Nome"];
    [parameters setObject:self.uitfSobrenome.text forKey:@"Sobrenome"];
    //[parameters setObject:self.uitfEmail.text forKey:@"Email"];
    
    if (self.dataNascimento) {
        
        [parameters setObject:self.dataNascimento forKey:@"DataNascimento"];
    }
    
    if (self.uisgcSexo.selectedSegmentIndex == 0) {
        
        [parameters setObject:@"M" forKey:@"Sexo"];
    }
    else {
        
        [parameters setObject:@"F" forKey:@"Sexo"];
    }
    
    if (self.uiswEmpresa.on) {
        
        [parameters setObject:@"true" forKey:@"PerfilEmpresa"];
    }
    else {
        
        [parameters setObject:@"false" forKey:@"PerfilEmpresa"];
    }
    [parameters setObject:self.uitfNomeEmpresa.text forKey:@"NomeEmpresa"];
    
    if (self.imagemPerfilData) {
        
        [parameters setObject:[self.imagemPerfilData objectForKey:@"MidiaId"] forKey:@"MidiaId"];
    }
    
    [parameters setObject:self.uitfEmail.text forKey:@"Email"];
    
    [NSRequestManager requestURL:@"Conta/EditarPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            [self.uibSalvarAlteracoes setEnabled:YES];
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             
             */
            
            //[[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Token"] forKey:@"Token"];
            //[[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"PerfilId"] forKey:@"PerfilId"];
            [self.uibSalvarAlteracoes setEnabled:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"UsuarioAlteradoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            /*
             [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"Dados"] objectForKey:@"Token"] forKey:@"Token"];
             
             [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Dados"] forKey:@"Client"];
             [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SelectedAccession"];
             
             TabBarViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
             [self.navigationController pushViewController:homeViewController animated:YES];
             */
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.uibSalvarAlteracoes setEnabled:YES];
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(void) uploadPhoto {
    
    //imagemPerfilParameters
    
    NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:(NSString *)[[self.imagemPerfilParameters objectForKey:@"Data"] path] error:nil];
    NSNumber * size = [properties objectForKey: NSFileSize];
    
    //self.uinavUpload = [NSNotificationAlertView notificationViewWithParentViewController:self.navigationController tintColor:[UIColor colorWithRed:141.0f/255.0f green:187.0f/255.0f blue:33.0f/255.0f alpha:1.0f] title:NSLocalizedString(@"UploadPhoto", nil)];
    
    [self showNotificationAlert];
    
    ////[MRProgressOverlayView showOverlayAddedTo:picker.view.window animated:YES];
    //MRProgressOverlayView *progress = [MRProgressOverlayView showOverlayAddedTo:picker.view animated:YES];
    //[progress setMode:MRProgressOverlayViewModeDeterminateHorizontalBar];
    [self.uilProgress setText:NSLocalizedString(@"UploadPhoto", nil)];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.imagemPerfilParameters objectForKey:@"Tipo"] forKey:@"Tipo"];
    
    //[self.imagemPerfilParameters setObject:fileName forKey:@"FileName"];
    //[self.imagemPerfilParameters setObject:mediaType forKey:@"MediaType"];
    
    [NSRequestManager uploadTaskURL:@"Midia/PostFile" andParameters:parameters andData:[self.imagemPerfilParameters objectForKey:@"Data"] andFile:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileURL:[self.imagemPerfilParameters objectForKey:@"Data"] name:@"file" fileName:[self.imagemPerfilParameters objectForKey:@"Filename"] mimeType:[self.imagemPerfilParameters objectForKey:@"MediaType"] error:nil];
        
    } andProgess:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
        //int aPercentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
        
        //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) size.floatValue) * 100.0);
        
        [self performBlock:^{
            
            float percentDownloaded = ((float) totalBytesSent / (float) size.floatValue);
            NSLog(@"Upload size: %.2f", size.floatValue);
            NSLog(@"Upload totalBytesSent: %.2f", (float)totalBytesSent);
            NSLog(@"Upload percentDownloaded: %.2f", percentDownloaded);
            //[progress setProgress:percentDownloaded];
            
            [self.uipvProgressBar setProgress:percentDownloaded];
            
        } afterDelay:0.3];
        
        
        /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         
         
         });
         */
        
    } andSuccess:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSDictionary *result = (NSDictionary *)responseObject;
        
        //self.navigationItem.rightBarButtonItem = nil;
        //[self.uinavUpload
        //[progress dismiss:YES];
        [self dismissNotificationAlert];
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            [self.uibSalvarAlteracoes setEnabled:YES];
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            self.imagemPerfilData = [NSDictionary dictionaryWithDictionary:result];
            
            [self editarPerfil];
        }
        
    } andFailure:^(NSError *error) {
        
        [self.uibSalvarAlteracoes setEnabled:YES];
        [self dismissNotificationAlert];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.navigationController presentViewController:cameraUI animated:YES completion:^{
                
            }];
        }
        else if (buttonIndex == 2) {
            
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.navigationController presentViewController:cameraUI animated:YES completion:^{
                
            }];
        }
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
    else if (alertView.tag == 2) {
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if (buttonIndex == 1) {
            
            UITextField *uitfSenha = [alertView textFieldAtIndex:0];
            UITextField *uitfConfirmarSenha = [alertView textFieldAtIndex:1];
            
            if([uitfSenha.text isEqualToString:@""]) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSenhaAtual", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
                
                return;
            }
            
            if([uitfConfirmarSenha.text isEqualToString:@""]) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSenhaAtual", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
                
                return;
            }
            
            if(![uitfConfirmarSenha.text isEqualToString:uitfSenha.text]) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PreenchaSenhaAtual", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
                
                return;
            }
            
            //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            [parameters setObject:uitfSenha.text forKey:@"NovaSenha"];
            
            [NSRequestManager requestURL:@"Conta/RecuperarSenha" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    /*
                     Mensagem = "<null>";
                     PerfilId = 26;
                     Sucesso = 1;
                     Token = fa4ba583b8354edfb64d84204867388d;
                     */
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Senha alterada enviada com sucesso." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
        }
    }
    else if (alertView.tag == 3) {
        
        
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    self.imagemPerfilParameters = nil;
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    NSInteger tipoMedia = 0;
    NSString *filePath = @"";
    NSString *fileName = @"";
    NSURL *dataURL;
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        tipoMedia = 1;
        UIImage *originalImage, *editedImage, *imageToSave;
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            
            imageToSave = editedImage;
        }
        else {
            
            imageToSave = originalImage;
        }
        

        //"Imagem deve conter no máximo 2MB"
        if (imageToSave) {
            
             NSData *imgData = UIImagePNGRepresentation(imageToSave);
            
            if ([imgData length] > TAMANHO_MAXIMO_FOTO_CADASTRO) {
                
                [picker dismissViewControllerAnimated:NO completion:nil];
                
                NSString *mensagem = [[NSString stringWithString:NSLocalizedString(@"TamanhoMaximoFotoCadastro", nil) ] stringByReplacingOccurrencesOfString:@"_Tamanho_" withString:[NSString stringWithFormat:@"%i", ((TAMANHO_MAXIMO_FOTO_CADASTRO / 1000) / 1000)]];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:mensagem delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro setTag:3];
                [uialErro show];
                
                return;
            }
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        fileName = @"Image.png";
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        
        [UIImagePNGRepresentation(imageToSave) writeToFile:filePath atomically:YES];
        
        dataURL = [NSURL fileURLWithPath:filePath];
        [self.uiivFoto setImage:imageToSave];
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    }
    
    if (dataURL) {
        
        self.imagemPerfilParameters = [NSMutableDictionary dictionaryWithCapacity:2];
        [self.imagemPerfilParameters setObject:[NSNumber numberWithInteger:tipoMedia] forKey:@"Tipo"];
        [self.imagemPerfilParameters setObject:dataURL forKey:@"Data"];
        [self.imagemPerfilParameters setObject:filePath forKey:@"FilePath"];
        [self.imagemPerfilParameters setObject:fileName forKey:@"FileName"];
        [self.imagemPerfilParameters setObject:mediaType forKey:@"MediaType"];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Picker Trigger

- (void)hideDate {
    
    if (!self.uivData.hidden) {
        
        [UIView animateWithDuration:0.3 animations:^(void) {
            
            self.uivData.frame = CGRectMake(0, self.view.frame.size.height + self.uivData.frame.size.height, self.uivData.frame.size.width, self.uivData.frame.size.height);
        }];
        self.uivData.hidden = YES;
    }
}

- (void)showDate {
    
    [self hideKeyboard];
    [self.uidpDataNascimento setMaximumDate:[NSDate date]];
    if (self.uivData.hidden) {
        
        [UIView animateWithDuration:0.3 animations:^(void) {
            
            self.uivData.frame = CGRectMake(0, self.view.frame.size.height - self.uivData.frame.size.height, self.uivData.frame.size.width, self.uivData.frame.size.height);
        }];
        self.uivData.hidden = NO;
    }
}

#pragma mark - TextField Delegate

-(void) hideKeyboard {
    
    [self.uitfDataNascimento resignFirstResponder];
    [self.uitfEmail resignFirstResponder];
    [self.uitfNome resignFirstResponder];
    [self.uitfNomeEmpresa resignFirstResponder];
    [self.uitfSobrenome resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideKeyboard];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        
        return YES;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	if (heightFraction < 0.0) {
        
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
        
		heightFraction = 1.0;
	}
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
		self.animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	}
	else {
        
		self.animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    /*
     if (textField == self.uitfLogin) {
     
     [self.uitfPassword becomeFirstResponder];
     
     return;
     }
     */
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += self.animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self hideKeyboard];
    
    return YES;
}

@end

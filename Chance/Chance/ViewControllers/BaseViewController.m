//
//  BaseViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 27/11/13.
//  Copyright (c) 2013 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    //[self.navigationItem setTitle:<#(NSString *)#>]
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions

#pragma mark Internal Actions

-(void)showNotificationAlert {
    
        NSTimeInterval animationDuration = 0.4;
        [self.uivUpload setHidden:NO];
    
        CGRect startFrame, endFrame;
        [self animationFramesForVisible:YES startFrame:&startFrame endFrame:&endFrame];
        
            self.uivUpload.frame = startFrame;
    
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.uivUpload setFrame:endFrame];
        } completion:^(BOOL finished) {

        }];
}

-(void)dismissNotificationAlert {
    
    NSTimeInterval animationDuration = 0.4;

    CGRect startFrame, endFrame;
    [self animationFramesForVisible:NO startFrame:&startFrame endFrame:&endFrame];
    
    //self.uivUpload.frame = endFrame;
    [UIView animateWithDuration:animationDuration animations:^{
        
        [self.uivUpload setFrame:startFrame];
    } completion:^(BOOL finished) {
        
        [self.uivUpload setHidden:YES];
    }];
}

#pragma mark Internal Methods

- (CGFloat)topLayoutGuideLengthCalculation {
    
    CGFloat top = MIN([UIApplication sharedApplication].statusBarFrame.size.height, [UIApplication sharedApplication].statusBarFrame.size.width);
    
    if (self && !self.navigationController.navigationBarHidden) {
        
        top += CGRectGetHeight(self.navigationController.navigationBar.frame);
    }
    
    return top;
}

- (CGRect)visibleFrame {
    
    if (!self.isViewLoaded) {
        return CGRectZero;
    }
    
    //CGFloat topLayoutGuideLength = [self topLayoutGuideLengthCalculation];
    
    CGSize transformedSize = CGSizeApplyAffineTransform(self.view.frame.size, self.view.transform);
    CGRect displayFrame = CGRectMake(0, 0, fabs(transformedSize.width),
                                     kNSNotificationAlertViewHeight);
    
    return displayFrame;
}

- (CGRect)hiddenFrame {
    
    if (!self.isViewLoaded) {
        return CGRectZero;
    }
    
    CGFloat topLayoutGuideLength = [self topLayoutGuideLengthCalculation];
    
    CGSize transformedSize = CGSizeApplyAffineTransform(self.view.frame.size, self.view.transform);
    CGRect offscreenFrame = CGRectMake(0, -kNSNotificationAlertViewHeight - topLayoutGuideLength,
                                       fabs(transformedSize.width),
                                       kNSNotificationAlertViewHeight);
    
    return offscreenFrame;
}

- (void)animationFramesForVisible:(BOOL)visible startFrame:(CGRect*)startFrame endFrame:(CGRect*)endFrame {
    
    if (startFrame) *startFrame = visible ? [self hiddenFrame]:[self visibleFrame];
    if (endFrame) *endFrame = visible ? [self visibleFrame] : [self hiddenFrame];
}

- (void)startActivityIndicator
{
    if (![SVProgressHUD isVisible]) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
        [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
        [SVProgressHUD show];
    }
}

- (void)stopActivityIndicator
{
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    [SVProgressHUD dismiss];
}


@end

//
//  CategoriaPedidoAjudaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PedidoAjudaCell.h"
#import "PedirAjudaViewController.h"

@interface CategoriaPedidoAjudaViewController : BaseTableViewController<CLLocationManagerDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivCategoria;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UISegmentedControl *uiscTipoFiltro;
@property(nonatomic, strong) IBOutlet UIView *uivCabecalho;
@property(nonatomic, strong) IBOutlet UIView *uivRaio;
@property(nonatomic, strong) IBOutlet UISlider *uisDistancia;
@property(nonatomic, strong) IBOutlet UILabel *uilDistancia;

@property(nonatomic, strong) NSMutableArray *pedidosAjudaList;
@property(nonatomic, strong) NSDictionary *categoria;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *localizacaoAtual;
@property(nonatomic, strong) NSString *cidadeAtual;

@property(nonatomic, assign) BOOL tipo;

@end

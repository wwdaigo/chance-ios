//
//  AdicionarMidiaViewCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "AdicionarMidiaViewCell.h"

@implementation AdicionarMidiaViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {

    NSLog(@"canPerformAction");
    // The selector(s) should match your UIMenuItem selector
    
    NSLog(@"Sender: %@", sender);
    if (action == @selector(customAction:)) {
        return YES;
    }
    return NO;
}

@end

//
//  MidiaViewCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "MidiaViewCell.h"

@implementation MidiaViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Actions

-(IBAction)OnAbrirMidia:(id)sender {

    /*
    if ([[self.midia objectForKey:@"Tipo"] intValue] == 1) {
        
        --
         ImageFullscreenViewController *imageFullscreenViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"FullscreenImage"];

        [imageFullscreenViewController reloadData:[self.midia objectForKey:@"Url"]];
        
        [self.parent.navigationController presentViewController:imageFullscreenViewController animated:YES completion:nil];
        --
    }
    else
    */
    if ([[self.midia objectForKey:@"Tipo"] intValue] == 2) {
        
        NSURL *videoURL = [NSURL URLWithString:[self.midia objectForKey:@"Url"]];
        
        MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
        
        [[NSNotificationCenter defaultCenter] removeObserver:movieController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:movieController.moviePlayer];
        
        //[[NSNotificationCenter defaultCenter] removeObserver:movieController  name:MPMoviePlayerPlaybackDidFinishNotification object:movieController.moviePlayer];
        
        [self.parent.navigationController presentMoviePlayerViewControllerAnimated:movieController];
        
        [movieController.moviePlayer prepareToPlay];
        [movieController.moviePlayer play];
    }
}

-(IBAction)OnExcluir:(id)sender {
    
    //self.midiaList removeObjectAtIndex:self.indexPath.row];
    //[self.collectionParent deselectItemAtIndexPath:self.indexPath animated:YES];
    [self.collectionParent performBatchUpdates:^{
        
        [self.midiaList removeObjectAtIndex:self.indexPath.row];
        [self.collectionParent deleteItemsAtIndexPaths:[NSArray arrayWithObject:self.indexPath]];
        [self.collectionParent reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:nil];
}

#pragma mark - Internal Actions

-(void) carregandoMidia:(NSMutableArray *) aMidiaList indexPath:(NSIndexPath *)aIndexPath parent:(UIViewController *)aParent {
    
    [self.uibExcluir setHidden:YES];
    
    //self.midiaList = [NSMutableArray arrayWithArray:aMidiaList];
    self.midiaList = aMidiaList;
    
    if ([self.midiaList count] == aIndexPath.row) {
        
        self.midia = [self.midiaList objectAtIndex:(aIndexPath.row)];
        self.indexPath = [NSIndexPath indexPathForRow:(aIndexPath.row) inSection:aIndexPath.section];
    }
    else {
        
        self.midia = [self.midiaList objectAtIndex:aIndexPath.row];
        self.indexPath = aIndexPath;
    }
    self.parent = aParent;
    
    [self carregandoMidia];
}

-(void) carregandoMidia:(NSMutableArray *)aMidiaList indexPath:(NSIndexPath *)aIndexPath parent:(UIViewController *)aParent collection:(UICollectionView *) aCollection {
    
    self.collectionParent = aCollection;
    [self carregandoMidia:aMidiaList indexPath:aIndexPath parent:aParent];
    
    [self.uibExcluir setHidden:NO];
}

-(void) carregandoMidia {
    
    [self.uibMidia setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    //[self.uiivMidia setFrame:CGRectMake(10, 10, (self.frame.size.width - 20), (self.frame.size.height - 20))];
    
    if ([[self.midia objectForKey:@"Tipo"] intValue] == 1) {

        [self.uibMidia setHidden:YES];
        
        self.uiivMidia.contentMode = UIViewContentModeScaleAspectFill;
        
        if (self.permiteFullscreen) {
            
            if ([self.midiaList count] > 0) {
                
                [self.uiivMidia setupImageViewerWithDatasource:self initialIndex:self.indexPath.row onOpen:nil onClose:nil];
            }
        }
        
        self.uiivMidia.clipsToBounds = YES;
        
        [self.uiivMidia setImageWithURL:[NSURL URLWithString:[self.midia objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM] options:SDWebImageRefreshCached];
        
        NSLog(@"Url_ImageThumb:%@", [self.midia objectForKey:@"UrlThumb"]);
        NSLog(@"Url_Image:%@", [self.midia objectForKey:@"Url"]);
        
       // [self.uiivMidia setImageWithProgressIndicatorAndURL:[NSURL URLWithString:[self.midia objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
        
        //[self.uiivMidia setImageWithURLString:[self.midia objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM]];
    }
    else if ([[self.midia objectForKey:@"Tipo"] intValue] == 2) {
        
        ////[MRProgressOverlayView showOverlayAddedTo:self title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];

        [self.uibMidia setHidden:NO];
        
        [self.uiivMidia setImageWithURL:[NSURL URLWithString:[self.midia objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_VIDEO] options:SDWebImageRefreshCached];
        
        //[self.uiivMidia setImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_VIDEO]];
        
        
        
        
        //[self.uiivMidia setImageWithURLString:[self.midia objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_VIDEO]];
        
        //[self performSelectorInBackground:@selector(capturarPrimeiroFrame) withObject:nil];
        
        /*
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.midia objectForKey:@"UrlThumb"]]];

        [self.uiivMidia setImageWithURLRequest:urlRequest placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_VIDEO] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            //[MRProgressOverlayView dismissAllOverlaysForView:self animated:YES];
            [self.uiivMidia setImage:image];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            NSLog(@"Error: %@", error);
            //[MRProgressOverlayView dismissAllOverlaysForView:self animated:YES];
            
            [self.uiivMidia setImage:[UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_VIDEO]];
        }];
        */
            /*
            //[MRProgressOverlayView showOverlayAddedTo:self title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
            
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                
                NSURL *videoURL = [NSURL URLWithString:[self.midia objectForKey:@"Url"]];
                
                AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
                AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
                generate1.appliesPreferredTrackTransform = YES;
                NSError *err = NULL;
                CMTime time = CMTimeMake(1, 2);
                CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
                UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
                
                [self.uiivMidia setImage:one];
                self.uiivMidia.contentMode = UIViewContentModeScaleAspectFit;
                
                [MRProgressOverlayView dismissAllOverlaysForView:self animated:YES];
            }];
            */
            //NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
            //[mainQueue addOperation:operation];
            /*
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [queue addOperation:operation];
            */
    }
}

-(void) capturarPrimeiroFrame {
    
    /*
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.midia objectForKey:@"Url"]]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id urlRequest) {
        
        self.uiivMidia c
        self.backgroundImageView.image = responseObject;
        [self saveImage:responseObject withFilename:@"background.png"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
    */
    /*
    NSString *filePath = [self.midia objectForKey:@"Url"];
    NSFileManager *dfm = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *tmpPath = [[documentPaths lastObject] stringByAppendingPathComponent:@"tmp.mov"];
    [dfm removeItemAtPath:tmpPath error:nil];
    [dfm linkItemAtPath:filePath toPath:tmpPath error:nil];
    */
    AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[self.midia objectForKey:@"Url"]] options:nil];
    AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
    generate1.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 2);
    CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
    UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
    
    [self.uiivMidia setImage:one];
    self.uiivMidia.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.uiivMidia setImageWithURLString:[self.midia objectForKey:@"Url"] imageCache:one];
}

#pragma mark - MHFacebookImageViewer Delegate

- (NSInteger) numberImagesForImageViewer:(MHFacebookImageViewer *)imageViewer {
    
    return [self.midiaList count];
}

-  (NSURL*) imageURLAtIndex:(NSInteger)index imageViewer:(MHFacebookImageViewer *)imageViewer {
    
    return [NSURL URLWithString:[[self.midiaList objectAtIndex:index] objectForKey:@"UrlThumb"]];
    //return [NSURL URLWithString:[[self.midiaList objectAtIndex:index] objectForKey:@"Url"]];
    //return [self.midia objectForKey:@"Url"];
    //return [NSURL URLWithString:[NSString stringWithFormat:@"http://iamkel.net/projects/mhfacebookimageviewer/%i.png",index]];
}

- (UIImage*) imageDefaultAtIndex:(NSInteger)index imageViewer:(MHFacebookImageViewer *)imageViewer{
    
    //NSLog(@"INDEX IS %i",index);
    return [UIImage imageNamed:IMAGEM_DEFAULT_MIDIA_IMAGEM];
    //return [UIImage imageNamed:[NSString stringWithFormat:@"%i_iphone",index]];
}

#pragma mark - MPMoviePlayer Delegate

- (void)movieFinishedCallback:(NSNotification*)aNotification {
    
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded) {
        
        MPMoviePlayerController *moviePlayer = [aNotification object];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];

        [self.parent.navigationController dismissMoviePlayerViewControllerAnimated];
    }
}

@end

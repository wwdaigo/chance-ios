//
//  AdicionarMidiaViewCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdicionarMidiaViewCell : UICollectionViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivAdicionarMidia;

@end

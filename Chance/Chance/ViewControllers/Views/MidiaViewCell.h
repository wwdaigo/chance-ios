//
//  MidiaViewCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ImageFullscreenViewController.h"
#import "MHFacebookImageViewer.h"

@interface MidiaViewCell : UICollectionViewCell<MHFacebookImageViewerDatasource> {
    
}

@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *uiaivMidia;
@property(nonatomic, strong) IBOutlet UIButton *uibMidia;
@property(nonatomic, strong) IBOutlet UIButton *uibExcluir;
@property(nonatomic, strong) IBOutlet UIImageView *uiivMidia;

@property(nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, strong) UICollectionView *collectionParent;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) NSDictionary *midia;
@property(nonatomic, strong) NSMutableArray *midiaList;
@property(nonatomic) BOOL permiteFullscreen;

-(void) carregandoMidia;
-(void) carregandoMidia:(NSMutableArray *)aMidiaList indexPath:(NSIndexPath *)aIndexPath parent:(UIViewController *)aParent;
-(void) carregandoMidia:(NSMutableArray *)aMidiaList indexPath:(NSIndexPath *)aIndexPath parent:(UIViewController *)aParent collection:(UICollectionView *) aCollection;

-(IBAction)OnAbrirMidia:(id)sender;
-(IBAction)OnExcluir:(id)sender;

@end

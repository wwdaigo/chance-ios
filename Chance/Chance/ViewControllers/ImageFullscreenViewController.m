//
//  ImageFullscreenViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 19/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ImageFullscreenViewController.h"

@interface ImageFullscreenViewController ()

@end

@implementation ImageFullscreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.uibDone addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    self.uisvImageFullscreen.backgroundColor = [UIColor clearColor];
    
    if(!self.uiivFoto) {
        
        self.uiivFoto = [[UIImageView alloc]init];
        [self.uisvImageFullscreen addSubview:self.uiivFoto];
        self.uiivFoto.contentMode = UIViewContentModeScaleAspectFill;
    }
    /*
    [self.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.imageURL]] placeholderImage:[UIImage imageNamed:@"icon512x512"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [_scrollViewInsideBlock setZoomScale:1.0f animated:YES];
        [_imageViewInTheBlock setImage:image];
        _imageViewInTheBlock.frame = [_justMeInsideTheBlock centerFrameFromImage:_imageViewInTheBlock.image];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Image From URL Not loaded");
    }];
    */
    //self.imageURL
    
    CGRect scrollViewFrame = self.uisvImageFullscreen.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.uisvImageFullscreen.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.uisvImageFullscreen.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.uisvImageFullscreen.minimumZoomScale = minScale;
    self.uisvImageFullscreen.maximumZoomScale = 1.0f;
    self.uisvImageFullscreen.zoomScale = minScale;
    
    [self centerScrollViewContents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Internal Actions

- (void)didSingleTap:(UITapGestureRecognizer*)recognizer {
    
    if(self.uibDone.superview){
    
        [self hideDoneButton];
    }
    else {
    
        if(self.uisvImageFullscreen.zoomScale == self.uisvImageFullscreen.minimumZoomScale) {
            
            if(!self.isDoneAnimating) {
                
                self.isDoneAnimating = YES;
                [self.view addSubview:self.uibDone];
                self.uibDone.alpha = 0.0f;
                [UIView animateWithDuration:0.2f animations:^{
                
                    self.uibDone.alpha = 1.0f;
                }
                completion:^(BOOL finished) {
                    
                    [self.view bringSubviewToFront:self.uibDone];
                    self.isDoneAnimating = NO;
                }];
            }
        }
        else if(self.uisvImageFullscreen.zoomScale == self.uisvImageFullscreen.maximumZoomScale) {
        
            CGPoint pointInView = [recognizer locationInView:self.uiivFoto];
            [self zoomInZoomOut:pointInView];
        }
    }
}

-(void) reloadData:(NSString *) aImageURL {
    
    self.imageURL = aImageURL;
    
    //UIImage *uiiFoto = nil;
        //[MRProgressOverlayView showOverlayAddedTo:self.uisvImageFullscreen title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.imageURL]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            self.uiivFoto = [[UIImageView alloc] initWithImage:(UIImage *)responseObject];
            self.uiivFoto.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=[(UIImage *)responseObject size]};
            self.uisvImageFullscreen.contentSize = [(UIImage *)responseObject size];
            
            [self.uisvImageFullscreen addSubview:self.uiivFoto];
            
            [MRProgressOverlayView dismissAllOverlaysForView:self.uisvImageFullscreen animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            self.uiivFoto = [[UIImageView alloc] initWithImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            self.uiivFoto.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=[[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] size]};
            self.uisvImageFullscreen.contentSize = [[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] size];
            
            [self.uisvImageFullscreen addSubview:self.uiivFoto];
            //[self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uisvImageFullscreen animated:YES];
        }];
        
        [operation start];
    
    /*
    }
    else {
        
        self.uiivFoto = [[UIImageView alloc] initWithImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        self.uiivFoto.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=[[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] size]};
        self.uisvImageFullscreen.contentSize = [[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] size];
        //[self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    */
    // Do any additional setup after loading the view.
    //self.uiivFoto = [[UIImageView alloc] initWithImage:image];

    
    // Tell the scroll view the size of the contents

    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.uisvImageFullscreen addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [self.uisvImageFullscreen addGestureRecognizer:twoFingerTapRecognizer];
}

- (void) hideDoneButton {
    
    if(!self.isDoneAnimating) {
        
        if(self.uibDone.superview) {
            
            self.isDoneAnimating = YES;
            self.uibDone.alpha = 1.0f;
            [UIView animateWithDuration:0.2f animations:^{
                
                self.uibDone.alpha = 0.0f;
            }
            completion:^(BOOL finished) {
                
                self.isDoneAnimating = NO;
                [self.uibDone removeFromSuperview];
            }];
        }
    }
}

- (void)close:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
#pragma mark - Add Pan Gesture
- (void) addPanGestureToView:(UIView*)view
{
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(gestureRecognizerDidPan:)];
    panGesture.cancelsTouchesInView = YES;
    panGesture.delegate = self;
    [view addGestureRecognizer:panGesture];
    [_gestures addObject:panGesture];
    panGesture = nil;
}

# pragma mark - Avoid Unwanted Horizontal Gesture
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer {
    CGPoint translation = [panGestureRecognizer translationInView:__scrollView];
    return fabs(translation.y) > fabs(translation.x) ;
}

#pragma mark - Gesture recognizer
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    _panOrigin = self.imageView.frame.origin;
    gestureRecognizer.enabled = YES;
    return !_isAnimating;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // Uncomment once iOS7 beta5 bugs for panGestures are worked out
    //    UITableView * tableView = (UITableView*)self.superview;
    //    if ( [tableView respondsToSelector:@selector(panGestureRecognizer)] &&
    //         [otherGestureRecognizer isEqual:(tableView.panGestureRecognizer)] )
    //    {
    //        return NO;
    //    }
    return YES;
}
*/

# pragma mark - UIScrollView Delegate

- (void)centerScrollViewContents {
    
    CGSize boundsSize = self.uisvImageFullscreen.bounds.size;
    CGRect contentsFrame = self.uiivFoto.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.uiivFoto.frame = contentsFrame;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    
    // Get the location within the image view where we tapped
    CGPoint pointInView = [recognizer locationInView:self.uiivFoto];
    
    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.uisvImageFullscreen.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, self.uisvImageFullscreen.maximumZoomScale);
    
    // Figure out the rect we want to zoom to, then zoom to it
    CGSize scrollViewSize = self.uisvImageFullscreen.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    [self.uisvImageFullscreen zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.uisvImageFullscreen.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, self.uisvImageFullscreen.minimumZoomScale);
    [self.uisvImageFullscreen setZoomScale:newZoomScale animated:YES];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    // Return the view that we want to zoom
    return self.uiivFoto;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}

- (void) zoomInZoomOut:(CGPoint)point {

    // Check if current Zoom Scale is greater than half of max scale then reduce zoom and vice versa
    CGFloat newZoomScale = self.uisvImageFullscreen.zoomScale > (self.uisvImageFullscreen.maximumZoomScale/2)?self.uisvImageFullscreen.minimumZoomScale:self.uisvImageFullscreen.maximumZoomScale;
    
    CGSize scrollViewSize = self.uisvImageFullscreen.bounds.size;
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = point.x - (w / 2.0f);
    CGFloat y = point.y - (h / 2.0f);
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    [self.uisvImageFullscreen zoomToRect:rectToZoomTo animated:YES];
}

@end
//
//  ConvidarAmigosFacebookViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 3/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ConvidarAmigosFacebookViewController.h"

@interface ConvidarAmigosFacebookViewController ()

@end

@implementation ConvidarAmigosFacebookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark Actions

-(IBAction)OnConvidarAmigos:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
        NSMutableArray *amigosFacebookList = [(ConvidarAmigosFacebookViewController *)formSheetController.presentedFSViewController amigosFacebookList];
        
        NSMutableArray *amigosSelecionadosList = [NSMutableArray arrayWithCapacity:1];
        for (NSIndexPath *indexPath in [self.tableView indexPathsForSelectedRows]) {
            
            [amigosSelecionadosList addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[amigosFacebookList objectAtIndex:indexPath.row] objectForKey:@"Nome"], @"Nome", [[amigosFacebookList objectAtIndex:indexPath.row] objectForKey:@"Email"], @"Email", nil]];
        }
        
        ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:amigosSelecionadosList forKey:@"Amigos"];
        
        [NSRequestManager requestURL:@"Conta/ConvidarAmigos" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
            
            NSDictionary *result = (NSDictionary *)responseObject;
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:@"Convites enviado com sucesso." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            
        } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
            
            //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];
    }];
}

-(IBAction)OnFechar:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
    }];
}

#pragma mark - Table view Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	// Return the number of sections.
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 30)];
    
    labelHeader.textColor = [UIColor whiteColor];
    
    [headerView addSubview:labelHeader];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:184.0f/255.0f green:190.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    
    if (section == 0) {
        
        labelHeader.text = @"Amigos Facebook";
    }
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [self.amigosFacebookList count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"ConvidarAmigoFacebookCell";
    
    ConvidarAmigoFacebookCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell reloadCell:[self.amigosFacebookList objectAtIndex:indexPath.row]];
    /*
    [[cell uilApelido] setText:[[self.pessoasBuscadasList objectAtIndex:indexPath.row] objectForKey:@"Apelido"]];
    [[cell uilNivel] setText:[[self.pessoasBuscadasList objectAtIndex:indexPath.row]  objectForKey:@"Nivel"]];
    [[cell uilPontuacao] setText:[[[self.pessoasBuscadasList objectAtIndex:indexPath.row]  objectForKey:@"Pontucao"] stringValue]];
    
    if ([[[self.pessoasBuscadasList objectAtIndex:indexPath.row] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        //[MRProgressOverlayView showOverlayAddedTo:cell.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[[self.pessoasBuscadasList objectAtIndex:indexPath.row] objectForKey:@"Foto"] objectForKey:@"Url"]]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [cell.uiivFoto setImage:(UIImage *)responseObject];
            [MRProgressOverlayView dismissAllOverlaysForView:cell.uiivFoto animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [cell.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:cell.uiivFoto animated:YES];
        }];
        
        [operation start];
    }
    else {
        
        [cell.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    */
    return cell;
}

#pragma mark - Table view Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    /*
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.pessoaSelecionada = [self.pessoasBuscadasList objectAtIndex:indexPath.row];
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
        //NSDictionary *pessoaSelecionada = [(AdicionarAmigosViewController *)formSheetController.presentedFSViewController pessoaSelecionada];
        
         if (self.pessoaSelecionada) {
         
         [[(PostViewController *)formSheetController.presentingViewController pessoasList] addObject:self.pessoaSelecionada];
         
         [(PostViewController *)formSheetController.presentingViewController adicionarToken:[self.pessoaSelecionada objectForKey:@"Apelido"] object:self.pessoaSelecionada];
         
         //[self.pessoasList addObject:pessoaSelecionada];
         }
         */
    //}];
}

@end

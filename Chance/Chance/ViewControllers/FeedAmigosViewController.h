//
//  FeedAmigosViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/3/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"
#import "TutorialViewController.h"

@interface FeedAmigosViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *uiaiLoading;

@property(nonatomic, strong) NSMutableArray *feedList;

@property (nonatomic) int qtdAmigos;

@end

//
//  PedidoAjudaViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 16/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PedidoAjudaViewController.h"

@interface PedidoAjudaViewController ()

@end

@implementation PedidoAjudaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.ajudaOferecidaPedirAjudaList = [NSMutableArray arrayWithCapacity:0];
    self.minhasOfertasPedirAjudaList = [NSMutableArray arrayWithCapacity:0];
    self.ajudaOferecidaOferecerAjudaList = [NSMutableArray arrayWithCapacity:0];
    self.minhasOfertasOferecidoAjudaList = [NSMutableArray arrayWithCapacity:0];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self refreshData:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    [super refreshData:page];
    
    ////[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [NSRequestManager requestURL:@"Ajuda/Resumo" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:NO];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            /*
             {"Sucesso":true,"Mensagem":null,"Oferecidas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":"Nunc nec sodales dolor. Praesent dapibus dignissim leo eu ornare. In varius, odio vitae dictum egestas, mauris est tincidunt erat, non porta lectus orci vel neque.","DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"MinhasOfertas":[{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}},{"Autor":{"PerfilId":0,"Apelido":null,"Foto":null,"Amizade":false,"Seguindo":false},"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod, urna et pulvinar congue, justo turpis commodo lorem, vitae mollis elit nulla eget nisi. Cras orci magna, convallis rhoncus ultrices sit amet, hendrerit hendrerit dolor. In hac habitasse platea.","TextoDeContato":null,"DataCriacao":"17/02/2014 08:26","Localizacao":{"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"}}],"Categorias":[{"CategoriaId":1,"Nome":"Emprego","Descricao":"Procure, ofereça , de uma chance"},{"CategoriaId":2,"Nome":"Educação","Descricao":"Livro, aula e conhecimento"},{"CategoriaId":3,"Nome":"Animais domésticos","Descricao":"Para tomar conta, dar comida, desapareceu?"},{"CategoriaId":4,"Nome":"Carona","Descricao":"compartilhe com vizinhos, ajude a melhorar o trânsito"},{"CategoriaId":5,"Nome":"Carro","Descricao":"Quebrou?, bateu?, conserto"},{"CategoriaId":6,"Nome":"Casa","Descricao":"Mudança, instalação, limpeza, manutenção"},{"CategoriaId":7,"Nome":"Saúde","Descricao":"dicas de saúde, ajuda de especialista, urgências"},{"CategoriaId":8,"Nome":"Tecnologia","Descricao":"recuperação de dados, instalação, produtos"},{"CategoriaId":9,"Nome":"Maternidade","Descricao":"Marinheira de primeira viagem?, querendo matar a vontade de ter um filho?"},{"CategoriaId":10,"Nome":"Doação","Descricao":"Sangue, roupa, alimentos, móveis, o que faz bem"}]}
             */
            //@property(nonatomic, strong) NSMutableArray *CategoriasPedidoAjudaList;
            
            /*
             AceitasPorOferta =     (
             );
             CategoriasDeOferecimento =     (
             );
             CategoriasDePedido =     (
             );
             Mensagem = "<null>";
             MinhasOfertasPorOferecimento =     (
             );
             MinhasOfertasPorPedido =     (
             );
             OferecidasParaPedido =     (
             );
             */
            
            self.ajudaOferecidaOferecerAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"AceitasPorOferta"]];
            self.minhasOfertasOferecidoAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"MinhasOfertasPorOferecimento"]];
            
            self.ajudaOferecidaPedirAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"OferecidasParaPedido"]];
            self.minhasOfertasPedirAjudaList = [NSMutableArray arrayWithArray:[result objectForKey:@"MinhasOfertasPorPedido"]];
            
            [self.tableView reloadData];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return [self.ajudaOferecidaPedirAjudaList count];
    }
    else if (section == 1) {
        
        return [self.minhasOfertasPedirAjudaList count];
    }
    else if (section == 2) {
        
        return [self.ajudaOferecidaOferecerAjudaList count];
    }
    else if (section == 3) {
        
        return [self.minhasOfertasOferecidoAjudaList count];
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 30)];
    
    //labelHeader.font = [UIFont fontWithName:@"" size:17.0f];
    labelHeader.textColor = [UIColor whiteColor];
    
    [headerView addSubview:labelHeader];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:184.0f/255.0f green:190.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    
    if (section == 0) {
        
        labelHeader.text = NSLocalizedString(@"QuemQuerTeAjudar", nil);
    }
    else if (section == 1) {
        
        labelHeader.text = NSLocalizedString(@"ContatosQuemQuerTeAjudar", nil);
    }
    else if (section == 2) {
        
        labelHeader.text = NSLocalizedString(@"QuemVoceQuerAjudar", nil);
    }
    else if (section == 3) {
        
        labelHeader.text = NSLocalizedString(@"ContatosQuemVoceQuerAjudar", nil);
    }
    
    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        if ([self.ajudaOferecidaPedirAjudaList count] == 0) {
            
            return 0;
        }
    }
    else if (section == 1) {
        
        if ([self.minhasOfertasPedirAjudaList count] == 0) {
            
            return 0;
        }
    }
    else if (section == 2) {
        
        if ([self.ajudaOferecidaOferecerAjudaList count] == 0) {
            
            return 0;
        }
    }
    else if (section == 3) {
        
        if ([self.minhasOfertasOferecidoAjudaList count] == 0) {
            
            return 0;
        }
    }
   
    
    return 30;
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        if ([self.ajudaOferecidaPedirAjudaList count] == 0) {
            
            return nil;
        }
    }
    else if (section == 1) {
       
        if ([self.minhasOfertasPedirAjudaList count] == 0) {
            
            return nil;
        }
    }
    else if (section == 2) {
        
        if ([self.ajudaOferecidaOferecerAjudaList count] == 0) {
            
            return nil;
        }
    }
    else if (section == 3) {
        
        if ([self.minhasOfertasOferecidoAjudaList count] == 0) {
            
            return nil;
        }
    }
    
    return @"";
}

/*
 -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 return 76;
 }
 */
/*
 
 -(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 
 return @"";
 }
 */

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        AjudaOfertadaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AceitarAjuda"];
        
        cell.parent = self;
        [cell reloadCell:[self.ajudaOferecidaPedirAjudaList objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if (indexPath.section == 1) {
        
        OfertarAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfertarAjuda"];
        
        cell.parent = self;
        [cell reloadCell:[self.minhasOfertasPedirAjudaList objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if (indexPath.section == 2) {
        
        AjudaOfertadaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AceitarAjuda"];
        
        cell.parent = self;
        [cell reloadCell:[self.ajudaOferecidaOferecerAjudaList objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if (indexPath.section == 3) {
        
        OfertarAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfertarAjuda"];
        
        cell.parent = self;
        [cell reloadCell:[self.minhasOfertasOferecidoAjudaList objectAtIndex:indexPath.row]];
        
        return cell;
    }
//    else if (indexPath.section == 4) {
//        
//        CategoriaAjudaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoriaAjuda"];
//        
//        [cell reloadCell:[self.categoriasList objectAtIndex:indexPath.row]];
//        
//        return cell;
//    }
    
    return nil;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    /*
    if (indexPath.section == 2) {
     
        CategoriaPedidoAjudaViewController *categoriaPedidoAjudaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Categoria"];
        categoriaPedidoAjudaViewController.categoria = [self.categoriasPedirAjudaList objectAtIndex:indexPath.row];
        categoriaPedidoAjudaViewController.tipo = NO;
        [self.navigationController pushViewController:categoriaPedidoAjudaViewController animated:YES];
    }
    else
    */
    if (indexPath.section == 4) {
        
        CategoriaPedidoAjudaViewController *categoriaPedidoAjudaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Categoria"];
        categoriaPedidoAjudaViewController.categoria = [self.categoriasList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:categoriaPedidoAjudaViewController animated:YES];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  PostViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 13/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdicionarAmigosViewController.h"
#import "AdicionarMidiasViewController.h"

@interface PostViewController : BaseViewController<CLLocationManagerDelegate, MZFormSheetBackgroundWindowDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UITokenFieldDelegate> {
    
}

@property (strong, nonatomic) IBOutlet UISwitch *uiswFacebook;
@property(nonatomic, strong) IBOutlet UIButton *uibConvidarAmigos;
@property(nonatomic, strong) IBOutlet UIButton *uibFechar;
@property(nonatomic, strong) IBOutlet UIButton *uibLocalizacao;
@property(nonatomic, strong) IBOutlet UIButton *uibImagem;
@property(nonatomic, strong) IBOutlet UIButton *uibPost;
@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UITextView *uitvTexto;
@property(nonatomic, strong) IBOutlet UITokenFieldView *uitfvPost;
@property(nonatomic, strong) IBOutlet UIView *uivUploads;
@property(nonatomic, strong) IBOutlet UIView *uivPost;
@property(nonatomic, strong) IBOutlet UISegmentedControl *uisgcTipo;

@property(nonatomic, assign) CGFloat animatedDistance;
@property(nonatomic, assign) CGFloat keyboardHeight;
@property(nonatomic, strong) NSMutableArray *midiasList;
@property(nonatomic, strong) NSMutableArray *pessoasList;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLLocation *localizacaoAtual;
@property(nonatomic, strong) NSString *cidadeAtual;
@property(nonatomic, strong) NSDictionary *imagemPerfilData;
@property(nonatomic, strong) NSMutableDictionary *imagemPerfilParameters;

-(void) adicionarToken:(NSString *) title object:(id) object;

@end

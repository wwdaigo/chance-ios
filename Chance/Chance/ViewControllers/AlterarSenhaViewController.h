//
//  AlterarSenhaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 15/4/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlterarSenhaViewController : UIViewController <UITextFieldDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UITextField *uitfSenhaAtual;
@property(nonatomic, strong) IBOutlet UITextField *uitfNovaSenha;
@property(nonatomic, strong) IBOutlet UITextField *uitfConfirmarNovaSenha;

@end

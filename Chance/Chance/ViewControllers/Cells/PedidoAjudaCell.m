//
//  PedidoAjudaCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PedidoAjudaCell.h"

@implementation PedidoAjudaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Actions

-(IBAction)OnDenunciar:(id)sender {
    
    UIAlertView *uialDenunciar = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CertezaDenunciarPedidoAjuda", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Nao", nil) otherButtonTitles:NSLocalizedString(@"Sim", nil), nil];
    [uialDenunciar setTag:1];
    
    [uialDenunciar show];
}

-(IBAction)OnOferecerAjuda:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.pedido objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    [NSRequestManager requestURL:@"Ajuda/Oferecer" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            if ([[self.pedido objectForKey:@"TipoAjuda"] integerValue] == 0) {
                
                NSString *messagem = NSLocalizedString(@"OfertaAjudaEnviado", nil);
                
                messagem = [messagem stringByReplacingOccurrencesOfString:@"_Nome_" withString:[[self.pedido objectForKey:@"Autor"] objectForKey:@"Apelido"]];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:messagem delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else if ([[self.pedido objectForKey:@"TipoAjuda"] integerValue] == 1) {
                
                NSString *messagem = NSLocalizedString(@"PedidoAjudaEnviado", nil);
                
                messagem = [messagem stringByReplacingOccurrencesOfString:@"_Nome_" withString:[[self.pedido objectForKey:@"Autor"] objectForKey:@"Apelido"]];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:messagem delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnVerPerfil:(id)sender {
    
    if ((!self.pedido) ||
        ([[[self.pedido objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    
    verPerfilViewController.perfil = [self.pedido objectForKey:@"Autor"];
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];
}

#pragma mark - Internal Actions

-(void) reloadCell:(NSDictionary *) aPedido  {
    
    self.pedido = aPedido;
    
    [self.uilApelido setText:[[self.pedido objectForKey:@"Autor"] objectForKey:@"Apelido"]];
    [self.uilTexto setText:[self.pedido objectForKey:@"Texto"]];
    
    if ([[[self.pedido objectForKey:@"Autor"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [self.uiivFoto setImageWithURLString:[[[self.pedido objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        //[self.uiivFoto setImageWithURL:[NSURL URLWithString:[[[self.pedido objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    /*
     if ([[[self.pedido objectForKey:@"Autor"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
     
     //[MRProgressOverlayView showOverlayAddedTo:self.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
     
     NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[[self.pedido objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]]];
     
     AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
     operation.responseSerializer = [AFImageResponseSerializer serializer];
     
     [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
     
     [self.uiivFoto setImage:(UIImage *)responseObject];
     [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     
     NSLog(@"Error: %@", error);
     [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
     }];
     
     [operation start];
     }
     else {
     
     [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     }
     */
    NSString *localizacao;
    if (([self.pedido objectForKey:@"Localizacao"]) &&
        (![[[self.pedido objectForKey:@"Localizacao"] objectForKey:@"Descricao"] isKindOfClass:[NSNull class]])) {
        
        localizacao = [[self.pedido objectForKey:@"Localizacao"] objectForKey:@"Descricao"];
    }
    else {
        
        localizacao = @"";
    }
    
    [self.uilDataCriacao setText:[NSString stringWithFormat:@"%@ \n%@", [self.pedido objectForKey:@"DataCriacao"], localizacao]];
    
    NSString *texto = [self.pedido objectForKey:@"Texto"];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15]};
    
    CGRect titleRect = [texto boundingRectWithSize:(CGSize){300, 2000.0f} options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attributes context:nil];
    
    CGSize size = titleRect.size;
    
    CGFloat height = 0;
    
    if (size.height <= 64) {
        
        height = 70.0f;
    }
    else {
        
        height = size.height + 20;
    }
    
    [self.uivTexto setFrame:CGRectMake(0, 90, 320, height)];
    [self.uilTexto setFrame:CGRectMake(10, 0, 280, height)];
    [self.uivRodape setFrame:CGRectMake(0, 90 + height, 320, 40)];
    
    if ([[self.pedido objectForKey:@"JaDenunciei"] boolValue]) {
        
        [self botaoDenunciar:NO];
    }
    else {
        
        [self botaoDenunciar:YES];
    }
    
    [self.uibDenunciar setTitle:NSLocalizedString(@"Denunciar", nil) forState:UIControlStateNormal];
    if ([[self.pedido objectForKey:@"TipoAjuda"] boolValue]) {
        
        [self.uibOferecerAjuda setTitle:NSLocalizedString(@"PedirAjuda", nil) forState:UIControlStateNormal];
        [self.uibOferecerAjuda setImage:[UIImage imageNamed:@"ico-ajudar"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibOferecerAjuda setTitle:NSLocalizedString(@"OferecerAjuda", nil) forState:UIControlStateNormal];
        [self.uibOferecerAjuda setImage:[UIImage imageNamed:@"ico-oferecer-ajuda"] forState:UIControlStateNormal];
    }
    
    /*320
     //btn-tres-pontos
     [self.uiivPessoas1 setHidden:YES];
     [self.uiivPessoas2 setHidden:YES];
     [self.uiivPessoas3 setHidden:YES];
     [self.uiivPessoas4 setHidden:YES];
     
     for (int i = 0; i < [[self.oferta objectForKey:@"Pessoas"] count]; i++) {
     
     if (i == 0) {
     
     [self.uiivPessoas1 setHidden:NO];
     if ([[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
     [self.uiivPessoas1 setImageWithURL:[NSURL URLWithString:[[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
     }
     else {
     
     [self.uiivPessoas1 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     }
     }
     else if (i == 1) {
     
     [self.uiivPessoas2 setHidden:NO];
     if ([[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
     [self.uiivPessoas2 setImageWithURL:[NSURL URLWithString:[[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
     }
     else {
     
     [self.uiivPessoas2 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     }
     }
     else if (i == 2) {
     
     [self.uiivPessoas3 setHidden:NO];
     if ([[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
     [self.uiivPessoas3 setImageWithURL:[NSURL URLWithString:[[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
     }
     else {
     
     [self.uiivPessoas3 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     }
     }
     else if (i == 3) {
     
     [self.uiivPessoas4 setHidden:NO];
     if ([[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
     [self.uiivPessoas4 setImageWithURL:[NSURL URLWithString:[[[[self.oferta objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
     }
     else {
     
     [self.uiivPessoas4 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
     }
     }
     else {
     
     [self.uiivPessoas4 setImage:[UIImage imageNamed:@"btn-tres-pontos"]];
     break;
     }
     }
     */
}

-(void) reloadData:(NSDictionary *) aPedido {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.pedido objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    
    [parameters setObject:[self.pedido objectForKey:@"TipoAjuda"] forKey:@"TipoAjuda"];
    
    [NSRequestManager requestURL:@"Ajuda/Ver" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self reloadCell:result];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - Internal Methods

-(void) botaoDenunciar:(BOOL)status {
    
    if (status) {
        
        [self.uibDenunciar setUserInteractionEnabled:YES];
        [self.uibDenunciar setImage:[UIImage imageNamed:@"ico-denunciar"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibDenunciar setUserInteractionEnabled:NO];
        [self.uibDenunciar setImage:[UIImage imageNamed:@"ico-denunciar-ativo"] forState:UIControlStateNormal];
    }
}

#pragma mark - UIAlertViewDelegate Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            
            //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            [parameters setObject:[self.pedido objectForKey:@"PedidoId"] forKey:@"PedidoId"];
            [NSRequestManager requestURL:@"Ajuda/Denunciar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
                    //[self.uitvFeed reloadData];
                    [self reloadData:self.pedido];
                    
                    if (![[self.pedido objectForKey:@"TipoAjuda"] isKindOfClass:[NSNull class]]) {
                        
                        if ([[self.pedido objectForKey:@"TipoAjuda"] intValue] == 0) {
                            
                            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"OfertaDenunciadoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                            
                            [uialErro show];
                        }
                        else {
                            
                            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PedidoDenunciadoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                            
                            [uialErro show];
                        }
                    }
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
            
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

@end

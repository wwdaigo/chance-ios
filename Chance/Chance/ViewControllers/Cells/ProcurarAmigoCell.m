//
//  ProcurarAmigoCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 12/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ProcurarAmigoCell.h"

@implementation ProcurarAmigoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions

-(IBAction)OnAdicionar:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.amigo objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    [NSRequestManager requestURL:@"Relacionamento/SolicitarAmizade" andParameters:parameters andButton:sender andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            //[(BaseTableViewController *)self.parent refreshData:1];
            [self reloadData:self.amigo];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnSeguir:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.amigo objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    [NSRequestManager requestURL:@"Relacionamento/SeguirPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            //[(BaseTableViewController *)self.parent refreshData:1];
            [self reloadData:self.amigo];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnVerPerfil:(id)sender {
    
    if ((!self.amigo) ||
        ([[self.amigo objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    verPerfilViewController.perfil = self.amigo;
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];
}

#pragma mark - Internal Action

-(void) reloadCell:(NSDictionary *) aPerfil andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath {
    
    self.amigo = aPerfil;
    self.indexPath = aIndexPath;
    self.tableView = aTableView;
    
    if (self.indexPath.section == 0) {
        
        [[self uilApelido] setText:[self.amigo objectForKey:@"Apelido"]];
        
        if ([[self.amigo objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
            
            [self.uiivFoto setImageWithURL:[NSURL URLWithString:[[self.amigo objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        }
        else {
            
            [self.uiivFoto setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        }
        
        if ([[self.amigo objectForKey:@"Amizade"] boolValue]) {
            
            [[self uibAdicionar] setHidden:YES];
        }
        else {
            
            [[self uibAdicionar] setHidden:NO];
        }
        
        if ([[self.amigo objectForKey:@"Seguindo"] boolValue]) {
            
            [[self uibSeguir] setHidden:YES];
        }
        else {
            
            [[self uibSeguir] setHidden:NO];
        }
        
        [[self uilNivel] setText:[self.amigo objectForKey:@"Nivel"]];
        [[self uilPontuacao] setText:[[self.amigo objectForKey:@"Pontuacao"] stringValue]];
    }
    else if (self.indexPath.section == 1) {
        
        [[self uilApelido] setText:[self.amigo objectForKey:@"Apelido"]];
        
        if ([[self.amigo objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
            
            [[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[self.amigo objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        }
        else {
            
            [[self uiivFoto] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        }
        
        [[self uibAdicionar] setHidden:YES];
        [[self uibSeguir] setHidden:YES];
        
        [[self uilNivel] setText:[self.amigo objectForKey:@"Nivel"]];
        [[self uilPontuacao] setText:[[self.amigo objectForKey:@"Pontuacao"] stringValue]];
    }
}

-(void) reloadData:(NSDictionary *) aPerfil {

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[aPerfil objectForKey:@"PerfilId"] forKey:@"Id"];
    
    [NSRequestManager requestURL:@"Conta/VerPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self reloadCell:result andTableView:self.tableView andIndexPath:self.indexPath];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

@end

//
//  PostTableViewCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 21/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostCell.h"

@interface PostTableViewCell : UITableViewCell {
    
}

@property(nonatomic, strong) PostCell *uiPost;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) NSMutableArray *postList;

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath;
-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath andAllowDeleted:(BOOL) aAllowDeleted andReloadDataBlock:(void (^)())reloadDataBlock;

@end

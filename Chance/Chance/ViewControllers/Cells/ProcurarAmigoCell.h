//
//  ProcurarAmigoCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 12/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseTableViewController.h"
#import "VerPerfilViewController.h"

@interface ProcurarAmigoCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilPontuacao;
@property(nonatomic, strong) IBOutlet UILabel *uilNivel;

@property(nonatomic, strong) IBOutlet UIButton *uibAdicionar;
@property(nonatomic, strong) IBOutlet UIButton *uibSeguir;

@property(nonatomic, strong) NSDictionary *amigo;
@property(nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, strong) NSMutableArray *amigosList;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) UITableView *tableView;

-(void) reloadCell:(NSDictionary *) aPerfil andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath;
-(void) reloadData:(NSDictionary *) aPerfil;

@end

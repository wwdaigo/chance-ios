//
//  OfertarAjudaCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "OfertarAjudaCell.h"

@implementation OfertarAjudaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions

-(IBAction)OnRemover:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.oferta objectForKey:@"PedidoId"] forKey:@"PedidoId"];
    [NSRequestManager requestURL:@"Ajuda/Remover" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            [(BaseTableViewController *)self.parent refreshData:1];
        }
    } andFailure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnVisualizar:(id)sender {
    
    OfertaAjudaViewController *ofertaAjudaViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"OfertaAjuda"];
    ofertaAjudaViewController.oferta = [NSDictionary dictionaryWithDictionary:self.oferta];
    [self.parent.navigationController pushViewController:ofertaAjudaViewController animated:YES];
}

-(IBAction)OnVerPerfil:(id)sender {
    
    if ((!self.oferta) ||
        ([[[self.oferta objectForKey:@"Benfeitor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    
    verPerfilViewController.perfil = [self.oferta objectForKey:@"Benfeitor"];
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];
}

#pragma mark - Methods

-(void) reloadCell:(NSDictionary *) aOferta  {
 
    self.oferta = aOferta;
    
    if (![[[self.oferta objectForKey:@"Autor"] objectForKey:@"Apelido"] isKindOfClass:[NSNull class]]) {
        
        [[self uilApelido] setText:[[self.oferta objectForKey:@"Autor"] objectForKey:@"Apelido"]];
    }
    
    if (![[[self.oferta objectForKey:@"Categoria"] objectForKey:@"Nome"] isKindOfClass:[NSNull class]]) {
        
        [[self uilCategoria] setText:[[self.oferta objectForKey:@"Categoria"] objectForKey:@"Nome"]];
    }
    
    if (![[self.oferta objectForKey:@"TipoAjuda"] isKindOfClass:[NSNull class]]) {
        
        if ([[self.oferta objectForKey:@"TipoAjuda"] integerValue] == 0) {
            
            [[self uiivTipoAjuda] setImage:[UIImage imageNamed:@"ico-pedir-ajuda"]];
            [[self uilTipoAjuda] setText:NSLocalizedString(@"VisualizarContatos" , nil)];
            //[[self uilTipoAjuda] setText:NSLocalizedString(@"AceitarMostrarMeusContatos" , nil)];
        }
        else if ([[self.oferta objectForKey:@"TipoAjuda"] integerValue] == 1) {
            
            [[self uiivTipoAjuda] setImage:[UIImage imageNamed:@"ico-ajuda-oferecida"]];
            [[self uilTipoAjuda] setText:NSLocalizedString(@"VisualizarContatos" , nil)];
            //[[self uilTipoAjuda] setText:NSLocalizedString(@"AjudarMostrarMeusContatos", nil)];
        }
    }
    
    if (([[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) &&
        (![[[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"Url"] isKindOfClass:[NSNull class]])) {
        
        [self.uiivFoto setImageWithURLString:[[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"Url"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [[self uiivFoto] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
}

@end

//
//  PostCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/3/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PostCell.h"


@implementation PostCell

-(void)redirectToPerfil:(NSDictionary *)perfil{
    
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    
    verPerfilViewController.perfil = perfil;
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.allowDeleted = NO;
}

#pragma mark - Internal Action

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath andAllowDeleted:(BOOL) aAllowDeleted andReloadDataBlock:(void (^)())reloadDataBlock {
    
    self.allowDeleted = aAllowDeleted;
    
    [self reloadCell:aPost andTableView:aTableView andIndexPath:aIndexPath];
    
    if (reloadDataBlock) {
        
        self.reloadBlock = reloadDataBlock;
    }
}

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath {
    
    self.indexPath = aIndexPath;
    self.post = aPost;
    self.tableView = aTableView;
    
    [self.uivTexto setFrame:CGRectMake(self.uivTexto.frame.origin.x, self.uivCabecalho.frame.size.height, self.uivTexto.frame.size.width, self.uivTexto.frame.size.height)];
    
    [self.uivCabecalho setHidden:NO];
    [self.uivTexto setHidden:NO];
    [self.uivRodape setHidden:NO];
    
    if ([[self.post objectForKey:@"Midias"] count] == 0) {
        
        [self.uivMidias setHidden:YES];
        
        [self.uivRodape setFrame:CGRectMake(self.uivRodape.frame.origin.x, (self.uivCabecalho.frame.size.height + self.uivTexto.frame.size.height), self.uivRodape.frame.size.width, self.uivRodape.frame.size.height)];
    }
    else {
        
        [self.uivMidias setHidden:NO];
        
        [self.uivMidias setFrame:CGRectMake(self.uivMidias.frame.origin.x, (self.uivCabecalho.frame.size.height + self.uivTexto.frame.size.height), self.uivMidias.frame.size.width, self.uivMidias.frame.size.height)];
        [self.uivRodape setFrame:CGRectMake(self.uivRodape.frame.origin.x, (self.uivCabecalho.frame.size.height + self.uivTexto.frame.size.height + self.uivMidias.frame.size.height), self.uivRodape.frame.size.width, self.uivRodape.frame.size.height)];
    }
    
    //self.
    //144
    /*
     {"Texto":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat, augue eu pellentesque suscipit, arcu purus dignissim urna, quis auctor tortor erat nec magna. Pellentesque convallis est non tempor suscipit. Praesent et aliquam justo. In magna justo, sodales et sapien in, commodo aliquet sapien. In a lacus et eros porttitor posuere eu sit amet metus. Vivamus id augue nec libero lacinia euismod. Suspendisse potenti. Cras dictum rutrum pharetra. Mauris ligula nunc, dignissim vitae ante et, lobortis vulputate lectus. Vivamus nec neque sit amet dolor vulputate pulvinar. In vel dictum sem, quis fermentum odio. Ut consectetur quam vitae luctus porttitor. Curabitur quis ante ante. Sed ligula libero, dictum nec malesuada ut, sodales sed risus. Donec adipiscing, tellus id lacinia ornare, quam velit dapibus urna, ut sagittis metus nulla sed augue. Vestibulum luctus, neque et gravida pellentesque, tortor lacus imperdiet nisl, sed commodo tellus nunc at dolor. Integer bibendum porta nisl, luctus laoreet velit posuere non. Aenean gravida placerat urna, quis tempus turpis ullamcorper et. Nulla magna arcu, tincidunt eget nisi in, varius sollicitudin elit. Integer commodo scelerisque odio eget lacinia. Donec a molestie libero, et semper elit.",
     "DataCriacao":"03/02/2014 11:38",
     "Contagios":14,
     "Comentarios":7,
     "Autor":
     {"PerfilId":1,"Apelido":"Autor da Silva",
     "Foto":
     {"Tipo":1,"Url":"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t1/222875_1535228920254_8195527_n.jpg"}},
     "Localizacao":
     {"Latitude":0.736842734,"Longitude":0.293746823,"Descricao":"São Paulo - SP"},
     "Pessoas":
     [{"PerfilId":2,"Apelido":"Pessoa dos Santos","Foto":{"Tipo":1,"Url":"https://scontent-a-mia.xx.fbcdn.net/hphotos-prn2/t1/1533838_10200356296869617_805886947_n.jpg"}}],"Midias":[{"Tipo":1,"Url":"https://scontent-b-mia.xx.fbcdn.net/hphotos-prn1/t31/133418_1351987699338_3348746_o.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-ash2/t1/154808_1351977259077_3058407_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-prn1/t1/155008_1351969058872_3574943_n.jpg"},{"Tipo":1,"Url":"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-ash2/t1/430923_2334975593421_1912162679_n.jpg"}
     ]}
     */
    
    [self.uibComentar setEnabled:YES];
    [self botaoContagiar:YES];
    
    if ([[self.post objectForKey:@"JaContagiei"] boolValue]) {
        
        [self botaoContagiar:NO];
    }
    else {
        
        [self botaoContagiar:YES];
    }
    
    if ((!self.post) ||
        ([[[self.post objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        if (!self.allowDeleted) {
            
            [self.uibDenunciar setHidden:YES];
            [self.uibDeletar setHidden:YES];
        }
        else {
            
            [self.uibDenunciar setHidden:YES];
            [self.uibDeletar setHidden:NO];
        }
    }
    else {
       
        [self.uibDeletar setHidden:YES];
        [self.uibDenunciar setHidden:NO];
        
        [self botaoDenunciar:YES];
        
        if ([[self.post objectForKey:@"JaDenunciei"] boolValue]) {
            
            [self botaoDenunciar:NO];
        }
        else {
            
            [self botaoDenunciar:YES];
        }
    }
    
    [self.uilApelido setText:[[self.post objectForKey:@"Autor"] objectForKey:@"Apelido"]];
    
    if ([[[self.post objectForKey:@"Autor"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:[[[self.post objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[self.uiivFoto setImageWithProgressIndicatorAndURL:[NSURL URLWithString:[[[self.post objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        //[self.uiivFoto setImageWithURLString:[[[self.post objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    [self.uilTexto setText:[self.post objectForKey:@"Texto"]];
    
    CGRect labelRect = [self.uilTexto.text boundingRectWithSize:CGSizeMake(self.uilTexto.frame.size.width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName :self.uilTexto.font} context:nil];
    
    if (self.uilTexto.frame.size.height < labelRect.size.height) {
        
        [self.uilLerMais setHidden:NO];
        //[self.uibLerMais setHidden:NO];
    }
    else {
        
        [self.uilLerMais setHidden:YES];
        //[self.uibLerMais setHidden:YES];
    }
    
    [self.uilContagios setText:[[self.post objectForKey:@"Contagios"] stringValue]];
    [self.uilComentarios setText:[[self.post objectForKey:@"Comentarios"] stringValue]];
    
    NSString *localizacao;
    if (([self.post objectForKey:@"Localizacao"]) &&
        (![[[self.post objectForKey:@"Localizacao"] objectForKey:@"Descricao"] isKindOfClass:[NSNull class]])) {
        
        localizacao = [[self.post objectForKey:@"Localizacao"] objectForKey:@"Descricao"];
    }
    else {
        
        localizacao = @"";
    }
    
    [self.uilDataCriacao setText:[NSString stringWithFormat:@"%@ \n%@", [self.post objectForKey:@"DataCriacao"], localizacao]];
    
    //btn-tres-pontos
    [self.uiivPessoas1 setHidden:YES];
    [self.uiivPessoas2 setHidden:YES];
    [self.uiivPessoas3 setHidden:YES];
    [self.uiivPessoas4 setHidden:YES];
    
    for (int i = 0; i < [[self.post objectForKey:@"Pessoas"] count]; i++) {
        
        if (i == 0) {
            
            [self.uiivPessoas1 setHidden:NO];
            if ([[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                [self.uiivPessoas1 setImageWithURL:[NSURL URLWithString:[[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
            }
            else {
                
                [self.uiivPessoas1 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
        }
        else if (i == 1) {
            
            [self.uiivPessoas2 setHidden:NO];
            if ([[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                [self.uiivPessoas2 setImageWithURL:[NSURL URLWithString:[[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
            }
            else {
                
                [self.uiivPessoas2 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
        }
        else if (i == 2) {
            
            [self.uiivPessoas3 setHidden:NO];
            if ([[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                [self.uiivPessoas3 setImageWithURL:[NSURL URLWithString:[[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
            }
            else {
                
                [self.uiivPessoas3 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
        }
        else if (i == 3) {
            
            [self.uiivPessoas4 setHidden:NO];
            if ([[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
                [self.uiivPessoas4 setImageWithURL:[NSURL URLWithString:[[[[self.post objectForKey:@"Pessoas"] objectAtIndex:i] objectForKey:@"Foto"] objectForKey:@"Url"]]];
            }
            else {
                
                [self.uiivPessoas4 setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            }
        }
        else {
            
            [self.uiivPessoas4 setImage:[UIImage imageNamed:@"btn-tres-pontos"]];
            break;
        }
    }
    
    self.fotos = [NSMutableArray arrayWithArray:[self.post objectForKey:@"Midias"]];
    
    if ([self.fotos count] == 0) {
        
        [self.uivMidias setHidden:YES];
    }
    else {
        
        [self.uivMidias setHidden:NO];
    }
    
    /*
     if ([self.fotos count] > 1) {
     
     self.uicvMidias.collectionViewLayout.collectionViewContentSize = CGSizeMake(280.0f, 190.f);
     }
     else {
     
     self.uicvMidias.collectionViewLayout.collectionViewContentSize = CGSizeMake(280.0f, 190.f);
     }
     */
    [self.uicvMidias reloadData];
}

-(void) reloadData:(NSDictionary *) aPost {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[aPost objectForKey:@"PostId"] forKey:@"PostId"];
    [NSRequestManager requestURL:@"Post/VerPost" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            @try
            {
                
                if(self.indexPath.row && self.indexPath.row > 0)
                    [self.postList replaceObjectAtIndex:self.indexPath.row withObject:result];
                
                [self reloadCell:result andTableView:self.tableView andIndexPath:self.indexPath];
                
            }
            @catch (NSException *exception)
            {
                // Print exception information
                NSLog( @"NSRangeException caught" );
                NSLog( @"Reason: %@", exception.reason );
                return;
            }
           
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - Actions

-(IBAction)OnComentar:(id)sender {
    
    ComentariosViewController *comentariosViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"Comentarios"];
    
    comentariosViewController.post = self.post;
    
    [comentariosViewController reloadDataBlock:^{
        
        [self reloadData:self.post];
    }];
    
    [self.parent.navigationController pushViewController:comentariosViewController animated:YES];
    
    /*
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:comentariosViewController];
    
    formSheet.presentedFormSheetSize = CGSizeMake(300, 250);
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    //formSheet.shadowRadius = 2.0;
    //formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
    // formSheet.landscapeTopInset = 50;
    // formSheet.portraitTopInset = 100;
    
    formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
        
        
    };
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        
        [self reloadData:self.post];
    };
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
    
    [self.parent mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
    */
}

-(IBAction)OnContagiar:(id)sender {
    

    if(((UIButton *)sender).tag == 2424){
        
        QuemContagiouViewController *listaQuemContagiou = [self.storyboard instantiateViewControllerWithIdentifier:@"ListaContagiou"];
        
        listaQuemContagiou.parent = self;
        listaQuemContagiou.delegate =self;
        
        listaQuemContagiou.idPost = [self.post objectForKey:@"PostId"];
        
        
        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:listaQuemContagiou];
        
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        formSheet.presentedFormSheetSize = CGSizeMake(300, screenRect.size.height - 100);
        //
        //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
        
        //formSheet.shadowRadius = 2.0;
        
        //formSheet.shadowOpacity = 0.3;
        
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        
        formSheet.shouldCenterVertically = YES;
        
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
        
        
        
        //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
        
        //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
        
        //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
        
        //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
        
        
        
        // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
        
        // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
        
        // formSheet.landscapeTopInset = 50;
        
        // formSheet.portraitTopInset = 100;
        
        
        
        formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
            
            
            
        };
        
        
        
        formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
            
            
            
            
            
        };
        
        
        
        formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
        
        
        
        //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
        
        
        
        [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
            
            
            
        }];
        
        
        return;
    }
    

    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.post objectForKey:@"PostId"] forKey:@"PostId"];
    [NSRequestManager requestURL:@"Post/Contagiar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self.postList replaceObjectAtIndex:self.indexPath.row withObject:result];
            [self reloadData:self.post];
            
            if (self.reloadBlock) {
                
                self.reloadBlock();
            }
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnDeletar:(id)sender {
    
    UIAlertView *uialDeletar = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CertezaDeletarPost", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Nao", nil) otherButtonTitles:NSLocalizedString(@"Sim", nil), nil];
    [uialDeletar setTag:3];
    
    [uialDeletar show];
}

-(IBAction)OnDenunciar:(id)sender {
    
    UIAlertView *uialDenunciar = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CertezaDenunciar", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Nao", nil) otherButtonTitles:NSLocalizedString(@"Sim", nil), nil];
    [uialDenunciar setTag:2];
    
    [uialDenunciar show];
}

-(IBAction)OnLerMais:(id)sender {
    
     PostDetalheViewController *postDetalheViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"PostDetalhe"];
    
    [postDetalheViewController reloadDataBlock:^{
        
        [self reloadData:self.post];
    }];
     [postDetalheViewController reloadCell:self.post];
     [self.parent.navigationController pushViewController:postDetalheViewController animated:YES];
}

-(IBAction)OnPessoasAjudaram:(id)sender {
    
    if ([[self.post objectForKey:@"Pessoas"] count] == 0) {
        
        return;
    }
    
    PessoasMarcadasViewController *pessoasMarcadasViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"PessoasMarcadas"];
    
    pessoasMarcadasViewController.post = self.post;
    //[self.parent.navigationController pushViewController:comentariosViewController animated:YES];
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:pessoasMarcadasViewController];
    
    formSheet.presentedFormSheetSize = CGSizeMake(300, 300);
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    //formSheet.shadowRadius = 2.0;
    //formSheet.shadowOpacity = 0.3;
    //formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    //[formSheet setShadowOpacity:0.3];
    //formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    //[[MZFormSheetController sharedWindow] setBackgroundColor:[UIColor clearColor]];
    
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
    // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
    // formSheet.landscapeTopInset = 50;
    // formSheet.portraitTopInset = 100;
    
    formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
        
        
    };
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        

    };
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    //[MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self.parent;
    
    [self.parent mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
}

-(IBAction)OnShowImageFullscreen:(id)sender {
    
}

-(IBAction)OnShowVideo:(id)sender {
    
    NSInteger index = [(UIButton *)sender tag];
    
    NSURL *videoURL = [NSURL URLWithString:[[self.fotos objectAtIndex:index] objectForKey:@"Url"]];
    
    MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [self.parent.navigationController  presentMoviePlayerViewControllerAnimated:movieController];
    [movieController.moviePlayer play];
}

-(IBAction)OnVerPerfil:(id)sender {
    
    if ((!self.post) ||
        ([[[self.post objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    
    verPerfilViewController.perfil = self.post;
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];
}

#pragma mark - Internal Methods

-(void)allowDelete:(BOOL) aAllowDeleted {
    
    self.allowDeleted = aAllowDeleted;
}

-(void) botaoContagiar:(BOOL)status {
    
    if (status) {
         [self.uibContagiar setTag:2425];
        [self.uibContagiar setUserInteractionEnabled:YES];
        [self.uibContagiar setImage:[UIImage imageNamed:@"ico-contagiar"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibContagiar setTag:2424];
        [self.uibContagiar setImage:[UIImage imageNamed:@"ico-contagiar-ativo"] forState:UIControlStateNormal];
    }
}

-(void) botaoDenunciar:(BOOL)status {
    
    if (status) {
        
        [self.uibDenunciar setUserInteractionEnabled:YES];
        [self.uibDenunciar setImage:[UIImage imageNamed:@"ico-denunciar"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibDenunciar setUserInteractionEnabled:NO];
        [self.uibDenunciar setImage:[UIImage imageNamed:@"ico-denunciar-ativo"] forState:UIControlStateNormal];
    }
}

#pragma mark - UIAlertViewDelegate Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 2) {
        
        if (buttonIndex == 1) {
            
            //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            [parameters setObject:[self.post objectForKey:@"PostId"] forKey:@"PostId"];
            [NSRequestManager requestURL:@"Post/Denunciar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PostDenunciadoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                    
                    [self.postList replaceObjectAtIndex:self.indexPath.row withObject:result];
                    [self reloadData:self.post];
                    
                    if (self.reloadBlock) {
                        
                        self.reloadBlock();
                    }
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
        }
    }
    else if (alertView.tag == 3) {
        
        if (buttonIndex == 1) {
            
            //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            [parameters setObject:[self.post objectForKey:@"PostId"] forKey:@"PostId"];
            [NSRequestManager requestURL:@"Post/ExcluirPost" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PostDeletadoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                    
                    dispatch_async(dispatch_get_main_queue(),^ {
                        
                        [self.tableView deleteTableViewCell:self.indexPath andList:self.postList];
                    });
                    
                    if (self.reloadBlock) {
                        
                        self.reloadBlock();
                    }
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
        }
    }

    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.fotos count] == 1) {
        
        return CGSizeMake(320,190);
    }
    return CGSizeMake(280, 190);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    //if ([self.fotos count] == 1) {
        
    //    return UIEdgeInsetsMake(0, 0, 0, 0);
    //}
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return [self.fotos count];
    }
    return 0;
}
/*
 - (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
 
 return UIEdgeInsetsMake(10, 10, 10, 10);
 }
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"MidiaViewCell";
    
    MidiaViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    [cell setPermiteFullscreen:YES];

    [cell carregandoMidia:self.fotos indexPath:indexPath parent:self.parent];
    
    /*
    cell.midia = [self.fotos objectAtIndex:indexPath.row];
    [cell setParent:self.parent];
    [cell carregandoMidia];
   */
    return cell;
}
- (IBAction)onShareFacebook:(id)sender {
    
    NSString *urlPublish = [NSString stringWithFormat:URL_VIEW_POST,[self.post objectForKey:@"PostId"]];
    
    
    NSString *midiaImage = @"";
    
    for (NSDictionary *midia in [self.post objectForKey:@"Midias"]) {
        
        midiaImage = [midia objectForKey:@"UrlThumb"];
        break;
    }
    
    
    
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Chance", @"name",
                                   @"De uma chance para essa rede de ajuda contagiar você!", @"caption",
                                   [self.post objectForKey:@"Texto"], @"description",
                                   urlPublish, @"link",
                                   ![midiaImage  isEqual: @""]? midiaImage : @"http://www.iterative.com.br/mobile/chance/large.png", @"picture",
                                   nil];

    
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      NSLog(@"Error publishing story: %@", error.description);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
    
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

@end

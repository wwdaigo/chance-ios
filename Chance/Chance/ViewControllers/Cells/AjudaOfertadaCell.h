//
//  AjudaOfertadaCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VerPerfilViewController.h"

@interface AjudaOfertadaCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UIImageView *uiivTipoAjuda;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilCategoria;
@property(nonatomic, strong) IBOutlet UILabel *uilTipoAjuda;
@property(nonatomic, strong) IBOutlet UIButton *uibAceitar;
@property(nonatomic, strong) IBOutlet UIButton *uibRecusar;

@property(nonatomic, strong) NSDictionary *oferta;
@property(nonatomic, strong) UIViewController *parent;

-(void) reloadCell:(NSDictionary *) aOferta;

@end

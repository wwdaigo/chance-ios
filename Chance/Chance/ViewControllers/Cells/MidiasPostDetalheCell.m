//
//  MidiasPostDetalheCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 23/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "MidiasPostDetalheCell.h"

@implementation MidiasPostDetalheCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return [self.fotos count];
    }
    return 0;
}
/*
 - (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
 
 return UIEdgeInsetsMake(10, 10, 10, 10);
 }
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"MidiaViewCell";
    
    MidiaViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell setPermiteFullscreen:YES];
    /*
     if (cell == nil) {
     
     cell = [[[NSBundle mainBundle] loadNibNamed:@"MidiaViewCell" owner:self options:nil] objectAtIndex:0];
     }
     */
    
    [cell carregandoMidia:self.fotos indexPath:indexPath parent:self.parent];
    
    /*
    cell.midia = [self.fotos objectAtIndex:indexPath.row];
    [cell setParent:self.parent];
    [cell carregandoMidia];
    */
    return cell;
}

@end

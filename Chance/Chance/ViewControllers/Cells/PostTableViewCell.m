//
//  PostTableViewCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 21/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PostTableViewCell.h"

@implementation PostTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath {
    
    [self reloadCell:aPost andTableView:aTableView andIndexPath:aIndexPath andAllowDeleted:NO andReloadDataBlock:nil];
}

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath andAllowDeleted:(BOOL) aAllowDeleted andReloadDataBlock:(void (^)())reloadDataBlock {
    
    if (!self.uiPost) {
        
        self.uiPost = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"PostCell"];
        [self addSubview:self.uiPost.view];
    }
    
    [self.uiPost setPostList:self.postList];
    [self.uiPost setParent:self.parent];
    [self.uiPost reloadCell:aPost andTableView:aTableView andIndexPath:aIndexPath andAllowDeleted:aAllowDeleted andReloadDataBlock:reloadDataBlock];
}

@end

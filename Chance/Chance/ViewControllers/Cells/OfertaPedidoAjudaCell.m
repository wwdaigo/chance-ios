//
//  OfertaPedidoAjudaCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "OfertaPedidoAjudaCell.h"

@implementation OfertaPedidoAjudaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions


#pragma mark - Methods

-(void) reloadCell:(NSDictionary *) aOferta  {

    self.oferta = aOferta;

    [self.uilApelido setText:[[self.oferta objectForKey:@"Autor"] objectForKey:@"Apelido"]];
    
    if (![[self.oferta objectForKey:@"Texto"] isEqual:[NSNull null]]) {
        
        [self.uilTexto setText:[self.oferta objectForKey:@"Texto"]];
    }
    else {
        
        [self.uilTexto setText:@""];
    }
    
    if (![[self.oferta objectForKey:@"TextoDeContato"] isEqual:[NSNull null]]) {
        
        [self.uilContato setText:[self.oferta objectForKey:@"TextoDeContato"]];
    }
    else {
        
        [self.uilContato setText:@""];
    }
    
    if ([[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [self.uiivFoto setImageWithURLString:[[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        /*
        //[MRProgressOverlayView showOverlayAddedTo:self.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[[self.oferta objectForKey:@"Autor"] objectForKey:@"Foto"] objectForKey:@"UrlThumb"]]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self.uiivFoto setImage:(UIImage *)responseObject];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:self.uiivFoto animated:YES];
        }];
        
        [operation start];
        */
    }
    else {
        
        [self.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    NSString *localizacao;
    if (([self.oferta objectForKey:@"Localizacao"]) &&
        (![[[self.oferta objectForKey:@"Localizacao"] objectForKey:@"Descricao"] isKindOfClass:[NSNull class]])) {
        
        localizacao = [[self.oferta objectForKey:@"Localizacao"] objectForKey:@"Descricao"];
    }
    else {
        
        localizacao = @"";
    }
    
    [self.uilDataCriacao setText:[NSString stringWithFormat:@"%@ \n%@", [self.oferta objectForKey:@"DataCriacao"], localizacao]];
}

@end

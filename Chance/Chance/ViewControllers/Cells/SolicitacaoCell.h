//
//  SolicitacaoCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 12/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseTableViewController.h"
#import "VerPerfilViewController.h"

@interface SolicitacaoCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilPontuacao;
@property(nonatomic, strong) IBOutlet UILabel *uilNivel;

@property(nonatomic, strong) IBOutlet UIButton *uibAceitar;
@property(nonatomic, strong) IBOutlet UIButton *uibRecusar;

@property(nonatomic, strong) NSDictionary *solicitacao;
@property(nonatomic, strong) NSMutableArray *solicitacoesList;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, strong) UITableView *tableView;

-(void) reloadCell:(NSDictionary *) aPerfil andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath;
-(void) reloadData:(NSDictionary *) aPerfil;

@end

//
//  ComentarioCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 17/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ComentarioCell.h"

@implementation ComentarioCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions

-(IBAction)OnDeletar:(id)sender {
    
    UIAlertView *uialDeletar = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"CertezaDeletarComentario", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Nao", nil) otherButtonTitles:NSLocalizedString(@"Sim", nil), nil];
    [uialDeletar setTag:1];
    
    [uialDeletar show];
}

#pragma mark - Internal Action

-(void) reloadCell:(NSDictionary *) aComentario {

    self.comentario = aComentario;
    
    /*
     Autor =     {
     Amizade = 0;
     Apelido = "amanda ferro";
     Foto = "<null>";
     Nivel = Benfeitor;
     PerfilId = 46;
     Pontuacao = 65;
     Seguindo = 0;
     };
     DataCriacao = "20/02/2014 12:33";
     Idioma = "pt-br";
     Plataforma = "<null>";
     Texto = "bduwnzjdb djd   xie s kxi  a xjskxh q  kwkwhsbix s";
     Token = "<null>";
     Versao = "<null>";
     */
    
    NSDictionary *autor = [self.comentario objectForKey:@"Autor"];
    
    if ([[autor objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[[self uiivFoto] setImageWithProgressIndicatorAndURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        //[[self uiivFoto] setImageWithURL:[NSURL URLWithString:[[autor objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
        //[[self uiivFoto] setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    [[self uilApelido] setText:[autor objectForKey:@"Apelido"]];
    
    [[self uilComentario] setLineBreakMode:NSLineBreakByWordWrapping];
    [[self uilComentario] setNumberOfLines:0];
    
    // Configure the cell...
    NSString *text = [self.comentario objectForKey:@"Texto"];
    
    //CGSize constraint = CGSizeMake(tableView.frame.size.width - (10 * 2), 20000.0f);
    if ([text isKindOfClass:[NSNull class]]) {
        
        text = @"";
    }
    
    [[self uilComentario] setText:text];
}

#pragma mark - UIAlertViewDelegate Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            
            //[MRProgressOverlayView showOverlayAddedTo:self.view.window animated:YES];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            [parameters setObject:[self.comentario objectForKey:@"ComentarioId"] forKey:@"ComentarioId"];
            [NSRequestManager requestURL:@"Post/ExcluirComentario" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                NSDictionary *result = (NSDictionary *)responseObject;
                
                if (![[result objectForKey:@"Sucesso"] boolValue]) {
                    
                    if (!([result objectForKey:@"Mensagem"] == nil)) {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                    else {
                        
                        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                        
                        [uialErro show];
                    }
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"ComentarioDeletadoSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                    
                    dispatch_async(dispatch_get_main_queue(),^ {
                        
                        [self.tableView deleteTableViewCell:self.indexPath andList:self.comentariosList];
                    });
                }
                
            } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
                
                //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

@end

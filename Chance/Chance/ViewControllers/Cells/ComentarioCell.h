//
//  ComentarioCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 17/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComentarioCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilComentario;

@property(nonatomic, strong) NSDictionary *comentario;
@property(nonatomic, strong) NSMutableArray *comentariosList;
@property(nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) UITableView *tableView;

-(void) reloadCell:(NSDictionary *) aComentario;

@end

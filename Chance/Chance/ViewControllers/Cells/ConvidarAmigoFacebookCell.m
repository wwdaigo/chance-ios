//
//  AmigoFacebookCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 3/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "ConvidarAmigoFacebookCell.h"

@implementation ConvidarAmigoFacebookCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Methods

-(void) reloadCell:(NSDictionary *) aAmigoFacebook  {
    
    self.uilEmail.text = [aAmigoFacebook objectForKey:@"Email"];
    self.uilNome.text = [aAmigoFacebook objectForKey:@"Nome"];
    [self.uiivFoto setImageWithURLString:[aAmigoFacebook objectForKey:@"Foto"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
}

@end

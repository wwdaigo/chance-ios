//
//  AmigoFacebookCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 3/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConvidarAmigoFacebookCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UILabel *uilEmail;

@property(nonatomic, strong) NSDictionary *amigoFacebook;

-(void) reloadCell:(NSDictionary *) aAmigoFacebook;

@end

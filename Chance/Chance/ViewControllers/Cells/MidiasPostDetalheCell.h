//
//  MidiasPostDetalheCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 23/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MidiasPostDetalheCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UICollectionView *uicvMidias;

@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic, strong) UIViewController *parent;

@end

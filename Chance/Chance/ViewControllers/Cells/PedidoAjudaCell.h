//
//  PedidoAjudaCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VerPerfilViewController.h"

@interface PedidoAjudaCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas1;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas2;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas3;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas4;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilDataCriacao;
@property(nonatomic, strong) IBOutlet UILabel *uilTexto;
@property(nonatomic, strong) IBOutlet UIButton *uibOferecerAjuda;
@property(nonatomic, strong) IBOutlet UIButton *uibDenunciar;
@property(nonatomic, strong) IBOutlet UIView *uivCabecalho;
@property(nonatomic, strong) IBOutlet UIView *uivTexto;
@property(nonatomic, strong) IBOutlet UIView *uivRodape;

@property(nonatomic, strong) NSDictionary *pedido;
@property(nonatomic, strong) UIViewController *parent;

-(void) reloadCell:(NSDictionary *) aOferta;

@end

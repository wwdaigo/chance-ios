//
//  CategoriaAjudaCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriaAjudaCell : UITableViewCell {

}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilNome;
@property(nonatomic, strong) IBOutlet UILabel *uilDescricao;

@property(nonatomic, strong) NSDictionary *categoria;

-(void) reloadCell:(NSDictionary *) aCategoria;

@end

//
//  OfertaPedidoAjudaCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfertaPedidoAjudaCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilDataCriacao;
@property(nonatomic, strong) IBOutlet UILabel *uilContato;
@property(nonatomic, strong) IBOutlet UILabel *uilTexto;
@property(nonatomic, strong) IBOutlet UIView *uivCabecalho;
@property(nonatomic, strong) IBOutlet UIView *uivContato;
@property(nonatomic, strong) IBOutlet UIView *uivTexto;
@property(nonatomic, strong) IBOutlet UIView *uivRodape;

@property(nonatomic, strong) NSDictionary *oferta;
@property(nonatomic, strong) UIViewController *parent;

-(void) reloadCell:(NSDictionary *) aOferta;

@end

//
//  AdicionarAmigoCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 17/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdicionarAmigoCell : UITableViewCell {
    
}

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilPontuacao;
@property(nonatomic, strong) IBOutlet UILabel *uilNivel;

@property(nonatomic, strong) UIViewController *parent;

@end

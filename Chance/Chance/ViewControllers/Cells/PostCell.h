//
//  PostCell.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/3/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ComentariosViewController.h"
#import "MidiaViewCell.h"
#import "PessoasMarcadasViewController.h"
#import "PostDetalheViewController.h"
#import "VerPerfilViewController.h"
#import "QuemContagiouViewController.h"

@interface PostCell : BaseViewController<UIAlertViewDelegate, UITextViewDelegate,QuemContagiouDelegate> {
    
}
typedef void (^ReloadBlock)();

@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UICollectionView *uicvMidias;
@property(nonatomic, strong) IBOutlet UIButton *uibComentar;
@property(nonatomic, strong) IBOutlet UIButton *uibContagiar;
@property(nonatomic, strong) IBOutlet UIButton *uibDeletar;
@property(nonatomic, strong) IBOutlet UIButton *uibDenunciar;
@property(nonatomic, strong) IBOutlet UIButton *uibLerMais;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas1;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas2;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas3;
@property(nonatomic, strong) IBOutlet UIImageView *uiivPessoas4;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilComentarios;
@property(nonatomic, strong) IBOutlet UILabel *uilContagios;
@property(nonatomic, strong) IBOutlet UILabel *uilDataCriacao;
@property(nonatomic, strong) IBOutlet UILabel *uilLerMais;
@property(nonatomic, strong) IBOutlet UILabel *uilLocal;
@property(nonatomic, strong) IBOutlet UILabel *uilTexto;
@property(nonatomic, strong) IBOutlet UITextView *uitxvTexto;
@property(nonatomic, strong) IBOutlet UIView *uivCabecalho;
@property(nonatomic, strong) IBOutlet UIView *uivTexto;
@property(nonatomic, strong) IBOutlet UIView *uivMidias;
@property(nonatomic, strong) IBOutlet UIView *uivRodape;

@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) NSDictionary *post;
@property(nonatomic, strong) NSMutableArray *postList;
@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic) BOOL allowDeleted;
@property (strong) ReloadBlock reloadBlock;

-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath;
-(void) reloadCell:(NSDictionary *) aPost andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath andAllowDeleted:(BOOL) aAllowDeleted andReloadDataBlock:(void (^)())reloadDataBlock;
-(void) reloadData:(NSDictionary *) aPost;

@end

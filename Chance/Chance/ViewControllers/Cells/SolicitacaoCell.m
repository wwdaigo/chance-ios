//
//  SolicitacaoCell.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 12/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "SolicitacaoCell.h"

@implementation SolicitacaoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions

-(IBAction)OnAceitarSolicitacao:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.solicitacao objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    [NSRequestManager requestURL:@"Relacionamento/AprovarAmizade" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self.uibAceitar setHidden:YES];
            [self.uibRecusar setHidden:YES];
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            //[(BaseTableViewController *)self.parent refreshData:1];
            
            dispatch_async(dispatch_get_main_queue(),^ {
                
                [self.tableView deleteTableViewCell:self.indexPath andList:self.solicitacoesList];
            });
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnRecusarSolicitacao:(id)sender {
    
    //[MRProgressOverlayView showOverlayAddedTo:self.window animated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.solicitacao objectForKey:@"PerfilId"] forKey:@"PerfilId"];
    [NSRequestManager requestURL:@"Relacionamento/ReprovarAmizade" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self.uibAceitar setHidden:YES];
            [self.uibRecusar setHidden:YES];
            //self.feedList = [NSMutableArray arrayWithArray:[result objectForKey:@"Posts"]];
            //[self.uitvFeed reloadData];
            
            //[(BaseTableViewController *)self.parent refreshData:1];
            
            dispatch_async(dispatch_get_main_queue(),^ {
                
                [self.tableView deleteTableViewCell:self.indexPath andList:self.solicitacoesList];
            });
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        [MRProgressOverlayView dismissOverlayForView:self.window animated:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

-(IBAction)OnVerPerfil:(id)sender {
    
    if ((!self.solicitacao) ||
        ([[[self.solicitacao objectForKey:@"Autor"] objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    
    VerPerfilViewController *verPerfilViewController = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    verPerfilViewController.perfil = self.solicitacao;
    [self.parent.navigationController pushViewController:verPerfilViewController animated:YES];
}

#pragma mark - Internal Action

-(void) reloadCell:(NSDictionary *) aPerfil andTableView:(UITableView *) aTableView andIndexPath:(NSIndexPath *) aIndexPath {
    
    self.solicitacao = aPerfil;
    self.indexPath = aIndexPath;
    self.tableView = aTableView;
    
    if ([[self.solicitacao objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:[[self.solicitacao objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
   }
    else {
        
        [self.uiivFoto setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO] options:SDWebImageRefreshCached];
    }
    
    [[self uilApelido] setText:[self.solicitacao objectForKey:@"Apelido"]];
    [[self uilNivel] setText:[self.solicitacao objectForKey:@"Nivel"]];
    [[self uilPontuacao] setText:[[self.solicitacao objectForKey:@"Pontuacao"] stringValue]];
    
    [self.uibAceitar setHidden:NO];
    [self.uibRecusar setHidden:NO];
}

-(void) reloadData:(NSDictionary *) aPerfil {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[aPerfil objectForKey:@"PerfilId"] forKey:@"Id"];
    
    [NSRequestManager requestURL:@"Conta/VerPerfil" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            [self reloadCell:result andTableView:self.tableView andIndexPath:self.indexPath];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

@end

//
//  PostViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 13/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PostViewController.h"

@interface PostViewController ()

@end

@implementation PostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.locationManager = [[CLLocationManager alloc] init];
    self.midiasList = [NSMutableArray arrayWithCapacity:0];
    self.pessoasList = [NSMutableArray arrayWithCapacity:0];
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    
    [self.uitfvPost setShouldSearchInBackground:NO];
	[self.uitfvPost setShouldSortResults:NO];
    [self.uitfvPost.tokenField setRemovesTokensOnEndEditing:YES];
	[self.uitfvPost.tokenField addTarget:self action:@selector(tokenFieldFrameDidChange:) forControlEvents:(UIControlEvents)UITokenFieldControlEventFrameDidChange];
	[self.uitfvPost.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;"]]; // Default is a comma
    [self.uitfvPost.tokenField setPromptText:nil];
    [self.uitfvPost.tokenField setDelegate:self];
	[self.uitfvPost.tokenField setPlaceholder:NSLocalizedString(@"MarquePessoaAjudada", nil)];
    
	/*
     UIButton * addButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
     [addButton addTarget:self action:@selector(showContactsPicker:) forControlEvents:UIControlEventTouchUpInside];
     [_tokenFieldView.tokenField setRightView:addButton];
     */
    
	[self.uitfvPost.tokenField addTarget:self action:@selector(tokenFieldChangedEditing:) forControlEvents:UIControlEventEditingDidBegin];
	[self.uitfvPost.tokenField addTarget:self action:@selector(tokenFieldChangedEditing:) forControlEvents:UIControlEventEditingDidEnd];
	
	self.uitvTexto = [[UITextView alloc] initWithFrame:self.uitfvPost.contentView.bounds];
	[self.uitvTexto setScrollEnabled:NO];
	[self.uitvTexto setAutoresizingMask:UIViewAutoresizingNone];
	[self.uitvTexto setDelegate:self];
	[self.uitvTexto setFont:[UIFont systemFontOfSize:15]];
	[self.uitvTexto setText:NSLocalizedString(@"CompartilheHistoriaAjuda", nil)];
	[self.uitfvPost.contentView addSubview:self.uitvTexto];
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
	
    [self.uitvTexto becomeFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Actions
-(IBAction)ChangeTipoAjuda:(id)sender {
    
    if (self.uisgcTipo.selectedSegmentIndex == 0) {
        
        [self.uitfvPost.tokenField setPlaceholder:NSLocalizedString(@"MarquePessoaAjudada", nil)];
    }
    else {
        
        [self.uitfvPost.tokenField setPlaceholder:NSLocalizedString(@"MarqueQuemTeAjudou", nil)];
    }
}

-(IBAction)OnAtivarLocalizacao:(id)sender {
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        
        if (!self.localizacaoAtual) {
            
            [self.uibLocalizacao setBackgroundImage:[UIImage imageNamed:@"ico-pin-ativo"] forState:UIControlStateNormal];
            
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter=kCLDistanceFilterNone;
            
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            
                [self.locationManager startMonitoringSignificantLocationChanges];
            }
            
            [self.locationManager startUpdatingLocation];
            
            
        }
        else {
            
            self.localizacaoAtual = nil;
            [self.uibLocalizacao setBackgroundImage:[UIImage imageNamed:@"ico-pin"] forState:UIControlStateNormal];
        }
    }
    else {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"GPSNaoAtivo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }
}

-(IBAction)OnFechar:(id)sender{
    
    [self hideKeyboard];
}

-(IBAction)OnMarcarAmigos:(id)sender {
    
    [self hideKeyboard];
    UIAlertView *uialConvidarAmigos = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MarcarPessoas", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:nil];
    [uialConvidarAmigos setTag:1];
    
    [uialConvidarAmigos addButtonWithTitle:NSLocalizedString(@"DigitarEmail", nil)];
    [uialConvidarAmigos addButtonWithTitle:NSLocalizedString(@"MarcarPessoa", nil)];
    
    [uialConvidarAmigos show];
}

-(IBAction)OnFecharUpload:(id)sender {
    
    [UIView beginAnimations:@"hideUpload" context:nil];
    [self.uivUploads setHidden:YES];
    [UIView commitAnimations];
}

-(IBAction)OnPostar:(id)sender {
    

    
    /*
     Texto:Lorem Ipsum
     Localizacao[Latitude]:0.736842734
     Localizacao[Longitudo]:0.293746823
     Localizacao[Descricao]:São Paulo - SP
     Pessoas[]:1
     Pessoas[]:2
     Pessoas[]:bruno.bcosta@gmail.com
     Pessoas[]:10
     Pessoas[]:nnbrrc@hotmail.com
     Midias[]:8765
     Midias[]:8650
     Midias[]:7646
     Midias[]:7580
     RetornoAjuda - 0 - Ajudei - 1 - Fui ajudado
    */
    
    [self hideKeyboard];
    if ([[self.uitfvPost.tokenField tokenObjects] count] == 0) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"MarquePeloMenosUmaPessoa", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    if (([[self.uitvTexto.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) ||
        ([self.uitvTexto.text isEqualToString:NSLocalizedString(@"CompartilheHistoriaAjuda", nil)])) {
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"DigiteMessagemAntesPostar", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
        
        return;
    }
    
    [self botaoPostar:NO];
 
    [self postar];
    /*
    if (!self.imagemPerfilParameters) {
        
        [self postar];
    }
    else {
        
        [self uploadPhoto];
    }
    */
}

-(IBAction)OnUploadImagem:(id)sender {
    
    [self hideKeyboard];
    /*
    if (!self.imagemPerfilParameters) {
        
        if (([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeCamera] == NO) &&
            ([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypePhotoLibrary] == NO) &&
            ([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)){
            
            return;
        }
        
        UIAlertView *uialFoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Selecione", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Camera", nil), NSLocalizedString(@"Album", nil), nil];
        [uialFoto setTag:3];
        [uialFoto show];
    }
    else {
        
        UIAlertView *uialFoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"DesejaTrocarRemoverMidia", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"TrocarMidia", nil), NSLocalizedString(@"RemoverMidia", nil), nil];
        [uialFoto setTag:4];
        [uialFoto show];
    }
    */
    
    AdicionarMidiasViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdicionarMidias"];
    
    [vc setParent:self];
    vc.midiasList = [NSMutableArray arrayWithCapacity:0];
    [[vc midiasList] addObjectsFromArray:self.midiasList];
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
    
    formSheet.presentedFormSheetSize = CGSizeMake(320, 300);
    formSheet.shouldDismissOnBackgroundViewTap = NO;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    
    formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
        
        if ([(AdicionarMidiasViewController *)presentedFSViewController midiasList]) {
            
            [self.midiasList removeAllObjects];
            [self.midiasList addObjectsFromArray:[(AdicionarMidiasViewController *)presentedFSViewController midiasList]];
            
            if ([self.midiasList count] > 0) {
                
                [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img-ativo"] forState:UIControlStateNormal];
            }
            else {
                
                [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img"] forState:UIControlStateNormal];
            }
        }
    };
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
    
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
}

#pragma mark - Internal Methods

-(void) botaoFechar:(BOOL) status {

    if (status) {
        
        [self.uibPost setFrame:CGRectMake(self.uibPost.frame.origin.x, self.uibPost.frame.origin.y, 100, 30)];
    }
    else {
        
        [self.uibPost setFrame:CGRectMake(self.uibPost.frame.origin.x, self.uibPost.frame.origin.y, 214, 30)];
    }

    [self.uibFechar setHidden:!status];
}

-(void) botaoPostar:(BOOL) status {
    
    [UIView beginAnimations:@"" context:nil];
    
    [self.uibPost setEnabled:status];
    
    [UIView commitAnimations];
}

-(void) botaoUploadImagem {
    
    [UIView beginAnimations:@"" context:nil];
    
    /*
    if (self.imagemPerfilParameters) {
        
        [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img-ativo"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img"] forState:UIControlStateNormal];
    }
    */
    if ([self.midiasList count] > 0) {
        
        [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img-ativo"] forState:UIControlStateNormal];
    }
    else {
        
        [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img"] forState:UIControlStateNormal];
    }
    [UIView commitAnimations];
}

-(void) postar {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:self.uitvTexto.text forKey:@"Texto"];
    
    NSMutableArray *midias = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary *midia in self.midiasList) {
        
        [midias addObject:[midia objectForKey:@"MidiaId"]];
    }
    
    [parameters setObject:midias forKey:@"Midias"];
    [parameters setObject:[self.uitfvPost.tokenField tokenObjects] forKey:@"Pessoas"];
    
    if (self.localizacaoAtual) {
    
        //Localizacao[Latitude]:0.736842734
        //Localizacao[Longitudo]:0.293746823
        //Localizacao[Descricao]:São Paulo - SP
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:self.localizacaoAtual.coordinate.latitude], @"Latitude", [NSNumber numberWithFloat:self.localizacaoAtual.coordinate.longitude], @"Longitude", self.cidadeAtual, @"Descricao", nil]forKey:@"Localizacao"];
    }
    
    [parameters setObject:[NSNumber numberWithInteger:self.uisgcTipo.selectedSegmentIndex] forKey:@"RetornoAjuda"];
    
    //[parameters setObject:[self.solicitacao objectForKey:@"PostId"] forKey:@"PostId"];
    [NSRequestManager requestURL:@"Post/Criar" andParameters:parameters andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        [self botaoPostar:YES];
        
        NSDictionary *result = (NSDictionary *)responseObject;
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            if(self.uiswFacebook.isOn){
                
                [self publishStory:[result objectForKey:@"PostId"]];
                
            }

            
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"PostSucesso", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
            
            [self.uitfvPost.tokenField setText:@""];
            [self.uitfvPost.tokenField removeAllTokens];
            [self.uitfvPost.tokenField setPlaceholder:NSLocalizedString(@"MarquePessoaAjudada", nil)];
            [self.uisgcTipo setSelectedSegmentIndex:0];
            
            [self.pessoasList removeAllObjects];
            [self.midiasList removeAllObjects];
            self.localizacaoAtual = nil;
            
            [self.uibConvidarAmigos setBackgroundImage:[UIImage imageNamed:@"ico-add-pessoa"] forState:UIControlStateNormal];
            [self.uibLocalizacao setBackgroundImage:[UIImage imageNamed:@"ico-pin"] forState:UIControlStateNormal];
            [self.uibImagem setBackgroundImage:[UIImage imageNamed:@"ico-img"] forState:UIControlStateNormal];
            
            [self.uitvTexto setText:NSLocalizedString(@"CompartilheHistoriaAjuda", nil)];
        }
        
    } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
        
        //[MRProgressOverlayView dismissOverlayForView:self.view.window animated:YES];
        [self botaoPostar:YES];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}


- (void)verifyFacebookPermission {
//    
//    FBSession *session = [[FBSession alloc] init];
//    // Set the active session
//    [FBSession setActiveSession:session];
//    // Open the session
//    [session openWithBehavior:FBSessionLoginBehaviorWithNoFallbackToWebView
//            completionHandler:^(FBSession *session,
//                                FBSessionState status,
//                                NSError *error) {
//                
//                [FBRequestConnection startWithGraphPath:@"/me/permissions"
//                                      completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                          if (!error){
//                                              NSDictionary *permissions= [(NSArray *)[result data] objectAtIndex:0];
//                                              if (![permissions objectForKey:@"publish_actions"]){
//                                                  // Publish permissions not found, ask for publish_actions
//                                                  [self requestPublishPermissions];
//                                                  
//                                              } else {
//                                                  // Publish permissions found, publish the OG story
    
//                                              }
//                                              
//                                          } else {
//                                              // There was an error, handle it
//                                              // See https://developers.facebook.com/docs/ios/errors/
//                                          }
//                                      }];

                
//            }];
    
    
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    
    // If the session was opened successfully
    if (!error && (state == FBSessionStateOpen || state == FBSessionStateOpenTokenExtended)) {
        
        NSLog(@"Session opened");
        
        
        [self verifyFacebookPermission];
        
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed) {
        
        NSLog(@"Session closed");
    }
    
    if (error){
        
        NSLog(@"Error");
        
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES) {
            
            alertTitle = NSLocalizedString(@"Atencao", nil);
            alertText = [FBErrorUtility userMessageForError:error];
            //[self showMessage:alertText withTitle:alertTitle];
        }
        else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                
                alertTitle = NSLocalizedString(@"Atencao", nil);
                alertText = @"Your current session is no longer valid. Please log in again.";
                //[self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                //NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                if (!errorInformation) {
                    
                    alertTitle = NSLocalizedString(@"Atencao", nil);
                    alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                }
                else {
                    
                    alertTitle = NSLocalizedString(@"Atencao", nil);
                    alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                }
                
                // Show the user an error message
                alertTitle = NSLocalizedString(@"Atencao", nil);
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                //[self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        // Show the user the logged-out UI
        //[self userLoggedOut];
    }
    
    [FBSession.activeSession closeAndClearTokenInformation];
}

-(void)requestPublishPermissions{
    
    
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                          defaultAudience:FBSessionDefaultAudienceFriends
                                        completionHandler:^(FBSession *session, NSError *error) {
                                            __block NSString *alertText;
                                            __block NSString *alertTitle;
                                            if (!error) {
                                                if ([FBSession.activeSession.permissions
                                                     indexOfObject:@"publish_actions"] == NSNotFound){
                                                    // Permission not granted, tell the user we will not publish
                                                    alertTitle = @"Permission not granted";
                                                    alertText = @"Your action will not be published to Facebook.";
                                                    [[[UIAlertView alloc] initWithTitle:alertTitle
                                                                                message:alertText
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK!"
                                                                      otherButtonTitles:nil] show];
                                                } else {
                                                    // Permission granted, publish the OG story
//                                                    [self publishStory];
                                                }
                                                
                                            } else {
                                                // There was an error, handle it
                                                // See https://developers.facebook.com/docs/ios/errors/
                                            }
                                        }];
    
}

-(void)publishStory:(NSNumber *)postId{
    
    NSString *urlPublish = [NSString stringWithFormat:URL_VIEW_POST,postId];
    
    
    NSString *midiaImage = @"";
    
    for (NSDictionary *midia in self.midiasList) {
        
        midiaImage = [midia objectForKey:@"UrlThumb"];
        break;
    }
    
    

    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Chance", @"name",
                                   @"De uma chance para essa rede de ajuda contagiar você!", @"caption",
                                   self.uitvTexto.text, @"description",
                                   urlPublish, @"link",
                                   ![midiaImage  isEqual: @""]? midiaImage : @"http://www.iterative.com.br/mobile/chance/large.png", @"picture",
                                   nil];
    
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      NSLog(@"Error publishing story: %@", error.description);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];

    
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

-(void) uploadPhoto {
    
    NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:(NSString *)[[self.imagemPerfilParameters objectForKey:@"Data"] path] error:nil];
    NSNumber * size = [properties objectForKey: NSFileSize];
    
    [self showNotificationAlert];
    
    [self.uilProgress setText:NSLocalizedString(@"UploadMidia", nil)];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[self.imagemPerfilParameters objectForKey:@"Tipo"] forKey:@"Tipo"];
    
    [NSRequestManager uploadTaskURL:@"Midia/PostFile" andParameters:parameters andData:[self.imagemPerfilParameters objectForKey:@"Data"] andFile:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileURL:[self.imagemPerfilParameters objectForKey:@"Data"] name:@"file" fileName:[self.imagemPerfilParameters objectForKey:@"Filename"] mimeType:[self.imagemPerfilParameters objectForKey:@"MediaType"] error:nil];
        
    } andProgess:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
       
        [self performBlock:^{
            
            float percentDownloaded = ((float) totalBytesSent / (float) size.floatValue);
            NSLog(@"Upload size: %.2f", size.floatValue);
            NSLog(@"Upload totalBytesSent: %.2f", (float)totalBytesSent);
            NSLog(@"Upload percentDownloaded: %.2f", percentDownloaded);

            [self.uipvProgressBar setProgress:percentDownloaded];
            
        } afterDelay:0.3];
        
    } andSuccess:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSDictionary *result = (NSDictionary *)responseObject;
        
        //self.navigationItem.rightBarButtonItem = nil;
        //[self.uinavUpload
        //[progress dismiss:YES];
        [self dismissNotificationAlert];
        
        if (![[result objectForKey:@"Sucesso"] boolValue]) {
            
            [self botaoPostar:YES];
            if (!([result objectForKey:@"Mensagem"] == nil)) {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
            else {
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                
                [uialErro show];
            }
        }
        else {
            
            //self.imagemPerfilData = [NSDictionary dictionaryWithDictionary:result];
            [self.midiasList addObject:[NSDictionary dictionaryWithDictionary:result]];
            
            [self postar];
        }
        
    } andFailure:^(NSError *error) {
        
        [self botaoPostar:YES];
        [self dismissNotificationAlert];
        
        UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [uialErro show];
    }];
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if (buttonIndex == 1) {
            
            UIAlertView *uialConvidarAmigos = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"InformeEmailDesejaMarcar", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Marcar", nil), nil];
            [uialConvidarAmigos setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [uialConvidarAmigos setTag:2];
            
            UITextField *uitfAmigo = [uialConvidarAmigos textFieldAtIndex:0];
            [uitfAmigo setPlaceholder:NSLocalizedString(@"DigitarEmail", nil)];
            [uitfAmigo setKeyboardType:UIKeyboardTypeEmailAddress];
            
            [uialConvidarAmigos show];
            
            return;
        }
        else if (buttonIndex == 2) {
            
            AdicionarAmigosViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdicionarAmigos"];
            
            MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
            
            formSheet.presentedFormSheetSize = CGSizeMake(300, 250);
            //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
            //formSheet.shadowRadius = 2.0;
            //formSheet.shadowOpacity = 0.3;
            formSheet.shouldDismissOnBackgroundViewTap = YES;
            formSheet.shouldCenterVertically = YES;
            formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
            
            //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
            //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
            
            // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTop;
            // formSheet.keyboardMovementStyle = MZFormSheetKeyboardMovementStyleMoveToTopInset;
            // formSheet.landscapeTopInset = 50;
            // formSheet.portraitTopInset = 100;
            
            //__weak MZFormSheetController *weakFormSheet = formSheet;
            
            /*
             // If you want to animate status bar use this code
             formSheet.didTapOnBackgroundViewCompletionHandler = ^(CGPoint location) {
             
             [UIView animateWithDuration:0.3 animations:^{
             if ([weakFormSheet respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
             [weakFormSheet setNeedsStatusBarAppearanceUpdate];
             }
             }];
             };
             */
            
            formSheet.willPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {
                
                
            };
            
            formSheet.didDismissCompletionHandler  = ^(UIViewController *presentedFSViewController) {
                
                if ([(AdicionarAmigosViewController *)presentedFSViewController pessoaSelecionada]) {
                    
                    [[self pessoasList] addObject:[(AdicionarAmigosViewController *)presentedFSViewController pessoaSelecionada]];
                    
                    [self adicionarToken:[[(AdicionarAmigosViewController *)presentedFSViewController pessoaSelecionada] objectForKey:@"Apelido"] object:[[(AdicionarAmigosViewController *)presentedFSViewController pessoaSelecionada] objectForKey:@"PerfilId"]];
                }
            };
            
            formSheet.transitionStyle = MZFormSheetTransitionStyleCustom;
            
            [MZFormSheetController sharedBackgroundWindow].formSheetBackgroundWindowDelegate = self;
            
            [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
                
            }];
        }
    }
    else if (alertView.tag == 2) {
        
        if (buttonIndex == 1) {
            
            UITextField *uitfAmigo = [alertView textFieldAtIndex:0];
            
            if ([Utils IsValidEmail:uitfAmigo.text]) {
                
                [self.pessoasList addObject:uitfAmigo.text];
                [self adicionarToken:uitfAmigo.text object:uitfAmigo.text];
            }
            else {
                
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:NO];
                
                UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"EmailNaoEValido", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [uialErro show];
                
                return;
            }
        }
    }
    else if (alertView.tag == 3) {
        
        if (buttonIndex == 1) {
            
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.navigationController presentViewController:cameraUI animated:YES completion:^{
            }];
        }
        else if (buttonIndex == 2) {
            
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            //cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:cameraUI.sourceType];
            
            cameraUI.allowsEditing = YES;
            cameraUI.delegate = self;
            
            [self.navigationController presentViewController:cameraUI animated:YES completion:^{
            }];
        }
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
    else if (alertView.tag == 4) {
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if (buttonIndex == 1) {
        
            [self.midiasList removeAllObjects];
            self.imagemPerfilParameters = nil;
            [self botaoUploadImagem];
            
            UIAlertView *uialFoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Selecione", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Camera", nil), NSLocalizedString(@"Album", nil), nil];
            [uialFoto setTag:3];
            [uialFoto show];
        }
        else if (buttonIndex == 2) {
            
            [self.midiasList removeAllObjects];
            self.imagemPerfilParameters = nil;
            [self botaoUploadImagem];
        }
    }
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"NaoFoiPossivelAcharLocalizacao", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [MRProgressOverlayView dismissAllOverlaysForView:self.view.window animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
    if (!self.localizacaoAtual) {
        
        NSLog(@"didUpdateToLocation: %@", newLocation);
        self.localizacaoAtual = newLocation;
        
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                
                self.cidadeAtual = [NSString stringWithFormat:@"%@, %@",[placemark locality], [placemark administrativeArea]];
                break;
            }
        }];
    }
}

#pragma mark - TextView Delegate

-(void) hideKeyboard {
    
    [self.view resignFirstResponder];
    [self.uitvTexto resignFirstResponder];
    [self.uitfvPost resignFirstResponder];
    [self.uitfvPost.tokenField resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:NSLocalizedString(@"CompartilheHistoriaAjuda", nil)]) {
        
        [textView setTextColor:[UIColor blackColor]];
        [textView setText:@""];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        
        [textView setTextColor:[UIColor grayColor]];
        [textView setText:NSLocalizedString(@"CompartilheHistoriaAjuda", nil)];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@""]) {
        
        return YES;
    }
    
    if ([textView.text length] >= TAMANHO_MAXIMO_POST) {
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    NSInteger tipoMedia = 0;
    NSString *filePath = @"";
    NSString *fileName = @"";
    NSURL *dataURL;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        tipoMedia = 1;
        UIImage *originalImage, *editedImage, *imageToSave;
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            
            imageToSave = editedImage;
        }
        else {
            
            imageToSave = originalImage;
        }
        
        fileName = @"Image.png";
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        
        [UIImagePNGRepresentation(imageToSave) writeToFile:filePath atomically:YES];
        
        dataURL = [NSURL fileURLWithPath:filePath];
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    }
    else if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0)
             == kCFCompareEqualTo) {
        
        tipoMedia = 2;
        dataURL = [info valueForKey:UIImagePickerControllerMediaURL];
        fileName = @"Video.mov";
        filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    }
    
    if (dataURL) {
        
        self.imagemPerfilParameters = [NSMutableDictionary dictionaryWithCapacity:2];
        [self.imagemPerfilParameters setObject:[NSNumber numberWithInteger:tipoMedia] forKey:@"Tipo"];
        [self.imagemPerfilParameters setObject:dataURL forKey:@"Data"];
        [self.imagemPerfilParameters setObject:filePath forKey:@"FilePath"];
        [self.imagemPerfilParameters setObject:fileName forKey:@"FileName"];
        [self.imagemPerfilParameters setObject:mediaType forKey:@"MediaType"];
        
        [self botaoUploadImagem];
        /*
        NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:dataURL.path error:nil];
        NSNumber * size = [properties objectForKey: NSFileSize];
        
        MRProgressOverlayView *progress = [MRProgressOverlayView showOverlayAddedTo:picker.view animated:YES];
        [progress setMode:MRProgressOverlayViewModeDeterminateHorizontalBar];
        [progress setTitleLabelText:NSLocalizedString(@"UploadMidia", nil)];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[NSNumber numberWithInteger:tipoMedia] forKey:@"Tipo"];
        
        //
        
        [NSRequestManager uploadTaskURL:@"Midia/PostFile" andParameters:parameters andData:dataURL andFile:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath] name:@"file" fileName:fileName mimeType:mediaType error:nil];
            
        } andProgess:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
            
            //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
            //int aPercentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) totalBytesExpectedToSend) * 100.0);
            
            //float percentDownloaded = (int) nearbyintf(((float) totalBytesSent / (float) size.floatValue) * 100.0);
            
            [self performBlock:^{
                
                float percentDownloaded = ((float) totalBytesSent / (float) size.floatValue);
                NSLog(@"Upload %.2f", percentDownloaded);
                [progress setProgress:percentDownloaded];
                
            } afterDelay:0.3];
            
        } andSuccess:^(NSURLResponse *response, id responseObject, NSError *error) {
            NSDictionary *result = (NSDictionary *)responseObject;
            
            [progress dismiss:YES];
            
            if (![[result objectForKey:@"Sucesso"] boolValue]) {
                
                if (!([result objectForKey:@"Mensagem"] == nil)) {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
                else {
                    
                    UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    
                    [uialErro show];
                }
            }
            else {
                
                [self.midiasList addObject:result];
                
                [picker dismissViewControllerAnimated:YES completion:^{
                    
                    [self reloadImagens];
                }];
            }
        } andFailure:^(NSError *error) {
            
            [progress dismiss:YES];
            
            UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            
            [uialErro show];
        }];
        */
        /*
         [NSRequestManager postDataURL:@"Midia/PostFile" andParameters:parameters andData:dataURL andSuccess:^(NSURLSessionDataTask *task, id responseObject) {
         
         //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
         
         NSDictionary *result = (NSDictionary *)responseObject;
         
         if (![[result objectForKey:@"Sucesso"] boolValue]) {
         
         if (!([result objectForKey:@"Mensagem"] == nil)) {
         
         UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:[result objectForKey:@"Mensagem"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
         
         [uialErro show];
         }
         else {
         
         UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
         
         [uialErro show];
         }
         }
         else {
         
         [self.midiasList addObject:result];
         
         [picker dismissViewControllerAnimated:YES completion:nil];
         }
         
         } andFailure:^(NSURLSessionDataTask *task, id responseObject) {
         
         //[MRProgressOverlayView dismissOverlayForView:picker.view.window animated:YES];
         
         UIAlertView *uialErro = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atencao", nil) message:NSLocalizedString(@"FalhaConexaoServidor", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
         
         [uialErro show];
         }];
         */
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITokenFieldDelegate

-(void) adicionarToken:(NSString *) title object:(id) object {
    
    [self.uitfvPost.tokenField addTokenWithTitle:title representedObject:object];
}

- (void)keyboardWillShow:(NSNotification *)notification {
	
    [UIView beginAnimations:@"" context:nil];
    
    [UIView setAnimationDuration:0.4];
    
    [self.uitvTexto setScrollEnabled:NO];
	CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	self.keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect.size.height;
	[self resizeViews];
    
    [self botaoFechar:YES];
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
	
    [UIView beginAnimations:@"" context:nil];
    
    [UIView setAnimationDuration:0.4];
    
    [self.uitvTexto setScrollEnabled:YES];
    [self.uitvTexto scrollsToTop];
    
    self.keyboardHeight = 0;
	[self resizeViews];
    
    [self botaoFechar:NO];
    
    [UIView commitAnimations];
}

- (void)resizeViews {
    
    int tabBarOffset = self.tabBarController == nil ?  0 : self.tabBarController.tabBar.frame.size.height;
    
    if (self.keyboardHeight == 0) {
        
        self.keyboardHeight = self.tabBarController.tabBar.frame.size.height;
    }
    [self.uivPost setFrame:((CGRect){self.uivPost.frame.origin, {self.view.bounds.size.width, self.view.bounds.size.height + tabBarOffset - self.keyboardHeight}})];
    
	[self.uitvTexto setFrame:self.uitfvPost.contentView.bounds];
}

- (BOOL)tokenField:(UITokenField *)tokenField willAddToken:(UIToken *)token {
    
    if ([[tokenField tokenObjects] containsObject:token.representedObject]) {
        
        return NO;
    }
    
    if ([token.representedObject isKindOfClass:[NSString class]]) {
        
        if ([Utils IsValidEmail:token.title]) {
            
            return YES;
        }
        
        return NO;
    }
    
    return YES;
}

- (void)tokenField:(UITokenField *)tokenField didAddToken:(UIToken *)token {
    
    [self.uibConvidarAmigos setBackgroundImage:[UIImage imageNamed:@"ico-add-pessoa-ativo"] forState:UIControlStateNormal];
}

-(void)tokenField:(UITokenField *)tokenField didRemoveToken:(UIToken *)token {
    
    if ([[tokenField tokenObjects] count] <= 1) {
        
        [self.uibConvidarAmigos setBackgroundImage:[UIImage imageNamed:@"ico-add-pessoa"] forState:UIControlStateNormal];
    }
}

- (void)tokenFieldChangedEditing:(UITokenField *)tokenField {
    
    [tokenField setRightViewMode:(tokenField.editing ? UITextFieldViewModeAlways : UITextFieldViewModeNever)];
}

- (void)tokenFieldFrameDidChange:(UITokenField *)tokenField {
    
	[self textViewDidChange:self.uitvTexto];
}

- (void)textViewDidChange:(UITextView *)textView {
	
	CGFloat oldHeight = self.uitfvPost.frame.size.height - self.uitfvPost.tokenField.frame.size.height;
	CGFloat newHeight = textView.contentSize.height + textView.font.lineHeight;
	
	CGRect newTextFrame = textView.frame;
	newTextFrame.size = textView.contentSize;
	newTextFrame.size.height = newHeight;
	
	CGRect newFrame = self.uitfvPost.contentView.frame;
	newFrame.size.height = newHeight;
	
	if (newHeight < oldHeight){
		newTextFrame.size.height = oldHeight;
		newFrame.size.height = oldHeight;
	}
    
	[self.uitfvPost.contentView setFrame:newFrame];
	[textView setFrame:newTextFrame];
	[self.uitfvPost updateContentSize];
}


@end

//
//  AdicionarMidiasViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/21/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdicionarMidiaViewCell.h"

@interface AdicionarMidiasViewController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
}

@property(nonatomic, strong) IBOutlet UICollectionView *uicvMidias;

@property(nonatomic, strong) NSMutableArray *midiasList;
@property(nonatomic, strong) UIViewController *parent;

@end

//
//  PessoasMarcadasViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "BaseViewController.h"

#import "PessoaMarcadaCell.h"
#import "VerPerfilViewController.h"

@interface PessoasMarcadasViewController : BaseViewController {
    
}

@property(nonatomic, strong) IBOutlet UITableView *uitvPessoasMarcadas;

@property(nonatomic, strong) NSMutableArray *pessoasMarcadasList;
@property(nonatomic, strong) NSDictionary *post;

@end

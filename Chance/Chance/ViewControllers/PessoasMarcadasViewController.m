//
//  PessoasMarcadasViewController.m
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "PessoasMarcadasViewController.h"

@interface PessoasMarcadasViewController ()

@end

@implementation PessoasMarcadasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.pessoasMarcadasList = [NSMutableArray arrayWithCapacity:0];
    
}

-(void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    [self refreshData:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

-(IBAction)OnFechar:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
    }];
}

#pragma mark - Internal actions

-(void) refreshData:(NSInteger) page {
    
    //[super refreshData:page];
    self.pessoasMarcadasList = [NSMutableArray arrayWithArray:[self.post objectForKey:@"Pessoas"]];
    [self.uitvPessoasMarcadas reloadData];
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.pessoasMarcadasList count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 30)];
    
    //labelHeader.font = [UIFont fontWithName:@"" size:17.0f];
    labelHeader.textColor = [UIColor whiteColor];
    
    [headerView addSubview:labelHeader];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:184.0f/255.0f green:190.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    
    if ([self.pessoasMarcadasList count] == 1) {
        
        labelHeader.text = [NSString stringWithFormat: @"%i pessoa ajudada.", [self.pessoasMarcadasList count]];
    }
    else {
        
        labelHeader.text = [NSString stringWithFormat: @"%i pessoas ajudadas.", [self.pessoasMarcadasList count]];
    }
    
    
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PessoaMarcadaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PessoaMarcadaCell"];
    
    cell.pessoaMarcada = [self.pessoasMarcadasList objectAtIndex:indexPath.row];
    
    if ([[cell.pessoaMarcada objectForKey:@"Foto"] isKindOfClass:[NSDictionary class]]) {
        
        [cell.uiivFoto setImageWithURLString:[[cell.pessoaMarcada objectForKey:@"Foto"] objectForKey:@"Url"] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
        
        /*
        //[MRProgressOverlayView showOverlayAddedTo:cell.uiivFoto title:@"" mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[[cell.pessoaMarcada objectForKey:@"Foto"] objectForKey:@"Url"]]];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [cell.uiivFoto setImage:(UIImage *)responseObject];
            [MRProgressOverlayView dismissAllOverlaysForView:cell.uiivFoto animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [cell.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
            [MRProgressOverlayView dismissAllOverlaysForView:cell.uiivFoto animated:YES];
        }];
        
        [operation start];
        */
        //[[cell uiivFoto] setImageWithURL:[NSURL URLWithString:[[cell.pessoaMarcada objectForKey:@"Foto"] objectForKey:@"Url"]] placeholderImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    else {
        
        [cell.uiivFoto setImage:[UIImage imageNamed:IMAGEM_DEFAULT_USUARIO]];
    }
    
    //[[cell uiivFoto] setImageWithURL:[NSURL URLWithString:[[self.solicitacoesList objectAtIndex:indexPath.row] objectForKey:@""]] placeholderImage:[UIImage imageNamed:@"img-default"]];
    
    [[cell uilApelido] setText:[cell.pessoaMarcada objectForKey:@"Apelido"]];
    [[cell uilNivel] setText:[cell.pessoaMarcada objectForKey:@"Nivel"]];
    [[cell uilPontuacao] setText:[[cell.pessoaMarcada objectForKey:@"Pontucao"] stringValue]];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *pessoaMarcada = [self.pessoasMarcadasList objectAtIndex:indexPath.row];
    
    if ((!pessoaMarcada) ||
        ([[pessoaMarcada objectForKey:@"PerfilId"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"PerfilId"]])) {
        
        return;
    }
    VerPerfilViewController *verPerfilViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerPerfil"];
    verPerfilViewController.perfil = [self.pessoasMarcadasList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:verPerfilViewController animated:YES];
}

@end

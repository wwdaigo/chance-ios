//
//  AdicionarAmigosViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana Junior on 17/02/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdicionarAmigoCell.h"
//#import "PostViewController.h"

@interface AdicionarAmigosViewController : BaseViewController {
    
}

@property(nonatomic, strong) IBOutlet UISearchBar *uisbProcurar;
@property(nonatomic, strong) IBOutlet UITableView *uitvAdicionarAmigos;

@property(nonatomic, strong) NSMutableArray *pessoasAdicionadasList;
@property(nonatomic, strong) NSMutableArray *pessoasBuscadasList;
@property(nonatomic, strong) NSDictionary *pessoaSelecionada;
@end

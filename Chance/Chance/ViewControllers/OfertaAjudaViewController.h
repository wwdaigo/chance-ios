//
//  OfertaAjudaViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 2/24/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OfertarAjudaCell.h"
#import "OfertaPedidoAjudaCell.h"

@interface OfertaAjudaViewController : BaseTableViewController {
    
}

@property(nonatomic, strong) NSDictionary *oferta;

@property(nonatomic, assign) BOOL tipo;

@end

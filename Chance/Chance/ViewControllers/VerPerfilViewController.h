//
//  VerPerfilViewController.h
//  Chance
//
//  Created by Luiz Carlos Sant'Ana on 1/30/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EditarContaViewController.h"
#import "EditarPedirAjudaViewController.h"
#import "OfertaPedidoAjudaCell.h"

@interface VerPerfilViewController :BaseTableViewController {
    
}

@property(nonatomic, strong) IBOutlet UIButton *uibAdicionar;
@property(nonatomic, strong) IBOutlet UIButton *uibSeguir;
@property(nonatomic, strong) IBOutlet UIButton *uibEditarDados;
@property(nonatomic, strong) IBOutlet UIImageView *uiivFoto;
@property(nonatomic, strong) IBOutlet UIImageView *uiivNivel;
@property(nonatomic, strong) IBOutlet UILabel *uilApelido;
@property(nonatomic, strong) IBOutlet UILabel *uilIdade;
@property(nonatomic, strong) IBOutlet UILabel *uilNivel;
@property(nonatomic, strong) IBOutlet UILabel *uilNivelTitle;
@property(nonatomic, strong) IBOutlet UILabel *uilPontuacao;
@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *uiaiLoading;

@property(nonatomic, strong) NSMutableArray *postList;
@property(nonatomic, strong) NSMutableArray *pedidosAjudaList;
@property(nonatomic, strong) NSDictionary *perfil;

@end

//
//  Filtro.h
//  Chance
//
//  Created by pedro henrique borges on 11/8/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filtro : NSObject


// 1 - Amigos / 2 - Proximidade
@property (nonatomic) int tipoFiltro;
@property (nonatomic) float distancia;
@property (nonatomic) int idCategoria;

@property(nonatomic,strong) NSArray *categoriasSelecionadas;


@end

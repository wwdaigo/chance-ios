//
//  EmptyPostTableViewCell.m
//  Chance
//
//  Created by pedro henrique borges on 11/7/14.
//  Copyright (c) 2014 Luiz Carlos Sant'Ana Junior. All rights reserved.
//

#import "EmptyPostTableViewCell.h"

@implementation EmptyPostTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(NSString *)identifier{
    
    return @"EmptyCellId";
    
}

- (IBAction)onShowFriendsFind:(id)sender {
    
    [self.delegate showFriendsFind];

}


@end
